package com.coremedia.blueprint.taxonomies.semantic.service.opencalais;


import com.coremedia.blueprint.taxonomies.semantic.ContentSerializer;
import com.coremedia.blueprint.taxonomies.semantic.SemanticContext;
import com.coremedia.blueprint.taxonomies.semantic.SemanticEntity;
import com.coremedia.blueprint.taxonomies.semantic.service.AbstractSemanticService;
import com.coremedia.cache.Cache;
import com.coremedia.cache.CacheKey;
import com.coremedia.cap.content.Content;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import mx.bigdata.jcalais.CalaisClient;
import mx.bigdata.jcalais.CalaisObject;
import mx.bigdata.jcalais.CalaisResponse;
import mx.bigdata.jcalais.rest.CalaisRestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static java.lang.String.format;

public class CalaisService extends AbstractSemanticService {
  private static final String SERVICE_ID = "open.calais";

  private CalaisClient client;

  @PostConstruct
  @Override
  public void initialize() {
    client = new CalaisRestClient(getApiKey());
  }

  @Override
  public SemanticContext analyze(Content content) {
    Multimap<String, SemanticEntity> result = ArrayListMultimap.create();

    CalaisCacheKey calaisCacheKey = new CalaisCacheKey(client, content, getDocumentProperties(), SERVICE_ID);
    List<CalaisResponse> responses = getCache().get(calaisCacheKey);

    if (responses != null && !responses.isEmpty()) {
      for (CalaisResponse response : responses) {
        Iterable<CalaisObject> entities = response.getEntities();
        for (CalaisObject entity : entities) {
          Map<String, String> map = Maps.newHashMap();
          for (String semanticProperty : getSemanticProperties().keySet()) {
            String key = getSemanticProperties().get(semanticProperty);
            map.put(semanticProperty, entity.getField(key));
          }
          result.put(entity.getField(getGroupingKey()), SemanticEntity.populate(map));
        }
      }
    }

    return new SemanticContext(SERVICE_ID, result);
  }
}

class CalaisCacheKey extends CacheKey<List<CalaisResponse>> {
  private static final Logger LOG = LoggerFactory.getLogger(CalaisCacheKey.class);
  private CalaisClient client;
  private List<CalaisResponse> lastSuccess;
  private String serviceId;
  private Content content;
  private List<String> documentProperties;

  //executor service for the OpenCalais Request: only 4 allowed in the free version
  private ExecutorService service = Executors.newFixedThreadPool(4); //NOSONAR

  CalaisCacheKey(CalaisClient client, Content content, List<String> documentProperties, String serviceId) {
    this.client = client;
    this.serviceId = serviceId;
    this.content = content;
    this.documentProperties = documentProperties;
  }

  @Override
  public List<CalaisResponse> evaluate(Cache cache) {
    List<CalaisResponse> responses = new ArrayList<>();
    try {
      String extract = ContentSerializer.serialize(content, documentProperties);
      List<String> tokenized = ContentSerializer.tokenizeContent(extract, 10000); //NOSONAR
      List<Future<CalaisResponse>> futures = new ArrayList<>();

      //trigger callables for the executor service
      for (String part : tokenized) {
        CalaisServiceCallable callable = new CalaisServiceCallable(part, client);
        futures.add(service.submit(callable));
      }

      //...and collect them
      for (Future<CalaisResponse> entry : futures) {
        try {
          CalaisResponse callableResult = entry.get(20, TimeUnit.SECONDS); //NOSONAR //timeout of n seconds
          if (callableResult != null) {
            responses.add(callableResult);
          }
        } catch (TimeoutException e) {
          LOG.error("Timeout during waiting for callable " + entry + ": " + e.getMessage(), e);
        }
      }

      // remember last successful response
      if (!responses.isEmpty()) {
        lastSuccess = responses;
      }
    } catch (Exception e) {
      LOG.debug(format("Could not retrieve semantic info from %s", serviceId), e);
      // make sure to return last successful result
      responses = lastSuccess;
    }

    return (responses == null || responses.isEmpty()) ? lastSuccess : responses;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CalaisCacheKey that = (CalaisCacheKey) o;

    return !(client != null ? !client.equals(that.client) : that.client != null)
            && !(content != null ? !content.equals(that.content) : that.content != null)
            && !(serviceId != null ? !serviceId.equals(that.serviceId) : that.serviceId != null);

  }

  @Override
  public int hashCode() {
    int result = client != null ? client.hashCode() : 0;
    result = 31 * result + (serviceId != null ? serviceId.hashCode() : 0);
    result = 31 * result + (content != null ? content.hashCode() : 0);
    result = 31 * result + (documentProperties != null ? documentProperties.hashCode() : 0);
    return result;
  }
}
