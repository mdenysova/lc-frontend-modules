package com.coremedia.blueprint.taxonomies.semantic.service.opencalais;

import mx.bigdata.jcalais.CalaisClient;
import mx.bigdata.jcalais.CalaisResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.concurrent.Callable;

/**
 * Callable for OpenCalais
 */
public class CalaisServiceCallable implements Callable<CalaisResponse> {
  private static final Log LOG = LogFactory.getLog(CalaisServiceCallable.class);

  private String data;
  private CalaisClient client;

  public CalaisServiceCallable(String data, CalaisClient client) {
    this.data = data;
    this.client = client;
  }

  @Override
  public CalaisResponse call() {
    try {
      return client.analyze(data);
    } catch (Exception fe) {
      LOG.error("Error analyzing packet via Calais: " + fe.getMessage(), fe);
    }
    return null;
  }
}
