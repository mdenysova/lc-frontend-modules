package com.coremedia.blueprint.analytics.settings;

import com.coremedia.rest.cap.content.UrlModifier;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.StringUtils.hasText;

/**
 * Add the configured live CAE settings to a {@link UriComponentsBuilder}.
 */
public class LiveCAEUriComponentsBuilderCustomizer {

  private static final Logger LOG = LoggerFactory.getLogger(LiveCAEUriComponentsBuilderCustomizer.class);

  private final List<UrlModifier> urlModifiers = new ArrayList<>();

  private String liveCaeHost;
  private String liveCaePort;
  private String liveCaeScheme;

  @Required
  public void setLiveCaeHost(String liveCaeHost) {
    this.liveCaeHost = liveCaeHost;
  }

  @Required
  public void setLiveCaePort(String liveCaePort) {
    this.liveCaePort = liveCaePort;
  }

  @Required
  public void setLiveCaeScheme(String liveCaeScheme) {
    this.liveCaeScheme = liveCaeScheme;
  }

  @Required
  public void setUrlModifiers(List<UrlModifier> urlModifiers) {
    this.urlModifiers.clear();
    this.urlModifiers.addAll(urlModifiers);
  }

  public void fillIn(UriComponentsBuilder uriComponentsBuilder) {
    if(hasText(liveCaeHost)) {
      uriComponentsBuilder.host(getModified(liveCaeHost));
    }
    if(hasText(liveCaeScheme)) {
      uriComponentsBuilder.scheme(liveCaeScheme);
    }
    if(StringUtils.isNumeric(liveCaePort)) {
      int port = Integer.valueOf(liveCaePort).intValue();
      uriComponentsBuilder.port(port);
    }
  }

  private String getModified(String urlFragment) {
    for(UrlModifier urlModifier : urlModifiers) {
      // check if one of the urlModifiers modifies the given url fragment (giving us a tenant)
      final String modified = urlModifier.processUrl(urlFragment);
      if(!urlFragment.equals(modified)) {
        LOG.debug("modified url fragment {} to {}", urlFragment, modified);
        return modified;
      }
    }
    return urlFragment;
  }

  @PostConstruct
  void logConfiguration() {
    LOG.info("customizing deep link url using {}", this);
  }

  @Override
  public String toString() {
    return "LiveCAEUriComponentsBuilderCustomizer{" +
            "urlModifiers=" + urlModifiers +
            ", liveCaeHost='" + liveCaeHost + '\'' +
            ", liveCaePort='" + liveCaePort + '\'' +
            ", liveCaeScheme='" + liveCaeScheme + '\'' +
            '}';
  }
}
