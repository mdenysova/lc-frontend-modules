package {
public class OpenLayers {
  public static native var Map;
  public static native var Projection;
  public static native var Bounds;
}
}
