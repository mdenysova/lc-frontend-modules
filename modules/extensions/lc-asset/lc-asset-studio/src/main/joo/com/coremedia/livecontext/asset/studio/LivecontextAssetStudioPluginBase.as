package com.coremedia.livecontext.asset.studio {
import com.coremedia.cms.editor.configuration.StudioPlugin;
import com.coremedia.livecontext.asset.studio.config.livecontextAssetStudioPlugin;

public class LivecontextAssetStudioPluginBase extends StudioPlugin {

  public function LivecontextAssetStudioPluginBase(config:livecontextAssetStudioPlugin) {
    super(config)
  }
}
}