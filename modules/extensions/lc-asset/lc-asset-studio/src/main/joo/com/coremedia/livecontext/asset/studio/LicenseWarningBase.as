package com.coremedia.livecontext.asset.studio {
import com.coremedia.cms.editor.sdk.editorContext;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.ValueExpressionFactory;

import ext.Container;
import ext.config.container;

public class LicenseWarningBase extends Container {
  private var licenseInfoValueExpression:ValueExpression;

  public function LicenseWarningBase(config:container) {
    super(config);
    licenseInfoValueExpression = ValueExpressionFactory.create(
            'features', editorContext.getBeanFactory().getRemoteBean('licenseinfo'));
    licenseInfoValueExpression.addChangeListener(toggleVisibility);
  }

  override protected function afterRender():void {
    super.afterRender();
    toggleVisibility();
  }

  override protected function beforeDestroy():void {
    licenseInfoValueExpression.removeChangeListener(toggleVisibility);
    super.beforeDestroy();
  }

  private function toggleVisibility():void {
    var value:Array = licenseInfoValueExpression.getValue() || [];
    setVisible(value.indexOf('livecontext-asset-management') < 0 );
  }
}
}
