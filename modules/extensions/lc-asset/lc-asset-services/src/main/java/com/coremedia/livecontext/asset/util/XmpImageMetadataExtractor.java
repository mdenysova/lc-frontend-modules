package com.coremedia.livecontext.asset.util;

import com.adobe.xmp.XMPException;
import com.adobe.xmp.XMPIterator;
import com.adobe.xmp.XMPMeta;
import com.adobe.xmp.options.IteratorOptions;
import com.adobe.xmp.properties.XMPPropertyInfo;
import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.xmp.XmpDirectory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Extracts product codes of all products shown on the photo as XMP/IPTC "artwork or object in the picture".
 */
public class XmpImageMetadataExtractor {
  private static final Logger LOG = LoggerFactory.getLogger(XmpImageMetadataExtractor.class);
  private static final String IPTC_XMP_EXT_NS = "http://iptc.org/std/Iptc4xmpExt/2008-02-29/";
  private static final String ARTWORK_NODE = "ArtworkOrObject";
  private static final String INVENTORY_INFO = "Iptc4xmpExt:AOSourceInvNo";


  public static List<String> extractInventoryInfo(InputStream inputStream) {
    if (inputStream == null) {
      return Collections.emptyList();
    }

    List<String> externalIds = new ArrayList<>();
    Metadata metadata = null;
    try {
      metadata = ImageMetadataReader.readMetadata(new BufferedInputStream(inputStream), true);
    } catch (Exception e) {
      LOG.warn("Error processing image data" + e.getMessage());
    }
    if (metadata != null) {
      XmpDirectory xmpDirectory = metadata.getDirectory(XmpDirectory.class);
      if (xmpDirectory != null) {
        XMPIterator xmpIterator = null;
        try {
          XMPMeta xmpMeta = xmpDirectory.getXMPMeta();
          if (xmpMeta != null ) {
            xmpIterator = xmpMeta.iterator(IPTC_XMP_EXT_NS, ARTWORK_NODE, new IteratorOptions().setJustLeafnodes(true));
          }
        } catch (XMPException e) {
          LOG.debug("Could not access ");
        }
        if (xmpIterator != null) {
          while (xmpIterator.hasNext()) {
            XMPPropertyInfo next = (XMPPropertyInfo) xmpIterator.next();
            if (next.getPath() != null && next.getPath().endsWith(INVENTORY_INFO)) {
              String externalId = next.getValue();
              if (!StringUtils.isEmpty(externalId)) {
                if (LOG.isDebugEnabled()) {
                  LOG.debug("partNumber found: " + externalId);
                }
                if (!externalIds.contains(externalId)) {
                  externalIds.add(externalId);
                }
              }
            }
          }
        }
      }
    }
    return externalIds;
  }
}
