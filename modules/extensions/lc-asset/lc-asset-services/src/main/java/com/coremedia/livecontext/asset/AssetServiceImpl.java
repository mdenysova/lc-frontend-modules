package com.coremedia.livecontext.asset;

import com.coremedia.blueprint.base.settings.SettingsService;
import com.coremedia.blueprint.cae.search.Condition;
import com.coremedia.blueprint.cae.search.SearchConstants;
import com.coremedia.blueprint.cae.search.SearchQueryBean;
import com.coremedia.blueprint.cae.search.SearchResultBean;
import com.coremedia.blueprint.cae.search.SearchResultFactory;
import com.coremedia.blueprint.cae.search.Value;
import com.coremedia.blueprint.common.services.validation.ValidationService;
import com.coremedia.cap.common.IdHelper;
import com.coremedia.cap.content.Content;
import com.coremedia.cap.content.ContentRepository;
import com.coremedia.cap.content.ContentType;
import com.coremedia.cap.multisite.Site;
import com.coremedia.cap.multisite.SitesService;
import com.coremedia.livecontext.asset.license.AssetManagementLicenseInspector;
import com.coremedia.livecontext.ecommerce.asset.AssetService;
import com.coremedia.livecontext.ecommerce.asset.AssetUrlProvider;
import com.coremedia.livecontext.ecommerce.asset.CatalogPicture;
import com.coremedia.livecontext.ecommerce.catalog.CatalogService;
import com.coremedia.livecontext.ecommerce.catalog.Product;
import com.coremedia.livecontext.ecommerce.catalog.ProductVariant;
import com.coremedia.blueprint.base.livecontext.ecommerce.common.Commerce;
import com.coremedia.livecontext.ecommerce.common.StoreContext;
import com.coremedia.livecontext.ecommerce.common.StoreContextProvider;
import com.coremedia.objectserver.beans.ContentBean;
import com.coremedia.objectserver.beans.ContentBeanFactory;
import com.google.common.base.CharMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import static com.coremedia.blueprint.base.livecontext.util.CommerceServiceHelper.getServiceProxyForStoreContext;

public class AssetServiceImpl implements AssetService {

  public static final String CONFIG_KEY_DEFAULT_PICTURE = "livecontext.default.product.picture";

  private static final Logger LOG = LoggerFactory.getLogger(AssetServiceImpl.class);
  private AssetUrlProvider assetUrlProvider;
  private SitesService sitesService;
  private SearchResultFactory searchResultFactory;
  private SettingsService settingsService;
  private ContentBeanFactory contentBeanFactory;
  private AssetChanges assetChanges;
  private long searchCacheDurationInSeconds = 300;
  private ValidationService<ContentBean> validationService;
  private boolean validate = true;

  @Inject
  @Named("lcAssetManagementLicenseInspector")
  private AssetManagementLicenseInspector licenseInspector;

  @Override
  public CatalogPicture getCatalogPicture(String url) {

    String externalId = parsePartNumberFromUrl(url);
    if (externalId == null) {
      // Not a CMS URL
      return new CatalogPicture(url, null);
    }

    // Make absolute and replace {cmsHost}
    String imageUrl = assetUrlProvider.getImageUrl(url);

    List<Content> cmPictures = findPictures(externalId);
    if (cmPictures.isEmpty()) {
      return new CatalogPicture(imageUrl, null);
    }

    return new CatalogPicture(imageUrl, cmPictures.get(0));
  }

  @Override
  @Nonnull
  public List<Content> findPictures(@Nonnull String externalId) {

    List<ContentBean> references = Collections.emptyList();

    Site site = getSite();
    if (site != null) {
      if (licenseInspector.isFeatureActive()) {
        references = findAssets("CMPicture", externalId, site);
      }

      if (references.isEmpty()) {
        Content defaultPicture = getDefaultPicture(site);
        if (defaultPicture != null) {
          return Collections.singletonList(defaultPicture);
        }
      }
    }
    return toContentAssets(references);
  }

  @Override
  @Nonnull
  public List<Content> findVisuals(@Nullable String externalId) {
    Site site = getSite();
    if (site != null && licenseInspector.isFeatureActive() && externalId != null) {
      List<ContentBean> references = findAssets("CMVisual", externalId, site);
      if (references.isEmpty()) {
        Content defaultPicture = getDefaultPicture(site);
        if (defaultPicture != null) {
          return Collections.singletonList(defaultPicture);
        }
      }
      return toContentAssets(references);
    }
    return Collections.emptyList();
  }

  @Override
  @Nonnull
  public List<Content> findDownloads(@Nullable String externalId) {
    List<ContentBean> references = Collections.emptyList();
    Site site = getSite();
    if (site != null && licenseInspector.isFeatureActive() && externalId != null) {
      references = findAssets("CMDownload", externalId, site);
    }
    return toContentAssets(references);
  }

  @Nonnull
  List<ContentBean> findAssets(@Nonnull String contentType, @Nonnull String externalId, @Nonnull Site site) {

    Collection<Content> changedAssets = assetChanges.get(externalId, site);

    List<ContentBean> indexedAssets = doFindAssets(contentType, externalId, site);

    //merge indexed assets with changed assets
    List<ContentBean> assets = new ArrayList<>(indexedAssets);
    if (changedAssets != null) {
      for (Content changedContent: changedAssets) {
        //filter the documents of the given contentType
        if (changedContent.getType().isSubtypeOf(contentType)) {
          ContentBean changedBean = contentBeanFactory.createBeanFor(changedContent);
          if (!assets.contains(changedBean)) {
            assets.add(changedBean);
          }
        }
      }
    }

    //check now if the assets are up-to-date
    for (int i = assets.size() - 1; i >= 0; i--) {
      ContentBean asset = assets.get(i);
      if (!assetChanges.isUpToDate(asset.getContent(), externalId, site)) {
        assets.remove(asset);
      }
    }

    if (validate) {
      //filter validity
      //noinspection unchecked
      assets = (List<ContentBean>) validationService.filterList(assets);
    }

    //sort by the name of the content
    Collections.sort(assets, new Comparator<ContentBean>() {
      @Override
      public int compare(ContentBean bean1, ContentBean bean2) {
        return bean1.getContent().getName().compareTo(bean2.getContent().getName());
      }
    });


    if (assets.isEmpty()) {
      //try to load assets from parent Product, if partNumber belongs to ProductVariant
      assets = findFallbackForProductVariant(externalId, site, contentType);
    }

    return assets;
  }

  List<ContentBean> doFindAssets(@Nonnull String contentType, @Nonnull String externalId, @Nonnull Site site) {
    return poseSolrQuery(contentType, site, externalId);
  }

  private List<ContentBean> poseSolrQuery(@Nonnull String contentType, Site site, String externalId) {
    SearchQueryBean query = new SearchQueryBean();
    query.setSearchHandler(SearchQueryBean.SEARCH_HANDLER.DYNAMICCONTENT);
    int rootChannelId = IdHelper.parseContentId(site.getSiteRootDocument().getId());
    query.addFilter(Condition.is(SearchConstants.FIELDS.NAVIGATION_PATHS, Value.exactly("\\/" + rootChannelId)));
    query.setNotSearchableFlagIgnored(true);
    ContentRepository repository = site.getSiteRootDocument().getRepository();
    query.addFilter(Condition.is(SearchConstants.FIELDS.DOCUMENTTYPE, Value.anyOf(AssetServiceImpl.getSubTypesOf(contentType, repository))));
    query.setQuery(SearchConstants.FIELDS.COMMERCE_ITEMS.toString() + ':' + '"' + externalId + '"');
    SearchResultBean searchResult = searchResultFactory.createSearchResult(query, searchCacheDurationInSeconds);
    //noinspection unchecked
    return (List<ContentBean>) searchResult.getHits();
  }

  private List<ContentBean> findFallbackForProductVariant(String externalId, Site site, String contentType) {
    List<ContentBean> assets = Collections.emptyList();
    StoreContext storeContextForSite = getStoreContextProvider().findContextBySite(site);

    if (storeContextForSite != null) {

      getStoreContextProvider().setCurrentContext(storeContextForSite);

      Product product = getCatalogService().findProductById(Commerce.getCurrentConnection().getIdProvider().formatProductId(externalId));

      if (product != null && product instanceof ProductVariant) {
        Product parentProduct = ((ProductVariant) product).getParent();
        if (parentProduct != null) {
          //noinspection unchecked
          assets = findAssets(contentType, parentProduct.getExternalId(), site);
        }
      }
    }
    return assets;
  }

  private Site getSite() {
    if (Commerce.getCurrentConnection() != null) {
      String siteId = Commerce.getCurrentConnection().getStoreContext().getSiteId();
      return sitesService.getSite(siteId);
    }
    return null;
  }

  @Override
  public Content getDefaultPicture(@Nonnull Site site) {
    return settingsService.setting(CONFIG_KEY_DEFAULT_PICTURE, Content.class, site.getSiteRootDocument());
  }

  static List<String> getSubTypesOf(String contentType, ContentRepository contentRepository) {
    ContentType type = contentRepository.getContentType(contentType);
    if (type == null) {
      throw new IllegalStateException("The configured content type '" + contentType + "' does not exist.");
    }

    Set<ContentType> subtypes = type.getSubtypes();
    List<String> escapedContentTypes = new ArrayList<>(subtypes.size());
    for (ContentType subtype : subtypes) {
      escapedContentTypes.add(escapeLiteralForSearch(subtype.getName()));
    }
    return escapedContentTypes;
  }

  @Nonnull
  private static String escapeLiteralForSearch(@Nonnull String literal) {
    return '"' + CharMatcher.is('"').replaceFrom(literal, "\\\"") + '"';
  }

  private String parsePartNumberFromUrl(String urlStr) {
    if (!urlStr.contains(URI_PREFIX)) {
      return null;
    }

    try {
      //the expected url format is http://<host>/a/b/Partnumber.jpg
      URL url = new URL(urlStr);
      String fileNameWithExt = url.getFile();
      if (fileNameWithExt != null) {
        String fileName = fileNameWithExt.substring(0, fileNameWithExt.lastIndexOf("."));
        return fileName.substring(fileName.lastIndexOf('/') + 1);
      }
    } catch (MalformedURLException e) {
      LOG.info("Could not parse " + urlStr, e);
    }
    return null;
  }

  public static List<Content> toContentAssets(List<ContentBean> contentBeans) {
    ArrayList<Content> contents = new ArrayList<>(contentBeans.size());
    for (ContentBean contentBean : contentBeans) {
      contents.add(contentBean.getContent());
    }
    return contents;
  }


  public StoreContextProvider getStoreContextProvider() {
    return Commerce.getCurrentConnection().getStoreContextProvider();
  }

  @Required
  public void setAssetUrlProvider(AssetUrlProvider assetUrlProvider) {
    this.assetUrlProvider = assetUrlProvider;
  }

  @Required
  public void setSitesService(SitesService sitesService) {
    this.sitesService = sitesService;
  }

  @Required
  public void setSearchResultFactory(SearchResultFactory searchResultFactory) {
    this.searchResultFactory = searchResultFactory;
  }

  public CatalogService getCatalogService() {
    return Commerce.getCurrentConnection().getCatalogService();
  }

  @Required
  public void setSettingsService(SettingsService settingsService) {
    this.settingsService = settingsService;
  }

  @Required
  public void setContentBeanFactory(ContentBeanFactory contentBeanFactory) {
    this.contentBeanFactory = contentBeanFactory;
  }

  @Required
  public void setAssetChanges(AssetChanges assetChanges) {
    this.assetChanges = assetChanges;
  }

  @Required
  public void setValidationService(ValidationService<ContentBean> validationService) {
    this.validationService = validationService;
  }

  /**
   *
   * @param searchCacheDurationInSeconds cache duration for the search result in seconds
   */
  public void setSearchCacheDurationInSeconds(long searchCacheDurationInSeconds) {
    this.searchCacheDurationInSeconds = searchCacheDurationInSeconds;
  }

  /**
   * Set this to false to disable the validation. Default is true.
   * @param validate false to disable the validation.
   */
  public void setValidate(boolean validate) {
    this.validate = validate;
  }

  @Nonnull
  @Override
  public AssetService withStoreContext(StoreContext storeContext) {
    return getServiceProxyForStoreContext(storeContext, this, AssetService.class);
  }
}
