<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.contentbeans.CMPicture" -->

<div class="cm-teaser cm-teaser--picture cm-teaser--plain">
  <@cm.include self=self />
</div>
