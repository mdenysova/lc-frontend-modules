<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.contentbeans.CMPlaceholder" -->

<#assign searchId=bp.generateId("search") />

<#-- todo design + suggestions and more -->
<form class="cm-search">
  <label for="${searchId}" class="sr-only">${bp.getMessage("search.label")}</label>
  <input id="${searchId}" type="search" placeholder="Search" value="" name="search" class="cm-search" />
  <input type="submit" name="Search Button" />
</form>
