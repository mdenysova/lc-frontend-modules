<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.contentbeans.CMDownload" -->
<#-- @ftlvariable name="cssClass" type="java.lang.String" -->
<#-- @ftlvariable name="islast" type="java.lang.Boolean" -->

<#assign cssClass=cm.localParameters().cssClass!"" />
<#assign islast=cm.localParameters().islast!false />
<#assign link=cm.getLink(self.data!cm.UNDEFINED) />

<div class="cm-claim thumbnail ${cssClass}<#if !(islast)> is-last</#if>"<@cm.metadata self.content />>
  <#-- picture -->
  <@bp.optionalLink href="${link}">
    <#if self.picture?has_content>
      <@cm.include self=self.picture params={
      "limitAspectRatios": [ "portrait_ratio1x1" ],
      "classBox": "cm-claim__picture-box",
      "classImage": "cm-claim__picture",
      "metadata": ["properties.pictures"]
      }/>
    <#else>
      <div class="cm-claim__picture cm-aspect-ratio-box">
        <div class="cm-aspect-ratio-box__content cm-image--missing"></div>
      </div>
    </#if>
  </@bp.optionalLink>
  <div class="caption">
    <#-- headline -->
    <@bp.optionalLink href="${link}">
      <h3 class="cm-claim__headline thumbnail-label"<@cm.metadata "properties.teaserTitle" />>
        <span>${self.teaserTitle!""}
          <#if link?has_content>
              <i class="glyphicon glyphicon-download" aria-hidden="true"></i>
          </#if>
        </span>
      </h3>
    </@bp.optionalLink>
    <#-- teaser text, 3 lines ~ 120 chars -->
    <p class="cm-claim__text"<@cm.metadata "properties.teaserText" />>
      <@bp.renderWithLineBreaks bp.truncateText(self.teaserText!"", bp.setting(cmpage, "claim.max.length", 115)) />
    </p>
  </div>
</div>
