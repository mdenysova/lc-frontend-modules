<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.layout.PageGrid" -->

<#if self?has_content>
  <#list self.rows![] as row>
  <div class="cm-row row">
    <#-- Iterate over each placement-->
    <#list row.placements![] as placement>
      <#if placement.name != "header">
      <div class="col-xs-12 col-md-${placement.colspan!1}">
        <@cm.include self=placement />
      </div>
      <#else>
        <@cm.include self=placement />
      </#if>
    </#list>
  </div>
  </#list>
</#if>
