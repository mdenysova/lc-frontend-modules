<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.navigation.Navigation" -->
<#-- @ftlvariable name="cssClass" type="java.lang.String" -->
<#-- @ftlvariable name="childrenCssClass" type="java.lang.String" -->
<#-- @ftlvariable name="maxDepth" type="java.lang.Integer" -->
<#-- @ftlvariable name="isRoot" type="java.lang.Boolean" -->

<#assign isRoot=(isRoot!true)/>
<#assign cssClass=""/>

<#if self.visibleChildren?has_content>
  <#if !(maxDepth?has_content) || (maxDepth > 0)>
    <#if maxDepth?has_content>
      <#assign maxDepth=maxDepth - 1 />
    </#if>
    <#-- set css classes -->
    <#if isRoot>
      <#assign cssClass="cm-megamenu nav navbar navbar-nav row"/>
    <#else>
      <#assign cssClass="cm-menu"/>
    </#if>
    
    <ul class="${cssClass}">
      <#list self.visibleChildren![] as child>
        <@cm.include self=child view="asNavLinkListItem" params={
          "maxDepth": maxDepth!0,
          "navPathList": cmpage.navigation.navigationPathList
        } />
      </#list>
    </ul>
  </#if>
</#if>
