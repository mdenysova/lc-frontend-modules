<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.layout.PageGridPlacement" -->

<#assign numberOfItems=self.items?size />

<header id="cm-${self.name!""}" class="cm-header navbar navbar-default"<@cm.metadata data=bp.getPlacementPropertyName(self)!""/>>

  <#-- fixed position of logo and navigation button-->
  <div class="navbar-header">
    <#-- logo -->
    <a class="cm-logo navbar-brand" href="${cm.getLink(cmpage.navigation.rootNavigation!cm.UNDEFINED)}">
      <span class="cm-logo__image"></span>
      <span class="sr-only">${bp.getMessage("home")}</span>
    </a>
    <#-- button for navigation -->
    <button type="button" class="cm-header__button navbar-toggle collapsed" data-toggle="collapse" data-target=".cm-header-is-collapse">
      <span class="sr-only">${bp.getMessage("navigation.toggle")}</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>

  <#-- breadcrumb -->
  <div class="cm-breadcrumb--outer cm-header-is-collapse collapse in">
    <@cm.include self=cmpage.navigation!cm.UNDEFINED view="asBreadcrumb" params={"classBreadcrumb": "breadcrumb"} />
  </div>

  <#-- header items like search, language chooser, links -->
  <#if (numberOfItems > 0)>
    <ul class="cm-header__items cm-header-is-collapse collapse">
    <#list self.items![] as item>
      <li class="cm-header__item">
        <@cm.include self=item view="asHeader" />
      </li>
    </#list>
    </ul>
  </#if>
</header>

<#-- main navigation -->
<#if (cmpage.navigation.rootNavigation)?has_content>
  <nav id="cm-navigation" class="cm-header-is-collapse cm-nav-collapse navbar-collapse collapse">
    <#-- in blueprint rootNavigation declared as navigation is instance of CMChannel -->
    <#assign navi=cmpage.navigation.rootNavigation />
    <@cm.include self=navi view="asNavLinkList" params={ "maxDepth": 2 } />
    <div class="cm-nav-collapse__gradiant"></div>
  </nav>
</#if>
