<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.navigation.Navigation" -->
<#-- @ftlvariable name="isRoot" type="java.lang.Boolean" -->
<#-- @ftlvariable name="navPathList" type="java.util.List" -->

<#assign isActive=""/>
<#assign cssClass=""/> 
<#assign isRoot=(isRoot!true)/>

<#if isRoot || (!((self.hidden)!false))>
  
  <#if (bp.isActiveNavigation(self, navPathList![]))>
    <#assign isActive="active"/>
  </#if>
  
  <#if isRoot>
    <#assign cssClass="cm-megamenu__item col-xs-12 col-md-4"/>
    <#if (bp.isActiveNavigation(self, navPathList![]))>
      <#assign cssClass= cssClass + " active"/>
    </#if>
    <#assign linkCssClass="cm-megamenu__title"/>

  <#else>
    <#assign cssClass="cm-menu__item"/>
    <#if (bp.isActiveNavigation(self, navPathList![]))>
      <#assign cssClass= cssClass + " active"/>
    </#if>
    <#assign linkCssClass="cm-menu__title"/>
  </#if>

  <li class="${cssClass}">
    <#-- link to this item in navigation -->
    <@cm.include self=self view="asLink" params={"cssClass": linkCssClass}/>
    <#-- include child items, if exist-->
    <#if self.visibleChildren?has_content>
      <@cm.include self=self view="asNavLinkList" params={"isRoot": false}/>
    </#if>
  </li>
</#if>
