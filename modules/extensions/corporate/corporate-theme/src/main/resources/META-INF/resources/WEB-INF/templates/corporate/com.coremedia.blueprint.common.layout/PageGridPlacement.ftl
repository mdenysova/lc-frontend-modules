<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.layout.PageGridPlacement" -->

<#assign placementName=self.name!"" />
<#if placementName == "header">
  <#-- special placement: header -->
  <@cm.include self=self view="asHeader" />
<#elseif placementName == "footer">
  <#--  special placement: footer -->
  <@cm.include self=self view="asFooter" />
<#elseif placementName == "main" && cmpage.detailView>
  <#-- special placement: main if in detailview -->
  <#-- replace main placement with the single content and display it in detail view-->
  <@cm.include self=bp.getContainerFromBase(self, [cmpage.content]) view="asDetails" />
<#else>
  <#-- all others -->
  <#-- check setting (linklist) on page, if this placement should display the first element as header (gap) -->
  <#assign stringlist=bp.setting(cmpage, "placementsWithFirstItemAsHeader", []) />
  <#assign isInSidebar=placementName == "sidebar" />

  <#assign withGap=stringlist?seq_contains(placementName) />
  <#-- display first element as Gap -->
  <#assign items=self.items![] />
  <#if withGap && items?has_content>
    <@cm.include self=bp.getContainerFromBase(self, [items?first]) view="asGap" />
    <@cm.include self=bp.getContainerFromBase(self, items[1..]) view="asPlacement" params={"isInSidebar": isInSidebar} />
  <#else>
    <@cm.include self=self view="asPlacement" params={"isInSidebar": isInSidebar} />
  </#if>
</#if>
