<#-- @ftlvariable name="self" type="com.coremedia.blueprint.analytics.google.GoogleAnalytics" -->

<#if self.content?has_content>
  <#assign currentPageUrl= cm.getLink(self.content)/>
</#if>
<#-- google analytics -->
<#if self.enabled>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', '${self.webPropertyId!""}', '${self.domainName!""}');
  ga('set', 'anonymizeIp', true);
  ga('set', 'dimension1', '${self.contentId!""}');
  ga('set', 'dimension2', '${self.contentType!""}');
  ga('set', 'dimension3', '${self.navigationPathIds?join('_')}');
  ga('require', 'displayfeatures');
  ga('send', 'pageview', '${currentPageUrl!""}');
</script>
</#if>