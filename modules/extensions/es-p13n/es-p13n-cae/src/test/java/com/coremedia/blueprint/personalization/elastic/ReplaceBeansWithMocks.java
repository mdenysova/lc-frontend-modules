package com.coremedia.blueprint.personalization.elastic;

import com.coremedia.elastic.core.license.LicenseInspector;
import org.mockito.Mockito;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class ReplaceBeansWithMocks implements BeanPostProcessor {

  public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
    if(beanName.equals("licenseInspector")) {
      final LicenseInspector mock = Mockito.mock(LicenseInspector.class);
      Mockito.when(mock.isLicensed()).thenReturn(true);
      return mock;
    }
    return bean;
  }

  public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
    return bean;
  }

}