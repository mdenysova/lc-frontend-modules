/**
 *  CoreMedia Namespace
 */
var coremedia = (function (module) {
  return module;
}(coremedia || {}));
coremedia.blueprint = (function (module) {
  return module;
}(coremedia.blueprint || {}));

/**
 *
 */
coremedia.blueprint.faq = function (module) {
  'use strict';
  /* --- Vars --- */
  var $ = coremedia.blueprint.$;
  var $document = $(document);
  var $window = $(window);

  /* --- Events --- */

  var EVENT_PREFIX = "coremedia.blueprint.faq.";
  module.EVENT_LAYOUT_CHANGED = EVENT_PREFIX + "layoutChanged";
  module.EVENT_SCROLLED = EVENT_PREFIX + "scrolled";

  /**
   * Carousel Teaser
   */
  module.accordion = function() {

    var classAccordionItemHeader = "cm-faq-item__header";
    var classAccordionItemContent = "cm-faq-item__content";
    var classAccordionItemHeaderActive = "is-open";
    var classAccordionItemContentActive = "is-open";

    // prefix/namespace for events in this module
    var EVENT_PREFIX = "coremedia.blueprint.basic.accordion.";
    var EVENT_ACCORDION_CHANGED = EVENT_PREFIX + "accordionChanged";

    function change($accordion, $activeItem) {
      $accordion.find(".js-accordion-item").not($activeItem).each(function () {
        var $item = $(this);
        $item.find("." + classAccordionItemHeader).first().removeClass(classAccordionItemHeaderActive);
        $item.find("." + classAccordionItemContent).first().removeClass(classAccordionItemContentActive);
      });
      $activeItem.find("." + classAccordionItemHeader).first().addClass(classAccordionItemHeaderActive);
      $activeItem.find("." + classAccordionItemContent).first().addClass(classAccordionItemContentActive);
      //$accordion.trigger(EVENT_ACCORDION_CHANGED, [$activeItem]);
    };

    $(".js-accordion-item").each(function () {
      var $item = $(this);
      var $accordion = $item.closest(".js-accordion");
      var $itemHeader = $item.find("." + classAccordionItemHeader).first();

      $itemHeader.on("click", function () {
        change($accordion, $item);
      });
    });

    $(".js-accordion-item:eq(0)").find("." + classAccordionItemHeader).first().addClass(classAccordionItemHeaderActive);
    $(".js-accordion-item:eq(0)").find("." + classAccordionItemContent).first().addClass(classAccordionItemContentActive);
  }

  return module;
}(coremedia.blueprint.faq || {});

// --- DOCUMENT READY --------------------------------------------------------------------------------------------------
coremedia.blueprint.$(function () {

  var $ = coremedia.blueprint.$;
  var $window = $(window);
  var $document = $(document);

  coremedia.blueprint.logger.log("FAQ DOM RDY");

  // initially load responsive images
  $(".cm-responsive-image").responsiveImages();

  // remove the spinner and event listener, when images are loaded
  $(".cm-image--loading").each(function () {
    $(this).on("load", function () {
      coremedia.blueprint.logger.log("Responsive Image loaded, remove spinner");
      $(this).removeClass("cm-image--loading").off("load");
    })
  });

  coremedia.blueprint.faq.accordion();

  // trigger layout changed event if the size of the window changes using smartresize plugin
  $window.smartresize(function () {
    $document.trigger(coremedia.blueprint.faq.EVENT_LAYOUT_CHANGED);
  });

  // --- EVENTS --------------------------------------------------------------------------------------------------------

  // EVENT_LAYOUT_CHANGED
  $document.on(coremedia.blueprint.faq.EVENT_LAYOUT_CHANGED, function () {
    coremedia.blueprint.logger.log("Window resized");
    // recalculate responsive images if layout changes
    $(".cm-responsive-image").responsiveImages();
  });
});