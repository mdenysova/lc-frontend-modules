var gulp         = require('gulp'),
    livereload   = require('gulp-livereload'),
    sass         = require('gulp-sass'),
    sourcemaps   = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    notify       = require('gulp-notify'),
    plumber      = require('gulp-plumber');

var config = {
  src: './src/scss',
  dest: './webresources/src/main/resources/META-INF/resources/themes/faq/css',
  autoprefixer: {
    browsers: ['last 3 version'],
    cascade: true,
    remove: true
  },
  sass: {
    outputStyle: 'nested',
    sourceComments: false
  },
  extensions: ['sass', 'scss', 'css']
}

// define the default task and add the watch task to it
gulp.task('default', ['watch']);

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
  livereload.listen();
  gulp.watch('templates/**/*', ['reload']);
  gulp.watch('webresources/**/*', ['reload']);
  gulp.watch(config.src+'/**/*.{'+config.extensions+'}', ['stylesheet']);
});

gulp.task('reload', function() {
  livereload.reload();
});

gulp.task('stylesheet', function() {

  var onError = function(err) {
    notify.onError({
      title:    "Gulp",
      subtitle: "Failure!",
      message:  "Error: <%= error.message %>",
      sound:    "Beep"
    })(err);
    this.emit('end');
  };

  return gulp.src(config.src + '/*.{'+config.extensions+'}')
      .pipe(plumber({errorHandler: onError}))
      .pipe(sourcemaps.init())
      .pipe(sass(config.sass))
      .pipe(autoprefixer(config.autoprefixer))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(config.dest));
});