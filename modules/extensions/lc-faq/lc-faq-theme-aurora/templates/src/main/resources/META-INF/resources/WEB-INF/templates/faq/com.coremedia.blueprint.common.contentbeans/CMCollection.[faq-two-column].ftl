<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.contentbeans.CMCollection" -->
<#-- @ftlvariable name="cmpage" type="com.coremedia.blueprint.common.contentbeans.Page" -->

<div class="cm-faq">
    <div class="cm-faq__container">
        <div class="cm-faq-group__main">
            <div class="cm-faq-group__title">${self.teaserTitle!""}</div>
            <ul class="cm-faq-accordion js-accordion"<@cm.metadata data=[self.content, "properties.items"] />>
            <#list self.items![] as item>
                <li class="cm-faq-accordion__item js-accordion-item" <@cm.metadata item.content />>
                    <div class="cm-faq-item__header cm-faq-item__header--small"<@cm.metadata "properties.title" />><span> ${item.title!""}</span></div>
                    <div class="cm-faq-item__content cm-faq-item__content--border"<@cm.metadata "properties.teaserText" />>
                        <@cm.include self=item.teaserText!cm.UNDEFINED />
                        <#assign showLinkToDetailedAnswer=bp.setting(item, "showLinkToDetailedAnswer", false) />
                        <#if showLinkToDetailedAnswer && item.detailText?has_content && item.detailText != item.teaserText>
                            <a class="cm-faq__link" href="${cm.getLink(item)}">
                              ${bp.getMessage("faqDetailMore.label")}
                            </a>
                        </#if>
                    </div>
                </li>
            </#list>
            </ul>
        </div>
        <div class="cm-faq-group__aside">
            <div class="cm-faq-help__title">All Help Topic</div>
            <ul class="cm-faq-help__collection">
              <#list cmpage.context.parentNavigation.visibleChildren![] as category>
                  <li class="cm-faq-help__item">
                    <#if category == cmpage.context>
                      <a class="cm-faq-help__header active" href="${cm.getLink(category)}">${category.teaserTitle!""}</a>
                    <#else>
                      <a class="cm-faq-help__header" href="${cm.getLink(category)}">${category.teaserTitle!""}</a>
                    </#if>
                  </li>
              </#list>
            </ul>
        </div>
    </div>
</div>