<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.contentbeans.CMArticle" -->
<#-- @ftlvariable name="isSingleColumn" type="java.lang.Boolean" -->

<div class="cm-faq">
  <div class="cm-faq__container">
    <div class="cm-faq-group__main">
      <#--<@cm.include self=self!cm.UNDEFINED view="headline" params={"classHeadline": "cm-box__header"} />-->
      <#-- picture -->
      <#if self.picture?has_content>
        <@cm.include self=self.picture params={
        "limitAspectRatios": ["landscape_ratio5x2"],
        "classBox": "cm-faq__picture-box",
        "classImage": "cm-faq__picture",
        "metadata": ["properties.pictures"]
        }/>
      <#else>
        <div class="cm-faq__picture-box" <@cm.metadata "properties.pictures" />>
          <div class="cm-faq__picture"></div>
        </div>
      </#if>
      <h2 class="cm-faq-answer__title" <@cm.metadata "properties.title" />>${self.title!""}</h2>
      <div class="cm-faq-answer__content" <@cm.metadata "properties.detailText"/>>
        <@cm.include self=self.detailText!cm.UNDEFINED />
      </div>

      <#if self.related?has_content>
        <div class="cm-faq-rq">
        <#-- hook for extensions (e.g. elastic social comments) -->
          <@cm.hook id=bp.viewHookEventNames.VIEW_HOOK_END />
          <div class="cm-faq-rq__title">Related Questions</div>
          <ul class="cm-faq-rq__content">
            <#list self.related as related>
              <@cm.include self=related view="asLinkListItem" />
            </#list>
          </ul>
        </div>
      </#if>
    </div>
    <div class="cm-faq-group__aside">
      <div class="cm-faq-help__title">All Help Topic</div>
      <ul class="cm-faq-help__collection">
      <#list cmpage.context.parentNavigation.visibleChildren![] as category>
        <li class="cm-faq-help__item">
          <#if category == cmpage.context>
            <a class="cm-faq-help__header active" href="${cm.getLink(category)}">${category.teaserTitle!""}</a>
          <#else>
            <a class="cm-faq-help__header" href="${cm.getLink(category)}">${category.teaserTitle!""}</a>
          </#if>
        </li>
      </#list>
      </ul>
    </div>
  </div>
</div>