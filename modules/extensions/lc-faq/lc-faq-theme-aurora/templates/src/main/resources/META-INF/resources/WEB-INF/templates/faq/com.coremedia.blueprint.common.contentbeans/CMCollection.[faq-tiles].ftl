<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.contentbeans.CMCollection" -->
<#-- @ftlvariable name="cmpage" type="com.coremedia.blueprint.common.contentbeans.Page" -->

<#if self.items?has_content>
  <div class="cm-faq">
    <div class="cm-faq__container">
      <ul class="cm-faq-accordion js-accordion" <@cm.metadata data=[self.content, "properties.items"] />>
      <#list self.items![] as category>
        <#if category.items?has_content>
          <li class="cm-faq-accordion__collection js-accordion-item" <@cm.metadata category.content />>
            <div class="cm-faq-item__header cm-faq-item__header--overview" <@cm.metadata "properties.teaserTitle" /> >
              <a href="${cm.getLink(category)}">
                ${category.teaserTitle!""}
              </a>
            </div>
            <#assign limitOfItems=bp.setting(self, "limitOfItems", -1) />
              <ul class="cm-faq-item__content cm-faq-item__content--overview" <@cm.metadata data=[category.content, "properties.items"]/> data-content>
                <#list category.items![] as item>
                  <#if item_index==limitOfItems>
                    <#break />
                  </#if>
                   <@cm.include self=item view="asLinkListItem" />
                </#list>
                <a class="cm-faq__link" href="${cm.getLink(category)}">All ${category.teaserTitle!""} Topic</a>
              </ul>
          </li>
        </#if>
      </#list>
      </ul>
    </div>
  </div>
</#if>