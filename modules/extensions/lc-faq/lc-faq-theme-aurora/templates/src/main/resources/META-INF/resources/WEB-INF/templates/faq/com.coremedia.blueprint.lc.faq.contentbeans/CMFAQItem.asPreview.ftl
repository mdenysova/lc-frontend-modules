<#-- @ftlvariable name="self" type="com.coremedia.blueprint.lc.faq.contentbeans.CMFAQItem" -->

<#assign fragmentViews={
"DEFAULT": "Preview_Label_Default",
"asLinkListItem" : "Preview_Label_asLinkListItem",
"asTeaser": "Preview_Label_Teaser"
} />

<@cm.include self=self view="multiViewPreview" params={
"fragmentViews": fragmentViews
}/>