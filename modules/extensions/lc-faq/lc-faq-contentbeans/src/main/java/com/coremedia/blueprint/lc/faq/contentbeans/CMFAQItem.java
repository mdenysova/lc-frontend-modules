package com.coremedia.blueprint.lc.faq.contentbeans;

import com.coremedia.blueprint.common.contentbeans.CMTeasable;
import com.coremedia.cae.aspect.Aspect;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Generated interface for beans of document type "CMFAQItem".
 */
public interface CMFAQItem extends CMTeasable {
  /**
   * {@link com.coremedia.cap.content.ContentType#getName() Name of the ContentType} 'CMArticle'.
   */
  String NAME = "CMFAQItem";


  /**
   * Returns the value of the document property {@link #MASTER}.
   *
   * @return a {@link CMFAQItem} object
   */
  @Override
  CMFAQItem getMaster();

  @Override
  Map<Locale, ? extends CMFAQItem> getVariantsByLocale();

  @Override
  Collection<? extends CMFAQItem> getLocalizations();

  @Override
  Map<String, ? extends Aspect<? extends CMFAQItem>> getAspectByName();

  @Override
  List<? extends Aspect<? extends CMFAQItem>> getAspects();

}
