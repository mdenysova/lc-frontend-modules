package com.coremedia.blueprint.lc.faq.contentbeans;

import com.coremedia.blueprint.cae.contentbeans.CMTeasableImpl;
import com.coremedia.cae.aspect.Aspect;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Generated base class for beans of document type "CMFAQItem".
 */
public abstract class CMFAQItemBase extends CMTeasableImpl implements CMFAQItem {
 /* Returns the value of the document property {@link #MASTER}.
          *
          * @return a list of {@link CMFAQItem
  } objects
  */
  @Override
  public CMFAQItem getMaster() {
    return (CMFAQItem) super.getMaster();
  }

  @Override
  public Map<Locale, ? extends CMFAQItem> getVariantsByLocale() {
    return getVariantsByLocale(CMFAQItem.class);
  }

  @Override
  @SuppressWarnings("unchecked")
  public Collection<? extends CMFAQItem> getLocalizations() {
    return (Collection<? extends CMFAQItem>) super.getLocalizations();
  }

  @Override
  @SuppressWarnings("unchecked")
  public Map<String, ? extends Aspect<? extends CMFAQItem>> getAspectByName() {
    return (Map<String, ? extends Aspect<? extends CMFAQItem>>) super.getAspectByName();
  }

  @Override
  @SuppressWarnings("unchecked")
  public List<? extends Aspect<? extends CMFAQItem>> getAspects() {
    return (List<? extends Aspect<? extends CMFAQItem>>) super.getAspects();
  }
}
