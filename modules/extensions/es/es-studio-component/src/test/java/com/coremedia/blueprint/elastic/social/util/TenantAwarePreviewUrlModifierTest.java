package com.coremedia.blueprint.elastic.social.util;

import com.coremedia.elastic.core.api.tenant.TenantService;
import com.coremedia.elastic.core.test.Injection;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TenantAwarePreviewUrlModifierTest {

  @Test
  public void testProcessUrl() throws Exception {
    final TenantAwarePreviewUrlModifier tenantAwarePreviewUrlModifier = new TenantAwarePreviewUrlModifier();
    final TenantService mock = mock(TenantService.class);
    Injection.inject(tenantAwarePreviewUrlModifier, mock);
    when(mock.getCurrent()).thenReturn("test");
    assertEquals("not_modified", tenantAwarePreviewUrlModifier.processUrl("not_modified"));
    assertEquals("test.me", tenantAwarePreviewUrlModifier.processUrl("{0}.me"));
  }
}
