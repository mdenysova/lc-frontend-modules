package com.coremedia.blueprint.elastic.social.util;

import com.coremedia.elastic.core.api.tenant.TenantService;
import com.coremedia.rest.cap.content.UrlModifier;

import javax.inject.Inject;
import java.text.MessageFormat;

public class TenantAwarePreviewUrlModifier implements UrlModifier {
  @Inject
  private TenantService tenantService;

  @Override
  public String processUrl(String url) {
    String currentTenant = tenantService.getCurrent();
    if (currentTenant != null && !("".equals(currentTenant))) {
      return MessageFormat.format(url, tenantService.getCurrent());
    }
    return url;
  }
}
