package com.coremedia.blueprint.elastic.common;

import com.coremedia.elastic.core.cms.ContentWithSite;
import com.coremedia.objectserver.web.links.Link;
import com.coremedia.objectserver.web.links.LinkFormatter;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URISyntaxException;

/**
 * This class handles instances of {@link com.coremedia.elastic.core.cms.ContentWithSite}.
 */
@Named
@Link
public class ContentWithSiteLinkHandler {

  @Inject
  private LinkFormatter linkFormatter;

  @Link(type=ContentWithSite.class)
  public String formatLink(ContentWithSite contentWithSite, String view, HttpServletRequest request, HttpServletResponse response)
          throws URISyntaxException {
    return linkFormatter.formatLink(contentWithSite.getContent(), view, request, response, true);
  }
}
