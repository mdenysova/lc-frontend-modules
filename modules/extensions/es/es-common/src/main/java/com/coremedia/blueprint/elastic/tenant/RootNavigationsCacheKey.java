package com.coremedia.blueprint.elastic.tenant;

import com.coremedia.blueprint.common.util.RootNavigationUtil;
import com.coremedia.cache.Cache;
import com.coremedia.cache.CacheKey;
import com.coremedia.cap.content.Content;
import com.coremedia.cap.content.ContentRepository;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;

/**
 * {@link com.coremedia.cache.CacheKey} to hold the repository's root navigations.
 */
class RootNavigationsCacheKey extends CacheKey<List<Content>> {

  private final ContentRepository contentRepository;

  private static final String CACHE_CLASS = RootNavigationsCacheKey.class.getName();

  RootNavigationsCacheKey(@Nonnull ContentRepository contentRepository) {
    this.contentRepository = contentRepository;
  }

  @Override
  public String cacheClass(Cache cache, List<Content> value) {
    return CACHE_CLASS;
  }

  @Override
  @Nonnull
  public List<Content> evaluate(Cache cache) {
    final List<Content> result = RootNavigationUtil.getRootNavigations(contentRepository);
    return Collections.unmodifiableList(result);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    RootNavigationsCacheKey key = (RootNavigationsCacheKey) o;

    return contentRepository.equals(key.contentRepository);
  }

  @Override
  public int hashCode() {
    return 31 * contentRepository.hashCode();
  }
}
