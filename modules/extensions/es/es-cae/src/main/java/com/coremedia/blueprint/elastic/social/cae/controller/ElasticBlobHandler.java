package com.coremedia.blueprint.elastic.social.cae.controller;

import com.coremedia.blueprint.cae.handlers.HandlerBase;
import com.coremedia.blueprint.cae.util.SecureHashCodeGeneratorStrategy;
import com.coremedia.elastic.core.api.blobs.Blob;
import com.coremedia.elastic.core.api.blobs.BlobService;
import com.coremedia.objectserver.web.HandlerHelper;
import com.coremedia.objectserver.web.links.Link;
import com.coremedia.transform.BlobTransformer;
import com.google.common.collect.ImmutableMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.Map;

import static com.coremedia.blueprint.base.links.UriConstants.Patterns.PATTERN_NUMBER;
import static com.coremedia.blueprint.base.links.UriConstants.Patterns.PATTERN_WORD;
import static com.coremedia.blueprint.base.links.UriConstants.Prefixes.PREFIX_RESOURCE;
import static com.coremedia.blueprint.base.links.UriConstants.Segments.SEGMENT_ETAG;
import static com.coremedia.blueprint.base.links.UriConstants.Segments.SEGMENT_ID;
import static com.coremedia.blueprint.base.links.UriConstants.Segments.SEGMENT_NAME;
import static com.coremedia.objectserver.web.HandlerHelper.createModel;
import static com.coremedia.objectserver.web.HandlerHelper.notFound;
import static com.coremedia.objectserver.web.HandlerHelper.redirectTo;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Controller and LinkScheme for
 * {@link Blob blobs}
 */
@Named
@Link
@RequestMapping
public class ElasticBlobHandler extends HandlerBase {

  public static final String SECHASH_SEGMENT = "secHash";
  public static final String WIDTH_SEGMENT = "width";
  public static final String HEIGHT_SEGMENT = "height";
  public static final String TRANSFORM_SEGMENT = "transform";
  // default transformation matching the image dimensions hardcoded in Comment.jsp
  protected static final int DEFAULT_WIDTH = 48;
  protected static final int DEFAULT_HEIGHT = 48;
  protected static final int MAX_WIDTH = 1500;
  protected static final int MAX_HEIGHT = 1000;
  protected static final String URI_PREFIX = "elastic/image";
  protected static final String SIMPLE_URI_PATTERN =
          '/' + PREFIX_RESOURCE +
                  "/" + URI_PREFIX +
                  "/{" + SEGMENT_ID + ":" + PATTERN_WORD + "}" +
                  "/{" + SEGMENT_ETAG + "}" +
                  "/{" + SECHASH_SEGMENT + "}" +
                  "/{" + SEGMENT_NAME + "}";
  protected static final String URI_PATTERN =
          '/' + PREFIX_RESOURCE +
                  "/" + URI_PREFIX +
                  "/{" + SEGMENT_ID + ":" + PATTERN_WORD + "}" +
                  "/{" + SEGMENT_ETAG + "}" +
                  "/{" + WIDTH_SEGMENT + ":" + PATTERN_NUMBER + "}" +
                  "/{" + HEIGHT_SEGMENT + ":" + PATTERN_NUMBER + "}" +
                  "/{" + SECHASH_SEGMENT + "}" +
                  "/{" + SEGMENT_NAME + "}";

  private static final String EMPTY_ETAG = "-";
  @Inject
  private SecureHashCodeGeneratorStrategy secureHashCodeGeneratorStrategy;

  @Inject
  private BlobTransformer blobTransformer;

  @Inject
  private BlobService blobService;

  protected String getName(Blob o) {
    return o.getFileName();
  }

  /**
   * @return Transformed blob contained in CMMedia object
   */
  protected com.coremedia.cap.common.Blob getTransformedBlob(com.coremedia.cap.common.Blob blob, Integer width, Integer height) {

    // Validate against extension of original blob. This may be different from the actual content type of the
    // transformed blob, but the link generator appends the original extension for performance reasons.
    // if (!BlobHelper.i(extension, blobAdapter, getMimeTypeService())) {
    //  return null;
    //}

    StringBuilder builder = new StringBuilder("scale").append(";");
    builder.append("w").append("=").append(width).append(";");
    builder.append("h").append("=").append(height);

    try {
      return blobTransformer.transformBlob(blob, builder.toString());
    } catch (IOException e) {
      LOG.info("Error transforming blob.", e);
      return null;
    }
  }

  protected boolean eTagMatches(Blob blob, String eTag) {
    String blobETag = blob.getMd5();
    return (blobETag != null ? blobETag.equals(eTag) : EMPTY_ETAG.equals(eTag));
  }

  protected boolean isValid(Map<String, Object> parameters, String secureHashCode) {
    return secureHashCodeGeneratorStrategy.matches(parameters, secureHashCode);
  }

  protected Map<String, ?> buildLinkMap(Blob bean) {

    /**
     * create parameters map. This is more flexible than calling URI_TEMPLATE#expand with the parameters
     * since this way the parameter's sequence is not relevant and the URI_PATTERN can be changed easier
     */
    // Use content type of original blob, not of the transformed blob which may be different.
    // Requesting the transformed blob's content type forces the transformation to be performed, which is too
    // costly for link generation.
    Map<String, Object> parameters = new ImmutableMap.Builder<String, Object>()
            .put(SEGMENT_ID, bean.getId())
            .put(SEGMENT_ETAG, bean.getMd5())
            .put(SEGMENT_NAME, bean.getFileName())
            .build();

    //generate secure hash from all parameters and add to map
    String secHash = getSecureHashCode(parameters);

    return new ImmutableMap.Builder<String, Object>().putAll(parameters).put(SECHASH_SEGMENT, secHash).build();
  }

  public Map<String, ?> buildLinkMapWithTransformation(Blob bean, Map<String, Object> linkParameters) {

    Integer height = (Integer) linkParameters.get(HEIGHT_SEGMENT);
    Integer width = (Integer) linkParameters.get(WIDTH_SEGMENT);

    if (height == null) {
      height = DEFAULT_HEIGHT;
    } else if (height > MAX_HEIGHT) {
      height = MAX_HEIGHT;
    }
    if (width == null) {
      width = DEFAULT_WIDTH;
    } else if (width > MAX_WIDTH) {
      width = MAX_WIDTH;
    }

    /**
     * create parameters map. This is more flexible than calling URI_TEMPLATE#expand with the parameters
     * since this way the parameter's sequence is not relevant and the URI_PATTERN can be changed easier
     */
    // Use content type of original blob, not of the transformed blob which may be different.
    // Requesting the transformed blob's content type forces the transformation to be performed, which is too
    // costly for link generation.
    Map<String, Object> parameters = new ImmutableMap.Builder<String, Object>()
            .put(SEGMENT_ID, bean.getId())
            .put(SEGMENT_ETAG, bean.getMd5())
            .put(WIDTH_SEGMENT, width)
            .put(HEIGHT_SEGMENT, height)
            .put(SEGMENT_NAME, bean.getFileName())
            .build();

    //generate secure hash from all parameters and add to map
    String secHash = getSecureHashCode(parameters);

    return new ImmutableMap.Builder<String, Object>().putAll(parameters).put(SECHASH_SEGMENT, secHash).build();
  }


  protected String getSecureHashCode(Map<String, Object> parameters) {
    return secureHashCodeGeneratorStrategy.generateSecureHashCode(parameters);
  }

  // --- Handlers ------------------------------------------------------------------------------------------------------

  @RequestMapping(value = SIMPLE_URI_PATTERN)
  public ModelAndView handleRequest(@PathVariable(SEGMENT_ID) String imageId,
                                    @PathVariable(SEGMENT_ETAG) String eTag,
                                    @PathVariable(SECHASH_SEGMENT) String secHash,
                                    @PathVariable(SEGMENT_NAME) String name) {

    ModelAndView result = notFound();

    if (isBlank(imageId)) {
      //no image id given
      return result;
    }

    Blob blob = blobService.get(imageId);

    if (blob == null) {
      return result;
    }

    if (eTagMatches(blob, eTag)) {
      result = createModel(blob);
    } else {
      result = redirectTo(blob);
    }

    // URL validation: segment must match and hash value must be correct
    String mediaSegment = removeSpecialCharacters(blob.getFileName());
    if (name.equals(mediaSegment)) {
      //name matches, make sure that secHash matches given URL
      Map<String, Object> parameters = new ImmutableMap.Builder<String, Object>()
              .put(SEGMENT_ID, imageId)
              .put(SEGMENT_ETAG, eTag)
              .put(SEGMENT_NAME, name)
              .build();

      if (isValid(parameters, secHash)) {
        com.coremedia.cap.common.Blob resultBlob = new BlobAdapter(blob);
        return HandlerHelper.createModel(resultBlob);
      }
    }

    return result;
  }


  @RequestMapping(value = URI_PATTERN)
  public ModelAndView handleRequest(@PathVariable(SEGMENT_ID) String imageId,
                                    @PathVariable(SEGMENT_ETAG) String eTag,
                                    @PathVariable(WIDTH_SEGMENT) Integer width,
                                    @PathVariable(HEIGHT_SEGMENT) Integer height,
                                    @PathVariable(SECHASH_SEGMENT) String secHash,
                                    @PathVariable(SEGMENT_NAME) String name) {

    ModelAndView result = notFound();

    if (isBlank(imageId)) {
      //no image id given
      return result;
    }

    Blob blob = blobService.get(imageId);

    if (blob == null) {
      return result;
    }

    if (eTagMatches(blob, eTag)) {
      result = createModel(blob);
    } else {
      result = redirectTo(blob);
    }

    // URL validation: segment must match and hash value must be correct
    String mediaSegment = removeSpecialCharacters(blob.getFileName());
    if (name.equals(mediaSegment)) {
      //name matches, make sure that secHash matches given URL
      Map<String, Object> parameters = new ImmutableMap.Builder<String, Object>()
              .put(SEGMENT_ID, imageId)
              .put(SEGMENT_ETAG, eTag)
              .put(WIDTH_SEGMENT, width)
              .put(HEIGHT_SEGMENT, height)
              .put(SEGMENT_NAME, name)
              .build();

      if (isValid(parameters, secHash)) {

        com.coremedia.cap.common.Blob resultBlob = new BlobAdapter(blob);
        //request is valid, transform blob and return model
        resultBlob = getTransformedBlob(resultBlob, width, height);

        if (resultBlob != null) {
          return HandlerHelper.createModel(resultBlob);
        }
      }
    }

    return result;
  }

  // --- LinkSchemes ---------------------------------------------------------------------------------------------------

  @Link(type = Blob.class, uri = SIMPLE_URI_PATTERN)
  public Map<String, ?> buildLink(Blob bean) {
    return buildLinkMap(bean);
  }

  @Link(type = Blob.class, parameter = {HEIGHT_SEGMENT, WIDTH_SEGMENT}, uri = URI_PATTERN)
  public Map<String, ?> buildLinkWithWidthAndHeight(Blob bean, Map<String, Object> linkParameters) {
    return buildLinkMapWithTransformation(bean, linkParameters);
  }

  @Link(type = Blob.class, parameter = {TRANSFORM_SEGMENT}, uri = URI_PATTERN)
  public Map<String, ?> buildLinkWithTransformation(Blob bean, Map<String, Object> linkParameters) {
    Object transform = linkParameters.get(TRANSFORM_SEGMENT);
    if (transform instanceof Boolean && (Boolean) transform) {
      return buildLinkMapWithTransformation(bean, linkParameters);
    } else {
      return buildLinkMap(bean);
    }
  }

  // the same link format is used for BlobRefs as for Blob
  @Link(type = BlobRef.class, uri = SIMPLE_URI_PATTERN)
  public Map<String, ?> buildLink(BlobRef bean) {
    Blob blob = blobService.get(bean.getId());
    return buildLinkMap(blob);
  }

  @Link(type = BlobRef.class, parameter = {HEIGHT_SEGMENT, WIDTH_SEGMENT}, uri = URI_PATTERN)
  public Map<String, ?> buildLinkWithWidthAndHeight(BlobRef bean, Map<String, Object> linkParameters) {
    Blob blob = blobService.get(bean.getId());
    return buildLinkMapWithTransformation(blob, linkParameters);
  }

  @Link(type = BlobRef.class, parameter = {TRANSFORM_SEGMENT}, uri = URI_PATTERN)
  public Map<String, ?> buildLinkWithTransformation(BlobRef bean, Map<String, Object> linkParameters) {
    Blob blob = blobService.get(bean.getId());
    Object transform = linkParameters.get(TRANSFORM_SEGMENT);
    if (transform instanceof Boolean && (Boolean) transform) {
      return buildLinkMapWithTransformation(blob, linkParameters);
    } else {
      return buildLinkMap(blob);
    }
  }
}
