<#-- @ftlvariable name="self" type="java.util.List<com.coremedia.blueprint.elastic.social.cae.controller.CommentWrapper>" -->
<#-- @ftlvariable name="commentsResult" type="com.coremedia.blueprint.elastic.social.cae.controller.CommentsResult" -->
<#-- @ftlvariable name="classCollection" type="java.lang.String" -->
<#assign classCollection=cm.localParameters().classCollection!"" />

<#if self?has_content>
  <ul class="cm-collection cm-collection--comments ${classCollection!""}">
    <#list self as wrapper>
      <@cm.include self=wrapper!cm.UNDEFINED view="asListItem" params={"commentsResult": commentsResult} />
    </#list>
  </ul>
</#if>
