<#-- @ftlvariable name="self" type="com.coremedia.blueprint.elastic.social.cae.controller.CommentWrapper" -->
<#-- @ftlvariable name="commentsResult" type="com.coremedia.blueprint.elastic.social.cae.controller.CommentsResult" -->

<li class="cm-collection__item">
  <@cm.include self=self.comment params={
    "commentingAllowed": commentsResult.isWritingContributionsAllowed()
  } />
  <@cm.include self=self.subComments!cm.UNDEFINED view="asCommentList" params={"commentsResult": commentsResult!cm.UNDEFINED} />
</li>