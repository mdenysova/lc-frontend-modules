<#-- @ftlvariable name="self" type="java.util.List<com.coremedia.elastic.social.api.reviews.ReviewWrapper>" -->
<#-- @ftlvariable name="reviewsResult" type="com.coremedia.blueprint.elastic.social.cae.controller.ReviewsResult" -->
<#-- @ftlvariable name="classCollection" type="java.lang.String" -->
<#assign classCollection=cm.localParameters().classCollection!"" />

<#if self?has_content>
  <ul class="cm-collection cm-collection--reviews ${classCollection!""}">
    <#list self as wrapper>
      <@cm.include self=wrapper!cm.UNDEFINED view="asListItem" params={"reviewsResult": reviewsResult} />
    </#list>
  </ul>
</#if>
