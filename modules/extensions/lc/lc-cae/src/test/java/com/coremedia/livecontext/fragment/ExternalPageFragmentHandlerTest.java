package com.coremedia.livecontext.fragment;

import com.coremedia.blueprint.cae.contentbeans.PageImpl;
import com.coremedia.blueprint.common.contentbeans.Page;
import com.coremedia.blueprint.common.navigation.Linkable;
import com.coremedia.blueprint.common.navigation.Navigation;
import com.coremedia.cache.Cache;
import com.coremedia.cache.CacheKey;
import com.coremedia.livecontext.contentbeans.CMExternalChannel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ExternalPageFragmentHandlerTest extends FragmentHandlerTestBase<ExternalPageFragmentHandler> {

  @Mock
  private CMExternalChannel aboutUsPage;

  @Before
  public void defaultSetup() {
    super.defaultSetup();
    ExternalPageContextStrategy contextStrategy = new ExternalPageContextStrategy();
    Cache cache = new Cache("") {
      @Override
      public <T> T get(CacheKey<T> key) {
        try {
          return key.evaluate(null);
        } catch (Exception e) {
          e.printStackTrace();
        }
        return null;
      }
    };
    contextStrategy.setCache(cache);
    getTestling().setContextStrategy(contextStrategy);
    when(validationService.validate(anyObject())).thenReturn(true);
    doReturn(Collections.singletonList(aboutUsPage)).when(getRootChannelBean()).getChildren();
    when(aboutUsPage.isCatalogPage()).thenReturn(false);
    when(aboutUsPage.getExternalId()).thenReturn("aboutUs");
    when(aboutUsPage.getContext()).thenReturn(aboutUsPage);
  }

  @Override
  protected ExternalPageFragmentHandler createTestling() {
    return new ExternalPageFragmentHandler() {
      @Override
      protected Page asPage(Navigation context, Linkable content) {
        return new PageImpl(context, content, false, getSitesService(), null);
      }
    };
  }

  @Test
  public void testRootChannelCanBeResolved() {
    FragmentParameters params = getFragmentParametersWithExternalPage("");
    ModelAndView modelAndView = getTestling().createModelAndView(params, request);
    assertEquals(getRootChannelBean(), ((PageImpl)modelAndView.getModel().get("cmpage")).getContent());
  }

  @Test
  public void testAboutUsPageCanBeResolved() {
    FragmentParameters params = getFragmentParametersWithExternalPage("aboutUs");
    ModelAndView modelAndView = getTestling().createModelAndView(params, request);
    assertEquals(aboutUsPage, ((PageImpl)modelAndView.getModel().get("cmpage")).getContent());
  }

  @Test
  public void testRootChannelFallback() {
    FragmentParameters params = getFragmentParametersWithExternalPage("unknown-page-id");
    ModelAndView modelAndView = getTestling().createModelAndView(params, request);
    assertEquals(getRootChannelBean(), ((PageImpl)modelAndView.getModel().get("cmpage")).getContent());
  }

  private FragmentParameters getFragmentParametersWithExternalPage(String pageId) {
    String url = "http://localhost:40081/blueprint/servlet/service/fragment/" + STORE_ID + "/" + LOCALE_STRING + "/params;";
    FragmentParameters  params = FragmentParametersFactory.create(url);
    params.setView("default");
    params.setPlacement(PLACEMENT);
    params.setPageId(pageId);
    return params;
  }

}
