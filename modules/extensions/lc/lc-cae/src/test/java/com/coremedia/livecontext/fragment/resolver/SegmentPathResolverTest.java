package com.coremedia.livecontext.fragment.resolver;

import com.coremedia.blueprint.cae.search.SegmentResolver;
import com.coremedia.blueprint.common.contentbeans.CMLinkable;
import com.coremedia.cae.testing.TestInfrastructureBuilder;
import com.coremedia.cap.common.IdHelper;
import com.coremedia.cap.content.Content;
import com.coremedia.cap.multisite.Site;
import com.coremedia.livecontext.ecommerce.asset.AssetUrlProvider;
import com.coremedia.livecontext.fragment.FragmentParameters;
import com.coremedia.livecontext.fragment.FragmentParametersFactory;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SegmentPathResolverTest {

  private static final String CONTENT_REPOSITORY_URL = "classpath:/com/coremedia/livecontext/fragment/resolver/segmentpath-test-content.xml";

  private static TestInfrastructureBuilder.Infrastructure infrastructure;

  private SegmentPathResolver testling;

  private Site site;


  // --- Setup ------------------------------------------------------

  @BeforeClass
  public static void setUpStatic() {
    infrastructure = TestInfrastructureBuilder
            .create()
            .withLinkFormatter()
            .withBeans("classpath:/framework/spring/blueprint-contentbeans.xml")
            .withBeans("classpath:/META-INF/coremedia/livecontext-resolver.xml")
            .withBeans("classpath:/framework/spring/livecontext-services.xml")
            .withBean("segmentResolver", mock(SegmentResolver.class))
            .withBean("assetUrlProvider", mock(AssetUrlProvider.class))
            .withContentRepository(CONTENT_REPOSITORY_URL)
            .build();
  }

  @Before
  public void beforeEachTest() {
    testling = infrastructure.getBean("segmentPathResolver", SegmentPathResolver.class);
    site = mock(Site.class);
  }


  // --- tests ------------------------------------------------------

  @Test
  public void testInclude() {
    FragmentParameters parameters = createFragmentParameters();
    parameters.setExternalReference("cm-segmentpath:!achannel!asubchannel!alinkable");
    assertTrue(testling.include(parameters));
  }

  @Test
  public void testEmpty() {
    FragmentParameters parameters = createFragmentParameters();
    parameters.setExternalReference("cm-segmentpath:");
    LinkableAndNavigation can = testling.resolveExternalRef(parameters, site);
    assertNull(can);
  }

  @Test
  public void testRoot() {
    FragmentParameters parameters = createFragmentParameters();
    parameters.setExternalReference("cm-segmentpath:!root");
    LinkableAndNavigation can = testling.resolveExternalRef(parameters, site);
    checkResult(can, 124, 124);
  }

  @Test
  public void testSubchannel() {
    FragmentParameters parameters = createFragmentParameters();
    parameters.setExternalReference("cm-segmentpath:!root!level1!level2");
    LinkableAndNavigation can = testling.resolveExternalRef(parameters, site);
    checkResult(can, 130, 130);
  }

  @Test
  public void testContentInRoot() {
    mockSegmentResolver(124, "doc", 4);
    FragmentParameters parameters = createFragmentParameters();
    parameters.setExternalReference("cm-segmentpath:!root!doc");
    LinkableAndNavigation can = testling.resolveExternalRef(parameters, site);
    checkResult(can, 124, 4);
  }

  @Test
  public void testContentInSubchannel() {
    mockSegmentResolver(130, "doc", 4);
    FragmentParameters parameters = createFragmentParameters();
    parameters.setExternalReference("cm-segmentpath:!root!level1!level2!doc");
    LinkableAndNavigation can = testling.resolveExternalRef(parameters, site);
    checkResult(can, 130, 4);
  }

  @Test
  public void testContentNotInChannel() {
    FragmentParameters parameters = createFragmentParameters();
    parameters.setExternalReference("cm-segmentpath:!root!level1!doc");
    LinkableAndNavigation can = testling.resolveExternalRef(parameters, site);
    assertNull(can);
  }

  @Test
  public void testContentOnly() {
    FragmentParameters parameters = createFragmentParameters();
    parameters.setExternalReference("cm-segmentpath:!doc");
    LinkableAndNavigation can = testling.resolveExternalRef(parameters, site);
    assertNull(can);
  }

  @Test
  public void testNotAbsoluteSegmentPath() {
    FragmentParameters parameters = createFragmentParameters();
    parameters.setExternalReference("cm-segmentpath:root!level1!doc");
    LinkableAndNavigation can = testling.resolveExternalRef(parameters, site);
    assertNull(can);
  }

  @Test
  public void testRootPath() {
    FragmentParameters parameters = createFragmentParameters();
    parameters.setExternalReference("cm-segmentpath:!");
    LinkableAndNavigation can = testling.resolveExternalRef(parameters, site);
    assertNull(can);
  }

  @Ignore  // Known shortcoming.
  @Test
  public void testEvilSegment() {
    FragmentParameters parameters = createFragmentParameters();
    parameters.setExternalReference("cm-segmentpath:!root!level1!bses!Dokument");
    LinkableAndNavigation can = testling.resolveExternalRef(parameters, site);
    checkResult(can, 128, 8);
  }


  // --- internal ---------------------------------------------------

  private static void mockSegmentResolver(int contextId, String segment, int linkableId) {
    Content content = infrastructure.getContentRepository().getContent(IdHelper.formatContentId(linkableId));
    CMLinkable linkable = infrastructure.getContentBeanFactory().createBeanFor(content, CMLinkable.class);

    SegmentResolver segmentResolver = infrastructure.getBean("segmentResolver", SegmentResolver.class);
    when(segmentResolver.resolveSegment(contextId, segment, CMLinkable.class)).thenReturn(linkable);
  }

  private static FragmentParameters createFragmentParameters() {
    FragmentParameters parameters = FragmentParametersFactory.create("http://localhost:40081/blueprint/servlet/service/fragment/10001/en-US/params;view=asTeaser");
    parameters.setView("asTeaser");
    return parameters;
  }

  private static void checkResult(LinkableAndNavigation result, int expectedNavigation, int expectedContent) {
    assertEquals(expectedNavigation, IdHelper.parseContentId(result.getNavigation().getId()));
    assertEquals(expectedContent, IdHelper.parseContentId(result.getLinkable().getId()));
  }
}
