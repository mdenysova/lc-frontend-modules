package com.coremedia.livecontext.handler;

import com.coremedia.blueprint.base.livecontext.ecommerce.common.Commerce;
import com.coremedia.blueprint.cae.exception.InvalidContentException;
import com.coremedia.blueprint.common.contentbeans.Page;
import com.coremedia.cap.common.InvalidIdException;
import com.coremedia.livecontext.contentbeans.CMExternalChannel;
import com.coremedia.livecontext.ecommerce.catalog.CatalogService;
import com.coremedia.livecontext.ecommerce.common.CommerceConnection;
import com.coremedia.livecontext.ecommerce.common.CommerceException;
import com.coremedia.livecontext.ecommerce.common.CommerceIdProvider;
import com.coremedia.livecontext.handler.ExternalChannelValidityInterceptor;
import com.coremedia.objectserver.web.HandlerHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.verification.VerificationMode;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ExternalChannelValidityInterceptorTest {

  private ExternalChannelValidityInterceptor interceptor;

  @Mock
  private Page page;

  @Mock
  private ModelAndView modelAndView;

  @Mock
  CommerceIdProvider commerceIdProvider;

  @Mock
  CatalogService catalogService;

  @Mock
  private CommerceConnection commerceConnection;

  @Mock
  private CMExternalChannel externalChannel;

  @Before
  public void setUp() throws Exception {
    interceptor = new ExternalChannelValidityInterceptor();
    when(commerceIdProvider.formatCategoryId(any(String.class))).thenReturn("");
    when(commerceConnection.getIdProvider()).thenReturn(commerceIdProvider);
    when(externalChannel.isCatalogPage()).thenReturn(true);
    Commerce.setCurrentConnection(commerceConnection);
  }

  @Test
  public void testNoopModelAndViewEmpty() {
    triggerInterceptor(null);
    verifyNoExternalChannelIsValidated();
  }

  @Test
  public void testNoopRootModelNotPage() {
    when(modelAndView.getModel()).thenReturn(getModelMapWithModelRoot(new Object()));
    triggerInterceptor(modelAndView);
    verifyNoExternalChannelIsValidated();
  }

  @Test
  public void testNoopPageContentNotChannel() {
    when(page.getContent()).thenReturn(new Object());
    when(modelAndView.getModel()).thenReturn(getModelMapWithModelRoot(page));
    triggerInterceptor(modelAndView);
    verifyNoExternalChannelIsValidated();
  }

  @Test
  public void testNoopExternalChannelValid() {
    setUpCommerceEnvironment(true, false);
    when(page.getContent()).thenReturn(externalChannel);
    when(modelAndView.getModel()).thenReturn(getModelMapWithModelRoot(page));
    triggerInterceptor(modelAndView);
    verifyExternalChannelIsValidated();
  }

  @Test(expected = InvalidContentException.class)
  public void testExternalChannelInvalidNoCatalogService() {
    setUpCommerceEnvironment(false, false);
    when(page.getContent()).thenReturn(externalChannel);
    when(modelAndView.getModel()).thenReturn(getModelMapWithModelRoot(page));
    triggerInterceptor(modelAndView);
    verifyExternalChannelIsValidated();
  }

  @Test(expected = InvalidContentException.class)
  public void testExternalChannelInvalidCategoryNotFound() {
    setUpCommerceEnvironment(true, true);
    when(page.getContent()).thenReturn(externalChannel);
    when(modelAndView.getModel()).thenReturn(getModelMapWithModelRoot(page));
    triggerInterceptor(modelAndView);
    verifyExternalChannelIsValidated();
  }

  private void setUpCommerceEnvironment(final boolean setCatalogService, final boolean failOnFindCategory) {
    if (setCatalogService) {
      when(commerceConnection.getCatalogService()).thenReturn(catalogService);
    }
    if (failOnFindCategory) {
      when(catalogService.findCategoryById(any(String.class))).thenThrow(new CommerceException("something went wrong!"));
    }
  }

  private void verifyExternalChannelIsValidated(VerificationMode verificationMode) {
    verify(externalChannel, verificationMode).getExternalId();
  }

  private void verifyExternalChannelIsValidated() {
    verifyExternalChannelIsValidated(times(1));
  }

  private void verifyNoExternalChannelIsValidated() {
    verifyExternalChannelIsValidated(never());
  }

  private void triggerInterceptor(ModelAndView modelAndView) throws InvalidIdException {
    interceptor.postHandle(null, null, null, modelAndView);
  }

  private Map<String, Object> getModelMapWithModelRoot(Object self) {
    Map<String, Object> result = new HashMap<>();
    result.put(HandlerHelper.MODEL_ROOT, self);
    return result;
  }
}
