package com.coremedia.livecontext.handler;

import com.coremedia.blueprint.base.livecontext.ecommerce.common.BaseCommerceIdHelper;
import com.coremedia.blueprint.base.livecontext.ecommerce.common.Commerce;
import com.coremedia.blueprint.cae.exception.InvalidContentException;
import com.coremedia.blueprint.common.contentbeans.Page;
import com.coremedia.livecontext.contentbeans.CMExternalChannel;
import com.coremedia.livecontext.ecommerce.catalog.CatalogService;
import com.coremedia.livecontext.ecommerce.common.CommerceException;
import com.coremedia.objectserver.web.HandlerHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;

public class ExternalChannelValidityInterceptor extends HandlerInterceptorAdapter {

  private static final Logger LOG = LoggerFactory.getLogger(ExternalChannelValidityInterceptor.class);

  @Override
  public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
    if (null == modelAndView) {
      return;
    }
    Object self = HandlerHelper.getRootModel(modelAndView);
    if (!(self instanceof Page)) {
      return;
    }
    Page page = (Page) self;

    Object content = page.getContent();
    if (!(content instanceof CMExternalChannel)) {
      return;
    }

    CMExternalChannel cmExternalChannel = (CMExternalChannel) content;
    if (cmExternalChannel.isCatalogPage()) {
      try {
        CatalogService catalogService = Commerce.getCurrentConnection().getCatalogService();
        if (null != catalogService) {
          // find category to validate used external id
          catalogService.findCategoryById(BaseCommerceIdHelper.getCurrentCommerceIdProvider().formatCategoryId(cmExternalChannel.getExternalId()));
        } else {
          throw new IllegalStateException("catalog service is not present");
        }
      } catch (IllegalStateException | CommerceException e) {
        final String msg = "Trying to render invalid category page, returning " + SC_NOT_FOUND + ".  Page=" + self;
        LOG.debug(msg);
        throw new InvalidContentException(msg, self, e);
      }
    }
  }
}
