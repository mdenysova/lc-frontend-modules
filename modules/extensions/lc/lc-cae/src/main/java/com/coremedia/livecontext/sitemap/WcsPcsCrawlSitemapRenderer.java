package com.coremedia.livecontext.sitemap;

class WcsPcsCrawlSitemapRenderer extends WcsCrawlSitemapRenderer {
  @Override
  protected String toCrawlurl(String url) {
    url = url.replace("https://", "http://"); // crawl url must be http because ibm crawler can not handle https
    return url + "?view=forCrawler";
  }
}
