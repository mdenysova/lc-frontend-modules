package com.coremedia.livecontext.fragment;

import com.google.common.net.HttpHeaders;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.coremedia.blueprint.base.links.UriConstants.Patterns.PATTERN_SEGMENTS;
import static com.coremedia.blueprint.base.links.UriConstants.Prefixes.PREFIX_DYNAMIC;
import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * <p>
 * This {@link Filter filter} enables cross domain ajax requests without the need for a proxy by putting all values from the
 * {@link #setCrossDomainWhitelist(String[]) cross domain whitelist} to the corresponding
 * {@link com.google.common.net.HttpHeaders#ACCESS_CONTROL_ALLOW_ORIGIN http header}.
 * </p>
 * <p>
 * It decides whether a certain request is an ajax request or not by looking at the
 * {@link #setAjaxIndicatorHeaderName(String) ajax indicator header}. Requests with a value of the
 * {@link #setAjaxIndicatorHeaderValue(String) ajax indicator value} are handled as ajax requests. The default
 * ajax indicator header name is {@link #DEFAULT_AJAX_INDICATOR_HEADER_NAME}, the default expected value
 * for that header is {@link #DEFAULT_AJAX_INDICATOR_HEADER_VALUE}.
 * </p>
 * <p>
 * In order to allow script clients, e.g. jquery, to set the ajax indicator header in a cross domain request,
 * this filter provides an HTTP OPTIONS handler, that simply answers an OPTIONS request with the
 * {@link com.google.common.net.HttpHeaders#ACCESS_CONTROL_ALLOW_HEADERS allow headers} header containing
 * the {@link #setAjaxIndicatorHeaderName(String) ajax indicator header name} as value. Remember to enable
 * OPTIONS requests for springs {@link org.springframework.web.servlet.DispatcherServlet dispatcher servlet}.
 * They are disabled by default!
 * </p>
 */
@RequestMapping
public class CrossDomainEnabler implements Filter {

  public static final String DYNAMIC_FRAGMENT_URI_PATTERN = '/' + PREFIX_DYNAMIC + "/{all:"+ PATTERN_SEGMENTS +"}";
  public static final String DEFAULT_AJAX_INDICATOR_HEADER_NAME = "X-Requested-With";
  public static final String DEFAULT_AJAX_INDICATOR_HEADER_VALUE = "XMLHttpRequest";

  private static final String ALLOW_HEADER_VALUE = "OPTIONS, GET, POST";

  private String[] crossDomainWhitelist;
  private String ajaxIndicatorHeaderName = DEFAULT_AJAX_INDICATOR_HEADER_NAME;
  private String ajaxIndicatorHeaderValue = DEFAULT_AJAX_INDICATOR_HEADER_VALUE;

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    if (servletRequest instanceof HttpServletRequest) {
      HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
      if (isAjax(httpServletRequest) && servletResponse instanceof HttpServletResponse) {
        setCorsHeaders(httpServletRequest, (HttpServletResponse) servletResponse);
      }
    }

    filterChain.doFilter(servletRequest, servletResponse);
  }

  @Override
  public void destroy() {
  }

  @RequestMapping(value = DYNAMIC_FRAGMENT_URI_PATTERN, method = RequestMethod.OPTIONS)
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public ResponseEntity tryOptions(HttpServletRequest request, HttpServletResponse response) {
    setCorsHeaders(request, response);
    return null;
  }

  private void setCorsHeaders(HttpServletRequest request, HttpServletResponse response) {
    String originHeader = request.getHeader(HttpHeaders.ORIGIN);
    String origin = checkOrigin(originHeader);
    if (origin != null) {
      response.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, origin);
      response.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, ajaxIndicatorHeaderName);
      response.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
      response.setHeader(HttpHeaders.ALLOW, ALLOW_HEADER_VALUE);
    }
  }

  private String checkOrigin(String origin) {
    for (String allowedOrigin : crossDomainWhitelist) {
      if (allowedOrigin.equals(origin)) {
        return origin;
      }
    }
    return null;
  }

  private boolean isAjax(HttpServletRequest request) {
    return ajaxIndicatorHeaderValue.equals(request.getHeader(ajaxIndicatorHeaderName));
  }

  @Required
  public void setCrossDomainWhitelist(@Nullable String[] crossDomainWhitelist) {
    this.crossDomainWhitelist = crossDomainWhitelist == null ? new String[0] : crossDomainWhitelist;
  }

  public void setAjaxIndicatorHeaderName(@Nonnull String ajaxIndicatorHeaderName) {
    checkArgument(isNotBlank(ajaxIndicatorHeaderName), "The http header name for the Ajax indicator must not be blank.");
    this.ajaxIndicatorHeaderName = ajaxIndicatorHeaderName;
  }

  public void setAjaxIndicatorHeaderValue(@Nonnull String ajaxIndicatorHeaderValue) {
    checkArgument(isNotBlank(ajaxIndicatorHeaderValue), "The expected http header value for the Ajax indicator must not be blank.");
    this.ajaxIndicatorHeaderValue = ajaxIndicatorHeaderValue;
  }
}
