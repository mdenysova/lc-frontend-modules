package com.coremedia.livecontext.validation;

import com.coremedia.blueprint.common.services.validation.AbstractValidator;
import com.coremedia.livecontext.contentbeans.LiveContextExternalChannel;
import com.coremedia.livecontext.ecommerce.common.NotFoundException;
import com.google.common.base.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.annotation.Nullable;

/**
 * {@link com.coremedia.livecontext.contentbeans.CMExternalChannel} may link to categories that
 * are not existent on the remote commerce system anymore. This
 * {@link com.coremedia.blueprint.common.services.validation.Validator validator} can be used to
 * {@link com.coremedia.blueprint.common.services.validation.ValidationService#filterList(java.util.List) filter out}
 * those objects silently, so that the layout of a web page containing such a collection will not be broken.
 */
public class EmptyCategoryValidator extends AbstractValidator<LiveContextExternalChannel> {
  private static final Logger LOG = LoggerFactory.getLogger(EmptyCategoryPredicate.class);
  
  @Override
  protected Predicate createPredicate() {
    return new EmptyCategoryPredicate();
  }

  @Override
  public boolean supports(Class<?> clazz) {
    return LiveContextExternalChannel.class.isAssignableFrom(clazz);
  }

  private class EmptyCategoryPredicate implements Predicate<LiveContextExternalChannel> {
    @Override
    public boolean apply(@Nullable LiveContextExternalChannel externalChannel) {
      try {
        if (externalChannel != null && externalChannel.isCatalogPage()) {
          String externalId = externalChannel.getExternalId();
          if (StringUtils.isEmpty(externalId) || StringUtils.isEmpty(externalId.trim())) {
            LOG.warn("external id property in external page document is empty: {}", externalChannel.getContent().getPath());
            return false;
          }
          // simply try to get category, will throw Exception if not available
          externalChannel.getCategory();
        }
        return true;

      } catch (NotFoundException e) {
        LOG.debug("Could not get an e-Commerce category for {}", externalChannel.getContent().getPath());
      }
      return false;
    }
  }
}
