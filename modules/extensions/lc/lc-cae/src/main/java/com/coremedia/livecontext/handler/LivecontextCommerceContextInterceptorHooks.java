package com.coremedia.livecontext.handler;

import com.coremedia.blueprint.ecommerce.cae.CommerceContextInterceptorHooks;
import com.coremedia.cap.content.ContentRepository;
import com.coremedia.cap.multisite.Site;
import com.coremedia.livecontext.navigation.CompositeNameHelper;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Required;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * LiveContext specific store context issues.
 */
public class LivecontextCommerceContextInterceptorHooks implements CommerceContextInterceptorHooks, InitializingBean {
  public static final String QUERY_PARAMETER_PREVIEW_TOKEN = "previewToken";
  public static final String QUERY_PARAMETER_COMPOSITE_NAME = "compositeName";

  private ContentRepository contentRepository;
  private boolean isPreview;

  @Required
  public void setContentRepository(ContentRepository contentRepository) {
    this.contentRepository = contentRepository;
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    Objects.requireNonNull(contentRepository);
    isPreview = contentRepository.isContentManagementServer();
  }

  @Override
  public void updateStoreContext(HttpServletRequest request) {
    if (isPreview && request.getParameter(QUERY_PARAMETER_PREVIEW_TOKEN) != null) {
      request.setAttribute(PreviewTokenAppendingLinkTransformer.HAS_PREVIEW_TOKEN, true);
    }
  }

  @Override
  public void enhanceStoreContext(Site site, HttpServletRequest request) {
    String compositeName = request.getParameter(QUERY_PARAMETER_COMPOSITE_NAME);
    CompositeNameHelper.setCurrentCompositeName(compositeName);
  }
}
