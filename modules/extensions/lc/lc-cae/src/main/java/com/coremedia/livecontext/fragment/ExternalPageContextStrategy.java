package com.coremedia.livecontext.fragment;

import com.coremedia.blueprint.base.navigation.context.ContextStrategy;
import com.coremedia.blueprint.common.contentbeans.CMObject;
import com.coremedia.blueprint.common.navigation.Linkable;
import com.coremedia.blueprint.common.navigation.Navigation;
import com.coremedia.cache.Cache;
import com.coremedia.cache.CacheKey;
import com.coremedia.livecontext.contentbeans.CMExternalChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;

/**
 * A {@link ContextStrategy} that finds a context for a basic shop page identified by its page id.
 * A recursive traversal over the channel children will be made starting from the root channel.
 * Only documents of type CMExternalChannel will be taken into account (apart from the root channel
 * itself, that can be a CMChannel document - then it assumed external id is an empty string).
 * All findings will be cached with full dependency tracking.
 */
public class ExternalPageContextStrategy implements ContextStrategy<String, Navigation> {

  private Cache cache;

  @Required
  public void setCache(Cache cache) {
    this.cache = cache;
  }

  @Override
  public Navigation findAndSelectContextFor(String pageId, Navigation rootChannel) {
    return selectContext(rootChannel, findContextsFor(pageId, rootChannel));
  }

  @Override
  public List<Navigation> findContextsFor(@Nonnull String pageId) {
    throw new IllegalAccessError("method findContextsFor(pageId) is not supported");
  }

  @Override
  public List<Navigation> findContextsFor(@Nonnull String pageId, @Nullable Navigation rootChannel) {
    Navigation navigation = cache.get(new ExternalPageContextCacheKey(pageId, rootChannel));
    return Collections.singletonList(navigation);
  }

  @Override
  public Navigation selectContext(Navigation rootChannel, List<? extends Navigation> candidates) {
    return candidates != null && !candidates.isEmpty() ? candidates.get(0) : null;
  }

  private static String debug(Navigation navigation) {
    if (navigation instanceof CMObject && ((CMObject) navigation).getContent() != null) {
      return ((CMObject) navigation).getContent().getPath();
    }
    return navigation+"";
  }

  public static class ExternalPageContextCacheKey extends CacheKey<Navigation> {

    private final Logger LOG = LoggerFactory.getLogger(ExternalPageContextCacheKey.class);

    protected Navigation rootChannel;
    protected String pageId;

    public ExternalPageContextCacheKey(String pageId, Navigation rootChannel) {
      this.rootChannel = rootChannel;
      this.pageId = pageId != null ? pageId.trim() : "";
    }

    private Navigation findContextRecursively(String expectedPageId, Navigation channel) {
      if (channel != null) {
        LOG.debug("Checking if channel {} represents external page {}", debug(channel), expectedPageId);

        // if channel == rootChannel && (!rootChannel instanceOf CMExternalChannel) we assume "" as the actual page id
        String actualPageId = (channel instanceof CMExternalChannel) ? ((CMExternalChannel)channel).getExternalId() : "";
        if (actualPageId == null) {
          actualPageId = "";
        }
        actualPageId = actualPageId.trim();

        if (actualPageId.equals(expectedPageId)) {
          LOG.debug("Found channel {} for external page {}", debug(channel), expectedPageId);
          return channel;
        }
        for (Linkable child : channel.getChildren()) {
          if (child instanceof CMExternalChannel && !((CMExternalChannel) child).isCatalogPage()) {
            Navigation navigation = findContextRecursively(expectedPageId, (CMExternalChannel) child);
            if (navigation != null) {
              return navigation;
            }
          }
        }
      }
      return null;
    }

    public Navigation evaluate(Cache cache) throws Exception {
      Navigation context = findContextRecursively(pageId, rootChannel);
      return context != null ? context : rootChannel;
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      ExternalPageContextCacheKey that = (ExternalPageContextCacheKey) o;
      if (rootChannel != null ? !rootChannel.equals(that.rootChannel) : that.rootChannel != null) {
        return false;
      }
      //noinspection RedundantIfStatement
      if (pageId != null ? !pageId.equals(that.pageId) : that.pageId != null) {
        return false;
      }
      return true;
    }

    public int hashCode() {
      int result = rootChannel != null ? rootChannel.hashCode() : 0;
      result = 31 * result + (pageId != null ? pageId.hashCode() : 0);
      return result;
    }
  }
}
