package com.coremedia.livecontext.ecommerce.ibm.search;

public class WcSuggestion {
  private int frequency;
  private String term;

  public int getFrequency() {
    return frequency;
  }

  public void setFrequency(int frequency) {
    this.frequency = frequency;
  }

  public String getTerm() {
    return term;
  }

  public void setTerm(String term) {
    this.term = term;
  }
}
