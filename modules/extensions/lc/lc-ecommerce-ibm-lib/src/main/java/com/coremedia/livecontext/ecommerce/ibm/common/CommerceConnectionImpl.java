package com.coremedia.livecontext.ecommerce.ibm.common;

import com.coremedia.blueprint.base.livecontext.ecommerce.common.BaseCommerceConnection;

/**
 * An IBM specific connection class. Manages the IBM vendor specific properties.
 */
public class CommerceConnectionImpl extends BaseCommerceConnection {
}
