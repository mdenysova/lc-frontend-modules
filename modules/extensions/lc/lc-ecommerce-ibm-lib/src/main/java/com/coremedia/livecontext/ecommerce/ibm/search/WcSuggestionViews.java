package com.coremedia.livecontext.ecommerce.ibm.search;

import java.util.List;

public class WcSuggestionViews {
  private List<WcSuggestionView> suggestionView;

  public List<WcSuggestionView> getSuggestionView() {
    return suggestionView;
  }

  public void setSuggestionView(List<WcSuggestionView> suggestionView) {
    this.suggestionView = suggestionView;
  }
}
