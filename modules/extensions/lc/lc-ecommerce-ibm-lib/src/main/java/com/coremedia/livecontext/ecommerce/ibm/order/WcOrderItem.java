package com.coremedia.livecontext.ecommerce.ibm.order;

public class WcOrderItem {
  
  private String contractId;
  private String currency;
  private String offerID;
  private String orderItemId;
  private String orderItemInventoryStatus;
  private String orderItemPrice;
  private String orderItemStatus;
  private String partNumber;
  private String productId;
  private String quantity;
  private String salesTax;
  private String salesTaxCurrency;
  private String shippingCharge;
  private String shippingChargeCurrency;
  private String shippingTax;
  private String shippingTaxCurrency;
  private String unitPrice;
  private String unitQuantity;
  private String unitUom;

  public String getContractId() {
    return contractId;
  }

  public void setContractId(String contractId) {
    this.contractId = contractId;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getOfferID() {
    return offerID;
  }

  public void setOfferID(String offerID) {
    this.offerID = offerID;
  }

  public String getOrderItemId() {
    return orderItemId;
  }

  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }

  public String getOrderItemInventoryStatus() {
    return orderItemInventoryStatus;
  }

  public void setOrderItemInventoryStatus(String orderItemInventoryStatus) {
    this.orderItemInventoryStatus = orderItemInventoryStatus;
  }

  public String getOrderItemPrice() {
    return orderItemPrice;
  }

  public void setOrderItemPrice(String orderItemPrice) {
    this.orderItemPrice = orderItemPrice;
  }

  public String getOrderItemStatus() {
    return orderItemStatus;
  }

  public void setOrderItemStatus(String orderItemStatus) {
    this.orderItemStatus = orderItemStatus;
  }

  public String getPartNumber() {
    return partNumber;
  }

  public void setPartNumber(String partNumber) {
    this.partNumber = partNumber;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getQuantity() {
    return quantity;
  }

  public void setQuantity(String quantity) {
    this.quantity = quantity;
  }

  public String getSalesTax() {
    return salesTax;
  }

  public void setSalesTax(String salesTax) {
    this.salesTax = salesTax;
  }

  public String getSalesTaxCurrency() {
    return salesTaxCurrency;
  }

  public void setSalesTaxCurrency(String salesTaxCurrency) {
    this.salesTaxCurrency = salesTaxCurrency;
  }

  public String getShippingCharge() {
    return shippingCharge;
  }

  public void setShippingCharge(String shippingCharge) {
    this.shippingCharge = shippingCharge;
  }

  public String getShippingChargeCurrency() {
    return shippingChargeCurrency;
  }

  public void setShippingChargeCurrency(String shippingChargeCurrency) {
    this.shippingChargeCurrency = shippingChargeCurrency;
  }

  public String getShippingTax() {
    return shippingTax;
  }

  public void setShippingTax(String shippingTax) {
    this.shippingTax = shippingTax;
  }

  public String getShippingTaxCurrency() {
    return shippingTaxCurrency;
  }

  public void setShippingTaxCurrency(String shippingTaxCurrency) {
    this.shippingTaxCurrency = shippingTaxCurrency;
  }

  public String getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(String unitPrice) {
    this.unitPrice = unitPrice;
  }

  public String getUnitQuantity() {
    return unitQuantity;
  }

  public void setUnitQuantity(String unitQuantity) {
    this.unitQuantity = unitQuantity;
  }

  public String getUnitUom() {
    return unitUom;
  }

  public void setUnitUom(String unitUom) {
    this.unitUom = unitUom;
  }
}
