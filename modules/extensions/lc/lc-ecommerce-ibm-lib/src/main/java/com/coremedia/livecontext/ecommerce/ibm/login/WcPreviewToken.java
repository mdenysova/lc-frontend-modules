package com.coremedia.livecontext.ecommerce.ibm.login;

/**
 * The preview token for the WCS.
 */
public class WcPreviewToken {

  private String previewToken;

  public String getPreviewToken() {
    return previewToken;
  }

  public void setPreviewToken(String previewToken) {
    this.previewToken = previewToken;
  }

}
