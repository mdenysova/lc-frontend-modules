package com.coremedia.livecontext.ecommerce.ibm.catalog;

public class WcPartNumber {
  String partNumber;

  public WcPartNumber(String partNumber) {
    this.partNumber = partNumber;
  }

  public String getPartNumber() {
    return partNumber;
  }

  public void setPartNumber(String partNumber) {
    this.partNumber = partNumber;
  }
}
