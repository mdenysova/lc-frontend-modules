package com.coremedia.livecontext.studio {

import com.coremedia.livecontext.studio.action.CollectionViewModelActionTest;
import com.coremedia.livecontext.studio.collectionview.CatalogCollectionViewTest;
import com.coremedia.livecontext.studio.components.link.CatalogLinkPropertyFieldTest;
import com.coremedia.livecontext.studio.components.link.ProductLinksPropertyFieldTest;
import com.coremedia.livecontext.studio.components.product.ProductNameTextFieldTest;
import com.coremedia.livecontext.studio.forms.ExternalNavigationFormTest;

import flexunit.framework.TestSuite;

public class TestSuite {
  public static function suite():flexunit.framework.TestSuite {
    var suite:flexunit.framework.TestSuite = new flexunit.framework.TestSuite();
    suite.addTestSuite(CatalogCollectionViewTest);
    suite.addTestSuite(CollectionViewModelActionTest);
    suite.addTestSuite(ProductNameTextFieldTest);

    suite.addTestSuite(CatalogLinkPropertyFieldTest);
    suite.addTestSuite(ProductLinksPropertyFieldTest);
    suite.addTestSuite(ExternalNavigationFormTest);


    //todo: this test is not stable.
//    suite.addTestSuite(ProductTeaserDocumentFormTest);
    //todo: I'm too stupid to mock requests, so I disabled this test first
//    suite.addTestSuite(ProductTeaserSettingsFormTest);

    return suite;
  }
}
}
