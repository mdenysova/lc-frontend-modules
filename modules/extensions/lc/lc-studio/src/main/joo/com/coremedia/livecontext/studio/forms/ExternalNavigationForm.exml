<?xml version="1.0" encoding="UTF-8"?>
<exml:component xmlns:exml="http://www.jangaroo.net/exml/0.8"
                xmlns="exml:ext.config"
                xmlns:ui="exml:com.coremedia.ui.config"
                xmlns:editor="exml:com.coremedia.cms.editor.sdk.config"
                xmlns:u="exml:untyped"
                xmlns:livecontext="exml:com.coremedia.ecommerce.studio.config"
                xmlns:bp="exml:com.coremedia.blueprint.studio.config.components"
                xmlns:bpforms="exml:com.coremedia.blueprint.studio.config"
                xmlns:qc="exml:com.coremedia.blueprint.studio.config.quickcreate"
                baseClass="com.coremedia.livecontext.studio.forms.ExternalNavigationFormBase">

  <exml:import class="com.coremedia.blueprint.studio.BlueprintStudio_properties"/>
  <exml:import class="com.coremedia.blueprint.studio.BlueprintTabs_properties"/>
  <exml:import class="com.coremedia.blueprint.studio.CustomLabels_properties"/>
  <exml:import class="com.coremedia.blueprint.studio.BlueprintDocumentTypes_properties"/>
  <exml:import class="com.coremedia.livecontext.studio.LivecontextStudioPlugin_properties"/>
  <exml:import class="com.coremedia.ui.data.beanFactory"/>
  <exml:import class="com.coremedia.ecommerce.studio.CatalogModel"/>
  <exml:import class="com.coremedia.ecommerce.studio.helper.CatalogHelper"/>
  <exml:import class="com.coremedia.cms.editor.sdk.editorContext"/>
  <exml:import class="com.coremedia.ui.data.ValueExpressionFactory"/>
  <exml:import class="com.coremedia.cap.content.Content"/>
  <exml:import class="com.coremedia.blueprint.studio.forms.CMChannelFormBase"/>

  <exml:constant name="RADIO_GROUP_ITEM_ID" value="catalogRadioGroup"/>
  <exml:constant name="CATALOG_LINK_ITEM_ID" value="catalogLink"/>
  <exml:constant name="CHILDREN_LIST_ITEM_ID" value="children"/>
  <exml:constant name="VISIBILITY_ITEM_ID" value="visibility"/>
  <exml:constant name="EXTERNAL_ID_ITEM_ID" value="externalIdString"/>
  <exml:constant name="EXTERNAL_URI_PATH_ITEM_ID" value="externalUriPath"/>

  <exml:cfg name="catalogExpression" type="com.coremedia.ui.data.ValueExpression">
    <exml:description>
       An optional ValueExpression for the catalog flag. used by component test
    </exml:description>
  </exml:cfg>

  <exml:var name="catalogExpression" type="com.coremedia.ui.data.ValueExpression"
            value="{createCatalogExpression(config)}"/>
  <exml:var name="radioButtonFormName" value="{getNameId()}"/>

  <editor:collapsibleFormPanel title="{CustomLabels_properties.INSTANCE.PropertyGroup_Navigation_label}"
                               itemId="cmExternalChannelExternalIdForm">
    <items>
      <bp:extendedLinkListPropertyField bindTo="{config.bindTo}"
                                        propertyName="children"
                                        itemId="{CHILDREN_LIST_ITEM_ID}">
        <bp:additionalToolbarItems>
          <tbseparator/>
          <qc:quickCreateToolbarButton contentType="CMExternalChannel" bindTo="{config.bindTo}"
                                       propertyName="children"/>
        </bp:additionalToolbarItems>
      </bp:extendedLinkListPropertyField>
      <spacer height="6"/>
      <bpforms:visibilityDocumentForm bindTo="{config.bindTo}"
                                      itemId="{VISIBILITY_ITEM_ID}">
        <plugins>
          <ui:bindPropertyPlugin bidirectional="true"
                                 bindTo="{CMChannelFormBase.getIsRootChannelValueExpression(config.bindTo)}"
                                 componentProperty="hidden"/>
        </plugins>
      </bpforms:visibilityDocumentForm>
      <editor:collapsibleFormPanel title="{LivecontextStudioPlugin_properties.INSTANCE.EnhancedPageGroup_title}"
                                   itemId="cmExternalChannelEnhancedPage">
        <items>
          <radiogroup columns="1"
                      itemId="{RADIO_GROUP_ITEM_ID}"
                      autoWidth="true"
                      fieldLabel="{LivecontextStudioPlugin_properties.INSTANCE.PageType_label}">
            <plugins>
              <ui:bindPropertyPlugin componentProperty="value"
                                     bindTo="{catalogExpression}"
                                     ifUndefined="true"
                                     transformer="{stateToRadio}"
                                     reverseTransformer="{radioToState}"
                                     bidirectional="true"/>
            </plugins>
            <items>
              <radio inputValue="{CATALOG_PAGE_SETTING}"
                     itemId="{CATALOG_PAGE_SETTING}"
                     name="{radioButtonFormName}"
                     boxLabel="{LivecontextStudioPlugin_properties.INSTANCE.CMExternalChannel_settings_catalogPage}"
                     hideLabel="true"
                     u:inGroup="true">
                <plugins>
                  <editor:bindDisablePlugin bindTo="{config.bindTo}"
                                            forceReadOnlyValueExpression="{config.forceReadOnlyValueExpression}"/>
                </plugins>
              </radio>
              <radio inputValue="{OTHER_PAGE_SETTING}"
                     itemId="{OTHER_PAGE_SETTING}"
                     name="{radioButtonFormName}"
                     hideLabel="true"
                     boxLabel="{LivecontextStudioPlugin_properties.INSTANCE.CMExternalChannel_settings_otherPage}"
                     u:inGroup="true">
                <plugins>
                  <editor:bindDisablePlugin bindTo="{config.bindTo}"
                                            forceReadOnlyValueExpression="{config.forceReadOnlyValueExpression}"/>
                </plugins>
              </radio>
            </items>
          </radiogroup>
          <livecontext:catalogLinkPropertyField itemId="{CATALOG_LINK_ITEM_ID}"
                                                ddGroup="{CatalogHelper.DD_GROUP_CATALOG}"
                                                propertyName="externalId"
                                                catalogObjectType="{CatalogModel.TYPE_CATEGORY}"
                                                openLinkSources="{CatalogHelper.getInstance().openCatalog}"
                                                emptyText="{LivecontextStudioPlugin_properties.INSTANCE.Category_Link_empty_text}">
            <plugins mode="append">
              <ui:bindVisibilityPlugin bindTo="{catalogExpression}"/>
            </plugins>
          </livecontext:catalogLinkPropertyField>
          <editor:stringPropertyField propertyName="externalId"
                                      changeBuffer="1000"
                                      itemId="{EXTERNAL_ID_ITEM_ID}">
            <plugins mode="append">
              <ui:bindVisibilityPlugin bindTo="{negate(catalogExpression)}"/>
            </plugins>
          </editor:stringPropertyField>
          <editor:stringPropertyField propertyName="localSettings.externalUriPath"
                                      changeBuffer="1000"
                                      itemId="{EXTERNAL_URI_PATH_ITEM_ID}">
            <plugins mode="append">
              <ui:bindVisibilityPlugin bindTo="{negate(catalogExpression)}"/>
            </plugins>
          </editor:stringPropertyField>
        </items>
      </editor:collapsibleFormPanel>
    </items>
  </editor:collapsibleFormPanel>
</exml:component>
