package com.coremedia.livecontext.navigation;

public final class PagePrefixContentKeywords {

  public static final String PAGEGRID_SETTING_NAME = "pagegrid";

  public static final String SETTINGS_SETTING_NAME = "settings";
}
