<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.contentbeans.CMSitemap" -->
DEBUG!
<#if self.root?has_content>
<div class="cm-box">

  <@cm.include self=self view="headline" params={"classHeadline": "cm-box__header"} />

  <div class="cm-box__content">
      <ul class="cm-collection cm-collection--sitemap">
        <@cm.include self=self.root view="asLinkListItem" params={
        "maxDepth": bp.setting(self, "sitemap_depth", 3),
        "isRoot": true
        }/>
      </ul>
  </div>
</div>
</#if>