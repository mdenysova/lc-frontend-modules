<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.contentbeans.CMVideo" -->

<div class="cm-box"<@cm.metadata self.content />>
  <@cm.include self=self view="video" />
</div>