<#-- @ftlvariable name="self" type="com.coremedia.livecontext.contentbeans.CMMarketingSpot" -->
<#-- @ftlvariable name="metadata" type="java.util.List" -->

<div class="cm-teaser"<@cm.metadata (metadata![]) + [self.content] />>
  <@bp.optionalLink href="${cm.getLink(self)}">
    <div class="cm-teaser__title cm-heading2 cm-heading2--boxed"<@cm.metadata "properties.teaserTitle" />>${self.teaserTitle}</div>
  </@bp.optionalLink>
</div>