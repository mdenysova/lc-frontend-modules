<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.navigation.Navigation" -->
<#-- @ftlvariable name="fragmentParameter" type="com.coremedia.livecontext.fragment.FragmentParameters" -->

<!-- CoreMedia Metadata Header -->
<title>${fragmentParameter.metaDataTitle}</title>
<meta name="description" content="${fragmentParameter.metaDataDescription}"/>
<meta name="keywords" content="${fragmentParameter.metaDataKeywords}"/>
<!-- /CoreMedia Metadata Header -->
