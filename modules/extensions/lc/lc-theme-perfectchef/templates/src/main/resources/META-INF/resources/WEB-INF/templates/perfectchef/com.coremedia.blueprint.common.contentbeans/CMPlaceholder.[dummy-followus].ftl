<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.contentbeans.CMPlaceholder" -->

<div class="cm-followus"<@cm.metadata self.content />>
  <h3 class="cm-followus__title"><@bp.message "FollowUs_Label" /></h3>
  <div class="cm-followus__icon cm-icon cm-icon--nospace">
    <a href="https://www.facebook.com/coremedia" title="facebook">
      <i class="cm-icon__symbol icon-facebook-social-full"></i>
      <span class="cm-icon__info cm-visuallyhidden"><@bp.message "facebook" /></span>
    </a>
  </div>
  <div class="cm-followus__icon cm-icon cm-icon--nospace">
    <a href="https://twitter.com/perfectchefus" title="twitter">
      <i class="cm-icon__symbol icon-twitter-social-full"></i>
      <span class="cm-icon__info cm-visuallyhidden"><@bp.message "twitter" /></span>
    </a>
  </div>
  <div class="cm-followus__icon cm-icon cm-icon--nospace">
    <a href="http://www.pinterest.com/perfectchefus/" title="pinterest">
      <i class="cm-icon__symbol icon-pinterest-social-full"></i>
      <span class="cm-icon__info cm-visuallyhidden"><@bp.message "pinterest" /></span>
    </a>
  </div>
  <div class="cm-followus__icon cm-icon cm-icon--nospace">
    <a href="http://www.instagram.com/perfectchefus/" title="instagram">
      <i class="cm-icon__symbol icon-instagram-social-full"></i>
      <span class="cm-icon__info cm-visuallyhidden"><@bp.message "instagram" /></span>
    </a>
  </div>
</div>