<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.contentbeans.CMDownload" -->

<@bp.optionalLink href="${cm.getLink(self.data!self)}">
  <#if self.teaserTitle?has_content>
  <div <@cm.metadata "properties.teaserTitle" />>${self.teaserTitle}</div>
  </#if>
</@bp.optionalLink>
