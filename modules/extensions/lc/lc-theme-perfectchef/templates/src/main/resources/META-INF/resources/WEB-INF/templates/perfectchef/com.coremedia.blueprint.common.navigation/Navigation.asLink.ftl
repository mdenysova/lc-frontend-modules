<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.navigation.Navigation" -->
<#-- @ftlvariable name="cssClass" type="java.lang.String" -->

<#-- same as CMTeasable.asLink but without metadata for LiveContextNavigation links -->
<#assign cssClass=cm.localParameters().cssClass!"" />
<#assign target="">
<#if cm.localParameters().openInTab!false>
  <#assign target=' target="_blank"'>
</#if>

<a class="${cssClass}" href="${cm.getLink(self!cm.UNDEFINED)}"${target}>${self.title!""}</a>
