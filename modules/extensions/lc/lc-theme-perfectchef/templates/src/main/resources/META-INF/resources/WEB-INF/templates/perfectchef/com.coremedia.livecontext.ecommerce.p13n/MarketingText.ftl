<#-- @ftlvariable name="self" type="com.coremedia.livecontext.ecommerce.p13n.MarketingText" -->

<div class="cm-box">
    <div class="cm-box__content">
        <span><@cm.unescape (self.text)!"" /></span>
    </div>
</div>
