/**
 * Includes all javascripts required for theme perfectchef spring
 * (this file is a placeholder, the inclusion is defined in exported content)
 *
 * To be able to use javascript frameworks without conflicts, we first include all vendor
 * specific javascripts of all themes in the order they need to be loaded (e.g. basic-vendor.js, perfectchef-vendor.js)
 *
 * After all vendor specific javascript is loaded, coremedia specific javascript is being loaded.
 * All coremedia specific javascripts access javascript frameworks via an own namespace. So the first javascript loaded
 * will perform all actions necessary to avoid conflicts (e.g. if jQuery is being used call noConflict(true) in store
 * the result in the namespace to be used.
 */