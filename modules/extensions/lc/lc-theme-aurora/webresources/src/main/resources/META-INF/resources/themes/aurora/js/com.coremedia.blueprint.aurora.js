/* --- create own namespace in javascript for own stuff ------------------------------------------------------------- */
var coremedia = (function(module) { return module; }(coremedia || {}));
coremedia.blueprint = (function(module) { return module; }(coremedia.blueprint || {}));
coremedia.blueprint.aurora = (function(module) {
  // define specific functionality here
  return module;
}(coremedia.blueprint.aurora || {}));

/* --- document ready ----------------------------------------------------------------------------------------------- */
coremedia.blueprint.$(function () {

  // define actions performed on DOM ready
});