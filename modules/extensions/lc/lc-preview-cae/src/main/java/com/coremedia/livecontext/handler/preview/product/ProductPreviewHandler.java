package com.coremedia.livecontext.handler.preview.product;

import com.coremedia.blueprint.base.livecontext.ecommerce.common.BaseCommerceIdHelper;
import com.coremedia.blueprint.base.livecontext.ecommerce.common.Commerce;
import com.coremedia.blueprint.cae.handlers.PreviewHandler;
import com.coremedia.cap.multisite.Site;
import com.coremedia.cap.multisite.SitesService;
import com.coremedia.id.IdProvider;
import com.coremedia.livecontext.context.ProductInSite;
import com.coremedia.livecontext.ecommerce.catalog.Product;
import com.coremedia.livecontext.ecommerce.common.CommerceBeanFactory;
import com.coremedia.livecontext.ecommerce.common.StoreContext;
import com.coremedia.livecontext.ecommerce.common.StoreContextProvider;
import com.coremedia.livecontext.navigation.LiveContextNavigationFactory;
import com.coremedia.objectserver.beans.ContentBeanFactory;
import com.coremedia.objectserver.dataviews.DataViewFactory;
import com.coremedia.objectserver.web.HandlerHelper;
import com.coremedia.objectserver.web.IdRedirectHandlerBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Todo: Implement product preview handling in PreviewHandler using Commerce Id Scheme
 */
@Named
@RequestMapping
public class ProductPreviewHandler extends IdRedirectHandlerBase {
  private static final Logger LOG = LoggerFactory.getLogger(ProductPreviewHandler.class);

  public static final String URI_PATTERN = "/preview/product";

  @Inject
  private SitesService sitesService;

  @Inject
  private LiveContextNavigationFactory liveContextNavigationFactory;

  @Override
  @Inject
  public void setIdProvider(IdProvider idProvider) {
    super.setIdProvider(idProvider);
  }

  @Override
  @Inject
  public void setContentBeanFactory(ContentBeanFactory contentBeanFactory) {
    super.setContentBeanFactory(contentBeanFactory);
  }

  @Override
  @Inject
  public void setDataViewFactory(DataViewFactory dataViewFactory) {
    super.setDataViewFactory(dataViewFactory);
  }

  @RequestMapping(value = URI_PATTERN)
  public ModelAndView handleId(@RequestParam(value = "id", required = true) String id,
                               @RequestParam(value = "site", required = false) String siteId,
                               HttpServletRequest request) throws IOException {
    ProductInSite productInSite = null;

    try {
      if (BaseCommerceIdHelper.isProductId(id) || BaseCommerceIdHelper.isSkuId(id)) {
        Site site = sitesService.getSite(siteId);
        if (site != null) {
          StoreContext storeContext = getStoreContextProvider().getCurrentContext();
          if (storeContext != null) {
            Product product = (Product) getCommerceBeanFactory().createBeanFor(id, storeContext);
            productInSite = liveContextNavigationFactory.createProductInSite(product, site);
          } else {
            FormattingTuple ft = MessageFormatter.format("StoreContext for Product with ID '{}' for Site with ID '{}' could not be resolved", id, siteId);
            LOG.error(ft.getMessage());
            return HandlerHelper.badRequest(ft.getMessage());
          }
        }
      }
    } catch (Exception exception) { // NOSONAR
      LOG.error("An exception '{}' occurred resolving Product with ID '{}' for Site with ID '{}' ", exception, id, siteId);
      throw exception;
    }

    if (productInSite != null) {
      request.setAttribute(PreviewHandler.REQUEST_ATTR_IS_STUDIO_PREVIEW, true);
      return HandlerHelper.redirectTo(productInSite);
    } else {
      FormattingTuple ft = MessageFormatter.format("Product with ID '{}' for Site with ID '{}' could not be resolved", id, siteId);
      LOG.error(ft.getMessage());
      return HandlerHelper.notFound(ft.getMessage());
    }
  }

  @Override
  protected boolean isPermitted(Object o, String s) {
    return true;
  }

  public StoreContextProvider getStoreContextProvider() {
    return Commerce.getCurrentConnection().getStoreContextProvider();
  }

  public CommerceBeanFactory getCommerceBeanFactory() {
    return Commerce.getCurrentConnection().getCommerceBeanFactory();
  }
}
