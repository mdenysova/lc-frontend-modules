package com.coremedia.livecontext.handler.preview.product;

import com.coremedia.blueprint.base.livecontext.ecommerce.common.BaseCommerceIdProvider;
import com.coremedia.blueprint.base.livecontext.ecommerce.common.Commerce;
import com.coremedia.cap.multisite.Site;
import com.coremedia.cap.multisite.SitesService;
import com.coremedia.livecontext.context.ProductInSite;
import com.coremedia.livecontext.ecommerce.catalog.Product;
import com.coremedia.livecontext.ecommerce.common.CommerceBeanFactory;
import com.coremedia.livecontext.ecommerce.common.CommerceConnection;
import com.coremedia.livecontext.ecommerce.common.StoreContext;
import com.coremedia.livecontext.ecommerce.common.StoreContextProvider;
import com.coremedia.livecontext.navigation.LiveContextNavigationFactory;
import com.coremedia.livecontext.product.ProductPageHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.util.UriComponents;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ProductPreviewHandlerTest {
  private final String productReferenceId = "ibm:///catalog/product/123";

  @InjectMocks
  private ProductPreviewHandler productPreviewHandler;

  @Mock
  private SitesService sitesService;

  @Mock
  private LiveContextNavigationFactory liveContextNavigationFactory;

  @Mock
  private StoreContextProvider storeContextProvider;

  @Mock
  private CommerceBeanFactory commerceBeanFactory;

  @Mock
  private ProductPageHandler productPageHandler;

  @Mock
  private Site site;

  @Mock
  private StoreContext storeContext;

  @Mock
  private HttpServletRequest httpServletRequest;

  @Mock
  private HttpServletResponse httpServletResponse;

  @Mock
  private Product product;

  @Mock
  private ProductInSite productInSite;

  @Mock
  private UriComponents uriComponents;

  @Mock
  private CommerceConnection commerceConnection;

  @Test
  public void handleProductPreview() throws IOException {

    Commerce.setCurrentConnection(commerceConnection);
    when(commerceConnection.getStoreContextProvider()).thenReturn(storeContextProvider);
    when(commerceConnection.getCommerceBeanFactory()).thenReturn(commerceBeanFactory);
    when(commerceConnection.getIdProvider()).thenReturn(new BaseCommerceIdProvider("vendor"));
    when(commerceConnection.getStoreContext()).thenReturn(storeContext);
    when(storeContextProvider.getCurrentContext()).thenReturn(storeContext);

    when(sitesService.getSite(anyString())).thenReturn(site);
    when(storeContextProvider.createContext(site)).thenReturn(storeContext);
    doNothing().when(storeContextProvider).setCurrentContext(storeContext);
    when(commerceBeanFactory.createBeanFor(productReferenceId, storeContext)).thenReturn(product);
    when(liveContextNavigationFactory.createProductInSite(product, site)).thenReturn(productInSite);
    when(productPageHandler.buildLinkFor(any(ProductInSite.class), anyString(), anyMap(), any(HttpServletRequest.class))).thenReturn(uriComponents);
    when(uriComponents.toString()).thenReturn("");
    doNothing().when(httpServletResponse).sendRedirect("");

    productPreviewHandler.handleId(productReferenceId, "", httpServletRequest);
  }
}

