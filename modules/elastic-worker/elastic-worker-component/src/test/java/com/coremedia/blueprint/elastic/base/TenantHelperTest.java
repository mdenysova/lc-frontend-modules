package com.coremedia.blueprint.elastic.base;

import com.coremedia.cae.testing.TestInfrastructureBuilder;
import com.coremedia.cap.multisite.SitesService;
import com.coremedia.elastic.core.test.Injection;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class TenantHelperTest {

  private TestInfrastructureBuilder.Infrastructure infrastructure;

  @Before
  public void setUp() throws Exception {
    infrastructure = TestInfrastructureBuilder.create()
            .withContentRepository("classpath:/com/coremedia/testing/contenttest.xml")
            .withBeans("classpath:/com/coremedia/blueprint/base/multisite/bpbase-multisite-services.xml")
            .build();
  }

  @Test
  public void testReadTenantsFromContent() throws Exception {
    final TenantHelper tenantHelper = new TenantHelper();
    Injection.inject(tenantHelper, infrastructure.getContentRepository());
    Injection.inject(tenantHelper, infrastructure.getBean("sitesService", SitesService.class));
    tenantHelper.initialize();
    final Collection<String> strings = tenantHelper.readTenantsFromContent();
    assertEquals(2, strings.size());
    assertThat(strings, CoreMatchers.hasItems("tenant", "testTenant"));
  }
}
