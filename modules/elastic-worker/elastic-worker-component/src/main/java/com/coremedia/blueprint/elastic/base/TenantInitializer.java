package com.coremedia.blueprint.elastic.base;

import com.coremedia.elastic.core.api.tenant.TenantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.SmartLifecycle;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Predicates.in;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Collections2.filter;

/**
 * This bean registers the tenants configured in content repository (via root channel settings).
 */
@SuppressWarnings("UnusedDeclaration") // used by Spring - it's a managed bean
@Named
public class TenantInitializer implements SmartLifecycle {

  private static final Logger LOG = LoggerFactory.getLogger(TenantInitializer.class);

  private ScheduledExecutorService executorService;

  @Inject
  private TenantService tenantService;

  @Inject
  private TenantHelper tenantHelper;

  @Value("${tenant.recomputation.start.phase:0}")
  private int phase;

  @Value("${tenant.recomputation.interval:60}")
  private long delay;

  private void registerTenantsFromContent() {
    final Collection<String> configuredTenants = tenantHelper.readTenantsFromContent();
    final Collection<String> registeredTenants = tenantService.getRegistered();
    deregisterTenants(configuredTenants, registeredTenants);
    registerTenants(configuredTenants, registeredTenants);
  }

  private void registerTenants(Collection<String> configuredTenants, Collection<String> registeredTenants) {
    final Collection<String> tenantsToAdd = filter(configuredTenants, not(in(registeredTenants)));
    if(!tenantsToAdd.isEmpty()) {
      LOG.debug("registering tenants {}", tenantsToAdd);
      tenantService.registerAll(tenantsToAdd);
    }
  }

  private void deregisterTenants(Collection<String> configuredTenants, Collection<String> registeredTenants) {
    final Collection<String> tenantsToRemove = filter(registeredTenants, not(in(configuredTenants)));
    if(!tenantsToRemove.isEmpty()) {
      LOG.debug("deregistering tenants {}", tenantsToRemove);
      tenantService.deregisterAll(tenantsToRemove);
    }
  }

  @Override
  public void start() {
    if(executorService == null) {
      executorService = Executors.newSingleThreadScheduledExecutor(new CustomizableThreadFactory(TenantInitializer.class.getSimpleName() + "-scheduler-"));

      LOG.debug("scheduling tenant initializer every {} seconds", delay);
      executorService.scheduleWithFixedDelay(new TenantInitializerRunner(), 0L, delay, TimeUnit.SECONDS);
    }
  }

  @Override
  public void stop() {
    if(executorService != null) {
      executorService.shutdown();
      executorService = null;
    }
  }

  @Override
  public boolean isRunning() {
    return !(executorService == null || executorService.isShutdown());
  }

  @Override
  public boolean isAutoStartup() {
    return true;
  }

  @Override
  public void stop(Runnable callback) {
    try {
      stop();
    } finally {
      callback.run();
    }
  }

  @Override
  public int getPhase() {
    return phase;
  }

  private class TenantInitializerRunner implements Runnable {
    @Override
    public void run() {
      try {
        registerTenantsFromContent();
      } catch (Exception e) {
        LOG.info("caught unexpected exception while computing tenants", e);
      }
    }
  }
}