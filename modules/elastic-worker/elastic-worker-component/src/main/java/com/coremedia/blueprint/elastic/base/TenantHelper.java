package com.coremedia.blueprint.elastic.base;

import com.coremedia.cap.content.Content;
import com.coremedia.cap.content.ContentRepository;
import com.coremedia.cap.multisite.Site;
import com.coremedia.cap.multisite.SitesService;
import com.coremedia.cap.struct.Struct;
import com.coremedia.cap.struct.StructBuilder;
import com.coremedia.cap.struct.StructBuilderMode;
import com.coremedia.cap.struct.StructService;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.join;

/**
 * Helper bean to compute the configured tenants (from root navigation settings)
 */
@Named
public class TenantHelper {

  @Inject
  private ContentRepository contentRepository;

  @Inject
  private SitesService sitesService;

  private StructService structService;

  @PostConstruct
  public void initialize() {
    structService = contentRepository.getConnection().getStructService();
  }

  public Collection<String> readTenantsFromContent() {
    final Collection<String> tenants = new HashSet<>();
    for (Site site : sitesService.getSites()) {
      final Map<String, Object> stringObjectMap = getSettingsAsMap(site.getSiteRootDocument());
      final Object elasticSocialSettings = stringObjectMap.get("elasticSocial");
      if(elasticSocialSettings instanceof Map) {
        Map map = (Map) elasticSocialSettings;
        final String tenant = join(map.get("tenant"));
        if(!StringUtils.isEmpty(tenant)) {
          tenants.add(tenant);
        }
      }
    }
    return Collections.unmodifiableCollection(tenants);
  }

  private Map<String, Object> getSettingsAsMap(Content rootNavigation) {
    final Struct struct = getLocalSettings(rootNavigation);
    //tell structbuilder to allow merging of structs
    final StructBuilder structBuilder = struct.builder().mode(StructBuilderMode.LOOSE);
    addLinkedSettings(rootNavigation, structBuilder);
    return structBuilder.build().toNestedMaps();
  }

  private void addLinkedSettings(Content rootNavigation, StructBuilder structBuilder) {
    final List<Content> linkedSettings = rootNavigation.getLinks("linkedSettings");
    for (Content settingsContent : linkedSettings) {
      final Object settings = settingsContent.get("settings");
      if(settings instanceof Struct) {
        Struct settingsStruct = (Struct) settings;
        structBuilder.defaultTo(settingsStruct);
      }
    }
  }

  private Struct getLocalSettings(final Content rootNavigation) {
    final Struct struct = rootNavigation.getStruct("localSettings");
    if (struct == null) {
      return structService.emptyStruct();
    }
    return struct;
  }

}
