package com.coremedia.blueprint.studio.components {

import com.coremedia.blueprint.studio.config.components.multiListSelector;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.ValueExpressionFactory;
import com.coremedia.ui.store.BeanRecord;

import ext.Container;
import ext.DataView;
import ext.Ext;
import ext.list.ListView;

public class MultiListSelectorBase extends Container {
  public static const ALL:String = "allBeansList";
  public static const SELECT:String = "selectedBeansList";

  private var selectedBeansExp:ValueExpression;
  private var allBeansExp:ValueExpression;
  private var availableBeansExp:ValueExpression;

  private var left:ListView;
  private var right:ListView;

  public function MultiListSelectorBase(config:multiListSelector) {
    selectedBeansExp = config.selectedBeansExpression;
    allBeansExp = config.allBeansExpression;
    super(config);
  }

  override protected function afterRender():void {
    super.afterRender();
    calculateAmount().addChangeListener(calculateHeight);
  }

  protected function calculateHeight(amountOfElementsVE:ValueExpression):void {
    var innerContainer:Container = find("itemId", "inner-container")[0];
    innerContainer.setHeight(amountOfElementsVE.getValue() * 23);
  }

  protected function calculateAmount():ValueExpression {
    return ValueExpressionFactory.createFromFunction(function ():int {
      var fve:Array = Array(getAvailableBeansExpression().getValue());
      var vfe2:Array = Array(selectedBeansExp.getValue());
      if (Ext.isArray(fve) && Ext.isArray(vfe2)) {
        var itemsOnLeftHandSide:int = fve.length;
        var itemsOnRightHandSide:int = vfe2.length;
        return (itemsOnLeftHandSide > itemsOnRightHandSide ? itemsOnLeftHandSide : itemsOnRightHandSide )
      }
    });
  }

  public function getAvailableBeansExpression():ValueExpression {
    if (!availableBeansExp) {
      availableBeansExp = ValueExpressionFactory.createFromFunction(function ():Array {
        var allBeans:Array = Array(allBeansExp.getValue());
        var selectedBeans:Array = Array(selectedBeansExp.getValue());
        return filter(allBeans, selectedBeans);
      });
    }
    return availableBeansExp;
  }

  override protected function initComponent():void {
    super.initComponent();
    left = find("itemId", ALL)[0] as ListView;
    right = find("itemId", SELECT)[0] as ListView;

    left.on("dblclick", handleMoveToRight);
    right.on("dblclick", handleMoveToLeft);
  }

  private static function getSelectedBeans(dv:DataView):Array {
    var result:Array = [];
    Ext.each(dv.getSelectedRecords(), function (br:BeanRecord):void {
      result.push(br.getBean());
    });
    return result;
  }

  protected function handleMoveToRight():void {
    var oldValue:Array = selectedBeansExp.getValue();
    var selectedBeans:Array = getSelectedBeans(left);
    selectedBeansExp.setValue(oldValue ? oldValue.concat(selectedBeans) : selectedBeans);
  }

  protected function handleMoveToLeft():void {
    var oldValue:Array = selectedBeansExp.getValue();
    selectedBeansExp.setValue(filter(oldValue, getSelectedBeans(right)));
  }

  protected static function filter(items:Array, toBeRemoved:Array):Array {
    if (Ext.isArray(items) && Ext.isArray(toBeRemoved)) {
      return items.filter(function (item:*):Boolean {
        return items.indexOf(item) < 0;
      });
    }
    // default return value is undefined
  }

  protected static function getEmptyText(text:String):String {
    return '<div class="default-text"><label style="vertical-align: middle;cursor: pointer;">' + text + '</label></div>';
  }

  protected static function isButtonDisabled(items:Array):Boolean {
    return Ext.isEmpty(items);
  }
}
}
