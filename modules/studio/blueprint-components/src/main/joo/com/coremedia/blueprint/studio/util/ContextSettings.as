package com.coremedia.blueprint.studio.util {
import com.coremedia.cap.content.Content;

/**
 * Model for the context settings.
 */
public class ContextSettings {
  private var content:Content;
  private var contexts:Array = [];

  public function ContextSettings(content:Content) {
    this.content = content;
  }

  public function addContext(context:Content):void {
    this.contexts.push(context);
  }

  public function getContexts():Array {
    return contexts;
  }

  public function toString():String {
    return 'Context Settings for "' + content.getName() + '", contexts: ' + contexts.length;
  }
}
}