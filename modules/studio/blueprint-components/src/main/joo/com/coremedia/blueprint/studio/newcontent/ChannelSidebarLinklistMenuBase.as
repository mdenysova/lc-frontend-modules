package com.coremedia.blueprint.studio.newcontent {

import com.coremedia.blueprint.studio.util.ContentCreationUtil;
import com.coremedia.ui.components.IconButton;
import com.coremedia.ui.config.iconButton;
import com.coremedia.ui.data.ValueExpression;

public class ChannelSidebarLinklistMenuBase extends IconButton {
  
  public function ChannelSidebarLinklistMenuBase(config:iconButton) {
    super(config);
  }

  protected function getSuccessHandler(callback:Function, bindTo:ValueExpression, propertyName:String, openInTab:Boolean = true) {
    if(callback) {
      return callback
    }
    return ContentCreationUtil.linkInSourceLinklistHandler(bindTo.extendBy('properties', propertyName), openInTab);
  }
}
}
