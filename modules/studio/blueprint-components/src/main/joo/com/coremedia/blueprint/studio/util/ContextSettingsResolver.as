package com.coremedia.blueprint.studio.util {
import com.coremedia.cap.common.session;
import com.coremedia.cap.content.Content;
import com.coremedia.cap.content.ContentPropertyNames;
import com.coremedia.ui.data.RemoteBean;
import com.coremedia.ui.data.ValueExpressionFactory;
import com.coremedia.ui.data.beanFactory;

/**
 * Searches for _folderProperties and recursively collects all settings
 * found in the linked context.
 */
public class ContextSettingsResolver {

  private var context:Content;

  public function ContextSettingsResolver(context:Content) {
    this.context = context;
  }

  /**
   * Searches the _folderProperties for the corresponding context and
   * loads the settings for the context recursively if not applied to
   * the collected settings yet.
   * @param callback The callback function passing a ContextSettings instance.
   */
  public function resolveSettings(callback:Function):void {
    var settings:ContextSettings = new ContextSettings(context);

    context.invalidate(function ():void {
      var type:String = context.getType().getName();
      if (type === 'CMChannel' || type === 'CMSite') {
        loadSettingsFromChannel(context, settings, function ():void {
          callback.call(null, settings);
        });
      }
      else {
        loadSettingsViaFolderProperties(settings, function ():void {
          callback.call(null, settings);
        });
      }
    });
  }

  /**
   * First looks up the folder properties of the given content and then searches
   * the linked context, that is a channel that contains the settings.
   * @param settings The settings model bean.
   * @param callback The callback to call when the finished.
   */
  private function loadSettingsViaFolderProperties(settings:ContextSettings, callback:Function):void {
    var folder:Content = context.getParent();
    loadFolderProperties(folder, function (folderProperties:Content):void {
      if (folderProperties) {
        folderProperties.invalidate(function ():void {
          var contexts:Array = folderProperties.getProperties().get('contexts');
          if (contexts && contexts.length > 0) {
            var count:Number = contexts.length;
            contexts.forEach(function(context:Content):void {
              ValueExpressionFactory.create(ContentPropertyNames.PATH, context).loadValue(function ():void {
                settings.addContext(context);
                count--;
                if(count === 0) {
                  callback.call(null);
                }
              });
            });
          }
          else {
            callback.call(null);
          }
        });
      }
      else {
        callback.call(null);
      }
    });
  }

  /**
   * Loads the settings from the given channel document.
   * @param context The channel document
   * @param settings The settings model bean.
   * @param callback
   */
  private function loadSettingsFromChannel(context:Content, settings:ContextSettings, callback:Function):void {
    collectSettings(settings, context, function ():void {
      callback.call(null, settings);
    });
  }

  /**
   * Recursive search for all contexts settings.
   * @param settings The settings model to fill.
   * @param context The current context, which is a CMChannel.
   * @param callback The callback to call when search is finished.
   */
  private function collectSettings(settings:ContextSettings, context:Content, callback:Function):void {
    context.load(function ():void {
      var refBean:RemoteBean = beanFactory.getRemoteBean('blueprint/referrers/' + ContentUtil.getNumericId(context) + '/CMChannel');
      refBean.load(function ():void {
        var parentChannel:Content = refBean.get('id');
        if (parentChannel) {
          ValueExpressionFactory.create(ContentPropertyNames.PATH, parentChannel).loadValue(function ():void {
            settings.addContext(context);
            callback.call(null);
          });
        }
        else {
          callback.call(null);
        }
      });
    });
  }

  /**
   * Recursive search for the _folderProperties document.
   * @param folder The current folder instance or null.
   * @param callback The callback the first folder properties found are passed to.
   */
  private function loadFolderProperties(folder:Content, callback:Function):void {
    if (!folder) {
      callback.call(null, null);
    }
    ValueExpressionFactory.create('path', folder).loadValue(function ():void {
      var path:String = folder.getPath() + '/_folderProperties';
      session.getConnection().getContentRepository().getChild(path, function (folderProperties:Content):void {
        if (folderProperties) {
          callback.call(null, folderProperties);
        }
        else {
          loadFolderProperties(folder.getParent(), callback);
        }
      });
    });
  }
}
}