package com.coremedia.blueprint.studio.plugins {
import ext.Component;
import ext.Container;
import ext.Plugin;
import ext.form.Field;
import ext.form.TextField;

import js.HTMLElement;

public class PreserveCursorPlugin implements Plugin {
  private var originalSetValue:Function;

  public function PreserveCursorPlugin(config:Object) {}

  public function init(component:Component):void {
    if (component is Container) {
      Container(component).findByType("textfield").forEach(overwriteSetValue);
    }
    if (component is TextField) {
      overwriteSetValue(TextField(component));
    }
  }

  private function overwriteSetValue(field:TextField):void {
    originalSetValue = field['setValue'];
    field.setValue = function(newValue:String):Field {
      if (field.rendered) {
        var oldValue:String = field.getValue();
        var element:HTMLElement = field.el.dom;
        var oldCursorPos:Number = element.selectionEnd;

        originalSetValue.apply(field, [newValue]);

        var commonSuffixLength:int = 0;
        while (commonSuffixLength < newValue.length - oldCursorPos &&
          commonSuffixLength < oldValue.length &&
          newValue.charAt(newValue.length - 1 - commonSuffixLength) === oldValue.charAt(oldValue.length - 1 - commonSuffixLength)) {
          commonSuffixLength++;
        }

        var newCursorPos:int = newValue.length - commonSuffixLength;
        element['setSelectionRange'](newCursorPos, newCursorPos);

        return field;
      } else {
        return originalSetValue.apply(field, [newValue]);
      }
    }
  }
}
}
