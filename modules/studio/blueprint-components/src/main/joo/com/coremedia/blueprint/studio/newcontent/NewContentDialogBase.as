package com.coremedia.blueprint.studio.newcontent {

import com.coremedia.blueprint.studio.config.components.newContentDialog;
import com.coremedia.blueprint.studio.util.ContentCreationUtil;
import com.coremedia.blueprint.studio.util.contentcreation.FolderCreationResult;
import com.coremedia.cms.editor.sdk.util.PathFormatter;
import com.coremedia.cap.common.session;
import com.coremedia.cap.content.Content;
import com.coremedia.cap.content.ContentType;
import com.coremedia.cms.editor.EditorErrors_properties;
import com.coremedia.cms.editor.sdk.util.ContentLocalizationUtil;
import com.coremedia.cms.editor.sdk.util.MessageBoxUtil;
import com.coremedia.ui.data.Bean;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.beanFactory;

import ext.Window;
import ext.form.TextField;
import ext.util.StringUtil;

public class NewContentDialogBase extends Window {
  internal static const NAME:String = 'createContentNameField';

  private var model:Bean;
  private var ct:ContentType;
  private var promptOnCreate:Boolean;
  private var folders:Array;
  private var onSuccess:Function;
  private var defaultName:ValueExpression;
  private var contentTypeExpression:ValueExpression;
  private var openInTab:Boolean;
  private var skipInitializers:Boolean;

  public function NewContentDialogBase(config:newContentDialog = null) {
    promptOnCreate = config.promptOnFolderCreate;
    skipInitializers = config.skipInitializers;
    contentTypeExpression = config.contentTypeExpression;
    defaultName = config.defaultName;
    var contentType:String = config.contentType;
    if(!contentType) {
      contentType = contentTypeExpression.getValue();
    }
    if(!contentType) {
      throw new Error('No content type defined for the new content dialog.');
    }
    ct = session.getConnection().getContentRepository().getContentType(contentType);

    if (config.openInTab == undefined) {
        config.openInTab = true;
    }
    openInTab = config.openInTab;

    folders = config.folders;
    onSuccess = config.onSuccess;
    super(config);
    on('afterrender', initializeDialog);
  }

  public function initializeDialog():void {
    getModel().set('path', PathFormatter.formatSitePath(folders[0]));
    var nameField:TextField = find('name', NAME)[0];
    var initialName:String = NewContentStudioPlugin_properties.INSTANCE.CreateContent_name_initialName;
    if(defaultName && defaultName.getValue()) {
      initialName = defaultName.getValue();
    }
    nameField.setValue(initialName);
    getModel().set('name', initialName);
    nameField.focus(true, 500);
  }

  public function createContent():void {
    var newName:String = model.get('name');
    if (newName.length == 0 || newName == '..' || newName == '.' || newName.indexOf('/') != -1) {
      MessageBoxUtil.showError(EditorErrors_properties.INSTANCE.invalidName_title,
              StringUtil.format(EditorErrors_properties.INSTANCE.invalidName_generic_message, newName));
      return;
    }
    setVisible(false);
    ContentCreationUtil.createRequiredSubfolders(model.get('path'), function(result:FolderCreationResult):void {
      if (result.success) {
        getModel().set('created', result.createdFolders);
        ContentCreationUtil.createContent(result.baseFolder, openInTab, skipInitializers, model.get('name'), ct, function(content:Content):void {
          close();
          if (onSuccess) {
            onSuccess(content);
          }
        }, function():void { /*Callback-Function fail*/
          setVisible(true);
        });
      }
      else {
        setVisible(true);
        if (result.remoteError) {
          MessageBoxUtil.showError("Error", "Didn't work: " + result.remoteError.errorName);
        }
      }
    }, promptOnCreate);
  }

  public function getModel():Bean {
    if (!model) {
      model = beanFactory.createLocalBean();
    }
    return model;
  }

  /**
   * Returns the name of the content type that should be created, used
   * for the dialog title.
   * @param ct The content type as string if set.
   * @param ctVe The content type value expression, used if the content type is not defined.
   * @return The localized content type string.
   */
  protected function getLocalizedContentType(ct:String, ctVe:ValueExpression):String {
    if(ct) {
      return ContentLocalizationUtil.localizeDocumentTypeName(ct);
    }
    return ContentLocalizationUtil.localizeDocumentTypeName(ctVe.getValue());
  }
}
}
