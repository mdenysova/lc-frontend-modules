package com.coremedia.blueprint.studio.renderer {

import com.coremedia.blueprint.base.components.viewtypes.ViewtypeLocalizationUtil;
import com.coremedia.blueprint.studio.util.ContentUtil;
import com.coremedia.cap.content.Content;
import com.coremedia.ui.store.BeanRecord;
import com.coremedia.ui.util.ObjectUtils;
import com.coremedia.ui.util.QtipUtil;


public class ViewtypeRenderer {

  public static function convert(value:String, content:Content):Object {
    if (content) {
      var hasProperty:Boolean = ContentUtil.hasProperty(content, 'viewtype');
      if (hasProperty === undefined) {
        return undefined;
      }
      if (hasProperty) {
        var viewtypes:Array = ObjectUtils.getPropertyAt(content, 'properties.viewtype', null);
        if (viewtypes === undefined) {
          return undefined;
        }
        if (viewtypes && viewtypes.length > 0) {
          var viewType:Content = viewtypes[0];
          var thumbUri:String = ObjectUtils.getPropertyAt(viewType, 'properties.icon.uri', null);
          var viewTypeName:String = ViewtypeLocalizationUtil.localizeText(viewType);
          if (thumbUri === undefined) {
            return undefined;
          }
          if (viewTypeName === undefined) {
            return undefined;
          }
          return {url: thumbUri, viewTypeName: viewTypeName};
        }
      }
    }
    return {url: "", viewTypeName: ""};
  }

  public static function renderer(value:Object, metaData:*, record:BeanRecord):String {
    if (value) {
      var viewTypeName:String = value.viewTypeName;
      var thumbUri:String = value.url;
      if (viewTypeName && thumbUri) {
        return '<img '+QtipUtil.formatQtip(viewTypeName)+' src="' + thumbUri + '/s;w=32;h=22" width="32" height="22" class="viewtype-thumb"/>'
      }
    }
    return '<div style="width:32px;height:22px"></div>';  // spacer
  }
}
}
