package com.coremedia.blueprint.studio.property {
import com.coremedia.blueprint.studio.config.components.contextListPanel;
import com.coremedia.blueprint.studio.util.ContextSettings;
import com.coremedia.blueprint.studio.util.ContextSettingsResolver;
import com.coremedia.blueprint.studio.util.StudioUtil;
import com.coremedia.ui.data.Bean;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.ValueExpressionFactory;
import com.coremedia.ui.data.beanFactory;

import ext.Container;

/**
 * @private
 *
 * The application logic for a property field editor that edits
 * link lists. Links can be limited to documents of a given type.
 *
 * @see com.coremedia.cms.editor.sdk.config.linkListPropertyFieldGridPanelBase
 * @see com.coremedia.cms.editor.sdk.premular.fields.LinkListPropertyField
 */
public class ContextListPanelBase extends Container {
  private var selectedPositionsExpression:ValueExpression;
  private var selectedValuesExpression:ValueExpression;
  private var contextListValueExpression:ValueExpression;
  private var bindTo:ValueExpression;
  private var resolver:ContextSettingsResolver;

  /**
   * @param config The configuration options. See the config class for details.
   *
   * @see com.coremedia.cms.editor.sdk.config.linkListPropertyFieldGridPanelBase
   */
  public function ContextListPanelBase(config:contextListPanel) {
    super(config);
    this.bindTo = config.bindTo;
    config.bindTo.addChangeListener(reload);
  }

  /**
   * Updates the context list if the content has been changed.
   */
  protected function reload():void {
    resolver.resolveSettings(function(settings:ContextSettings):void {
      contextListValueExpression.setValue(settings.getContexts());
    });
  }


  override protected function afterRender():void {
    super.afterRender();
    find('itemId', 'contextList')[0].addListener('dblclick', onDblClick);
  }

  /**
   * Fired when the user double clicks a row.
   * The next taxonomy child level of the selected node is entered then.
   */
  private function onDblClick():void {
    var selection:Array = getSelectedValuesExpression().getValue();
    if (selection && selection.length > 0) {
      StudioUtil.openInTab(selection[0]);
    }
  }

  protected function getContextListValueExpression(bindTo:ValueExpression):ValueExpression {
    if (!contextListValueExpression) {
      contextListValueExpression = ValueExpressionFactory.create('values', beanFactory.createLocalBean());
      resolver = new ContextSettingsResolver(bindTo.getValue());
      reload();
    }
    return contextListValueExpression;
  }


  public function getSelectedPositionsExpression():ValueExpression {
    if (!selectedPositionsExpression) {
      var selectedPositionsBean:Bean = beanFactory.createLocalBean({ positions:[] });
      selectedPositionsExpression = ValueExpressionFactory.create("positions", selectedPositionsBean);
    }
    return selectedPositionsExpression;
  }

  public function getSelectedValuesExpression():ValueExpression {
    if (!selectedValuesExpression) {
      var selectedValuesBean:Bean = beanFactory.createLocalBean({ values:[] });
      selectedValuesExpression = ValueExpressionFactory.create("values", selectedValuesBean);
    }
    return selectedValuesExpression;
  }
}
}
