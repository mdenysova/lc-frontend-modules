package com.coremedia.blueprint.studio.rest;

import com.coremedia.cap.common.CapConnection;
import com.coremedia.cap.content.Content;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Required;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * A simple REST service for resolving referrers of content.
 */
@Produces(MediaType.APPLICATION_JSON)
@Path("blueprint/referrers")
public class ReferrersResource {
  private static final String PARAM_ID = "id";
  private static final String PARAM_TYPE = "type";
  private static final String PARAM_DESCRIPTOR = "descriptor";

  private CapConnection capConnection;

  @Required
  public void setCapConnection(CapConnection capConnection) {
    this.capConnection = capConnection;
  }

  @GET
  @Path("/{id}/all")
  public List<Content> getReferrers(@PathParam(PARAM_ID) String id) {
    List<Content> referrers = new ArrayList<>();
    if (isNotBlank(id)) {
      Content content = capConnection.getContentRepository().getContent(id);
      referrers.addAll(content.getReferrers());
    }
    return referrers;
  }

  @GET
  @Path("/{id}/{type}")
  public Map<String, Content> getReferrer(@PathParam(PARAM_ID) String id, @PathParam(PARAM_TYPE) String type) {
    Map<String, Content> result = Maps.newHashMap();
    Content referrer = null;
    if (isNotBlank(id)) {
      Content content = capConnection.getContentRepository().getContent(id);
      if (content != null && content.getRepository().getContentTypesByName().containsKey(type)) {
        referrer = content.getReferrerWithType(type);
        result.put(PARAM_ID, referrer);
      }
    }
    return result;
  }

  @GET
  @Path("/{id}/{type}/{descriptor}")
  public Map<String, Content> getReferrerWithDescriptor(@PathParam(PARAM_ID) String id,
                                                        @PathParam(PARAM_TYPE) String type,
                                                        @PathParam(PARAM_DESCRIPTOR) String descriptor) {
    Map<String, Content> result = Maps.newHashMap();
    Content referrer = null;
    if (isNotBlank(id)) {
      Content content = capConnection.getContentRepository().getContent(id);
      if (content != null && content.getRepository().getContentTypesByName().containsKey(type)) {
        referrer = content.getReferrerWithDescriptor(type, descriptor);
        result.put(PARAM_ID, referrer);
      }
    }
    return result;
  }

  @GET
  @Path("/navigation/{id}")
  public List<Content> getReferrerWithDescriptor(@PathParam(PARAM_ID) String id) {
    Content content = capConnection.getContentRepository().getContent(id);
    List<Content> navigationItems = new ArrayList<>();
    resolveNavigation(content, navigationItems);
    Collections.reverse(navigationItems);
    return navigationItems;
  }

  /**
   * Recursive navigation tree build.
   * @param child
   * @param navigationItems
   */
  private void resolveNavigation(Content child, List<Content> navigationItems) {
    if(!navigationItems.contains(child)) {
      navigationItems.add(child);
      Content referrer = child.getReferrerWithDescriptor("CMChannel", "children");
      if(referrer != null) {
        resolveNavigation(referrer, navigationItems);
      }
    }

  }
}
