package com.coremedia.blueprint.studio.rest;

import com.coremedia.blueprint.testing.ContentTestCaseHelper;
import com.coremedia.cap.common.IdHelper;
import com.coremedia.cap.content.Content;
import com.coremedia.cae.testing.TestInfrastructureBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ReferrersResourceTest {
  private ReferrersResource resource;
  private Content mediaChannel;
  private static TestInfrastructureBuilder.Infrastructure infrastructure = TestInfrastructureBuilder
          .create()
          .withContentRepository("classpath:/com/coremedia/testing/contenttest.xml")
          .build();

  @Before
  public void setUp() throws Exception {

    resource = new ReferrersResource();
    resource.setCapConnection(infrastructure.getContentRepository().getConnection());

    mediaChannel = ContentTestCaseHelper.getContent(infrastructure, 12);
  }

  @Test
  public void testReferrers() throws Exception {
    Assert.assertNotNull(mediaChannel);
    Assert.assertFalse(mediaChannel.getList("children").isEmpty());

    String id = IdHelper.formatContentId(14);
    Assert.assertNotNull(resource.getReferrers(id));
  }
}
