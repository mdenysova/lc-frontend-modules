package com.coremedia.blueprint.studio.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Required;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.nio.charset.Charset;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.KeySpec;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.coremedia.io.HexEncoder.encodeBytes;
import static com.coremedia.util.HexDecoder.decodeBytes;
import static javax.crypto.Cipher.DECRYPT_MODE;
import static javax.crypto.Cipher.ENCRYPT_MODE;

@Produces(MediaType.APPLICATION_JSON)
@Path("structeditor")
public class StructEditorService implements InitializingBean {
  private static final Logger LOG = LoggerFactory.getLogger(StructEditorService.class);
  private static final Random RANDOM = new Random();
  private static final Charset UTF8_CHARSET = Charset.forName("UTF-8");
  private static final String SEPARATOR = ",";
  private static final int INDEX_CIPHER_TEXT = 0;
  private static final int INDEX_SALT = 1;
  private static final int INDEX_INITIALIZATION_VECTOR = 2;
  private static final int PBE_KEY_SIZE = 65536;

  private SecretKeyFactory secretKeyFactory;
  private String masterPassword;
  private String cypherAlgorithm;
  private String keyFactoryAlgorithm;
  private String keySpecAlgorithm;
  private int keyLength;

  @Override
  public void afterPropertiesSet() throws Exception {
    secretKeyFactory = SecretKeyFactory.getInstance(keyFactoryAlgorithm);
  }

  @POST
  @Path("encryptPassword")
  @Consumes("application/x-www-form-urlencoded")
  public Map<String, String> encryptPassword(@FormParam("newPassword") String newPassword,
                                             @FormParam("repeatNewPassword") String repeatNewPassword) {
    try {
      if (!isEqual(newPassword, repeatNewPassword)) {
        LOG.warn("The new password was not repeated correctly.");
        throw new WebApplicationException(
                new IllegalArgumentException("The new password was not repeated correctly."),
                Response.Status.PRECONDITION_FAILED);
      }

      Map<String, String> json = new HashMap<>(1);
      json.put("password", encrypt(newPassword));
      return json;
// Sonar says "Avoid Rethrowing Exception", but there was an other problem
// w/o this catch clause.  See results tomorrow...
//    } catch (WebApplicationException e) {
//      throw e;
    } catch (RuntimeException e) {
      LOG.error("Unknown error while decrypting password.", e);
      throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
    } catch (GeneralSecurityException e) {
      LOG.error("security exception while decrypting password.", e);
      throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
    }
  }

  public String decryptPassword(String encrypted) throws IllegalBlockSizeException, InvalidKeyException,
          NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException,
          BadPaddingException, InvalidAlgorithmParameterException {
    return new String(decrypt(encrypted), UTF8_CHARSET);
  }

  private byte[] encryptPassword(String password, Cipher cipher) throws IllegalBlockSizeException, BadPaddingException {
    return cipher.doFinal(password.getBytes(UTF8_CHARSET));
  }

  protected String encrypt(String password) throws InvalidKeySpecException, InvalidKeyException,
          NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
          BadPaddingException, InvalidParameterSpecException, InvalidAlgorithmParameterException {
    byte[] salt = createSalt();
    SecretKey secretKey = createSecretKey(masterPassword, salt, keyLength);
    Cipher cipher = createCipher(secretKey, ENCRYPT_MODE, cypherAlgorithm);
    byte[] encryptedPassword = encryptPassword(password, cipher);
    byte[] initializationVector = readInitializationVector(cipher);

    return createJsonPassword(encryptedPassword, salt, initializationVector);
  }

  private byte[] decrypt(String encrypted) throws InvalidKeySpecException, InvalidKeyException, NoSuchAlgorithmException,
          NoSuchPaddingException, InvalidAlgorithmParameterException,
          IllegalBlockSizeException, BadPaddingException {
    byte[] cipherText = readCipherText(encrypted);
    byte[] initializingVector = readInitializationVector(encrypted);
    byte[] salt = readSalt(encrypted);
    SecretKey secretKey = createSecretKey(masterPassword, salt, keyLength);
    Cipher cipher = createCipher(secretKey, DECRYPT_MODE, initializingVector, cypherAlgorithm);
    cipher.init(DECRYPT_MODE, secretKey, new IvParameterSpec(initializingVector));

    return cipher.doFinal(cipherText);
  }

  private String createJsonPassword(byte[] encryptedPassword, byte[] salt, byte[] initializingVector) {
    String passwordHex = encodeBytes(encryptedPassword);
    String saltHex = encodeBytes(salt);
    String initializingVectorHex = encodeBytes(initializingVector);

    return passwordHex + SEPARATOR + saltHex + SEPARATOR + initializingVectorHex;
  }

  private byte[] readInitializationVector(Cipher cipher) throws InvalidParameterSpecException {
    AlgorithmParameters params = cipher.getParameters();
    return params.getParameterSpec(IvParameterSpec.class).getIV();
  }

  private byte[] readInitializationVector(String encrypted) {
    String initializationVectorString = encrypted.split(SEPARATOR)[INDEX_INITIALIZATION_VECTOR];
    return decodeBytes(initializationVectorString.trim());
  }

  private byte[] readCipherText(String encrypted) {
    String cipherTextString = encrypted.split(SEPARATOR)[INDEX_CIPHER_TEXT];
    return decodeBytes(cipherTextString);
  }

  private byte[] readSalt(String encrypted) {
    String saltString = encrypted.split(SEPARATOR)[INDEX_SALT];
    return decodeBytes(saltString);
  }

  private Cipher createCipher(SecretKey secretKey, int cipherMode, String algorithm) throws InvalidKeyException,
          NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {
    return createCipher(secretKey, cipherMode, null, algorithm);
  }

  private Cipher createCipher(SecretKey secretKey, int cipherMode, byte[] initializationVector, String algorithm)
          throws NoSuchAlgorithmException, NoSuchPaddingException,
          InvalidKeyException, InvalidAlgorithmParameterException {
    Cipher cipher = Cipher.getInstance(algorithm);
    if (secretKey != null) {
      if (initializationVector != null) {
        cipher.init(cipherMode, secretKey, new IvParameterSpec(initializationVector));
      } else {
        cipher.init(cipherMode, secretKey);
      }
    }

    return cipher;
  }

  private SecretKey createSecretKey(String password, byte[] salt, int keyLength) throws InvalidKeySpecException {
    KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, PBE_KEY_SIZE, keyLength);
    SecretKey tmp = secretKeyFactory.generateSecret(spec);

    return new SecretKeySpec(tmp.getEncoded(), keySpecAlgorithm);
  }

  private byte[] createSalt() {
    Long saltLong = RANDOM.nextLong();
    String saltString = Long.toHexString(saltLong);
    return saltString.getBytes(UTF8_CHARSET);
  }

  private boolean isEqual(String one, String two) {
    if (one == null) {
      return two == null;
    }

    return one.equals(two);
  }

  @Required
  public void setMasterPassword(String masterPassword) {
    this.masterPassword = masterPassword;
  }

  @Required
  public void setCypherAlgorithm(String cypherAlgorithm) {
    this.cypherAlgorithm = cypherAlgorithm;
  }

  @Required
  public void setKeyFactoryAlgorithm(String keyFactoryAlgorithm) {
    this.keyFactoryAlgorithm = keyFactoryAlgorithm;
  }

  @Required
  public void setKeyLength(int keyLength) {
    this.keyLength = keyLength;
  }

  @Required
  public void setKeySpecAlgorithm(String keySpecAlgorithm) {
    this.keySpecAlgorithm = keySpecAlgorithm;
  }
}