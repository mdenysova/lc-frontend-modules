package com.coremedia.blueprint.studio.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class StructEditorServiceTest {
  private StructEditorService structEditorService;

  @Before
  public void setup() throws Exception {
    structEditorService = new StructEditorService();
    structEditorService.setCypherAlgorithm("DESede/CBC/PKCS5Padding");
    structEditorService.setKeyFactoryAlgorithm("PBEWithMD5AndDES");
    structEditorService.setKeySpecAlgorithm("DESede");
    structEditorService.setKeyLength(192);
    structEditorService.setMasterPassword("Ey2go3I0K4Y04Pz_USKb9gBb");
    structEditorService.afterPropertiesSet();
  }

  @Test()
  public void encryptPasswordNewAndRepeatAreDifferent() {
    try {
      structEditorService.encryptPassword("new", "different");
      assertFalse("We must not reach this point.", true);
    } catch (WebApplicationException e) {
      // Alright!
    }
  }

  @Test()
  public void encryptPassword() throws IllegalBlockSizeException, InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, BadPaddingException, InvalidAlgorithmParameterException {
    Map<String, String> encrypted = structEditorService.encryptPassword("new", "new");
    assertNotNull(encrypted);
    assertEquals(structEditorService.decryptPassword(encrypted.get("password")), "new");
  }

  @Test()
  public void encryptPasswordPlainStructPropertyName() throws IllegalBlockSizeException, InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, BadPaddingException, InvalidAlgorithmParameterException {
    Map<String, String> encrypted = structEditorService.encryptPassword("new", "new");
    assertNotNull(encrypted);
    assertEquals(structEditorService.decryptPassword(encrypted.get("password")), "new");
  }
}
