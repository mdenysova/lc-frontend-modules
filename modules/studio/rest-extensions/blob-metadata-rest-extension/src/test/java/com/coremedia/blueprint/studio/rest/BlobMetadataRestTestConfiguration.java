package com.coremedia.blueprint.studio.rest;

import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:/com/coremedia/cap/common/xml/uapi-xml-services.xml")
class BlobMetadataRestTestConfiguration implements BeanPostProcessor {

  @Override
  public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
    return o;
  }

  @Override
  public Object postProcessAfterInitialization(Object o, String s) throws BeansException {
    if("xmlContentsLocation".equals(s)){
      return "classpath:/com/coremedia/testing/contenttest.xml";
    }
    if ("nonNullCapRepositoryHelper".equals(s)) {
      new BeanWrapperImpl(o).setPropertyValue("assertNonNullBeans", false);
    }
    return o;
  }
}
