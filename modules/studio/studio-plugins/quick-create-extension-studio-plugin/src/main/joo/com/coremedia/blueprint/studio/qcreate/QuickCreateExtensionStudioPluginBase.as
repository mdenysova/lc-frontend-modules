package com.coremedia.blueprint.studio.qcreate {

import com.coremedia.blueprint.studio.qcreate.config.quickCreateExtensionStudioPlugin;
import com.coremedia.blueprint.studio.qcreate.doctypes.CMChannelExtension;
import com.coremedia.cms.editor.configuration.StudioPlugin;
import com.coremedia.cms.editor.sdk.IEditorContext;

/**
 * Extension Plugin for the Quick Create Dialog.
 * Register Doctype specific UIs and Processors.
 */
public class QuickCreateExtensionStudioPluginBase extends StudioPlugin {
  public function QuickCreateExtensionStudioPluginBase(config:quickCreateExtensionStudioPlugin) {
    super(config);
  }

  override public function init(editorContext:IEditorContext):void {
    // Register Navigation Parent for CMChannel
    CMChannelExtension.register();
  }
}
}
