package com.coremedia.blueprint.studio.config.contentchooser {

import com.coremedia.ui.data.ValueExpression;

import ext.config.action;

/**
 * Shows the dialog for choosing content for a ValueExpression.
 *
 * @see com.coremedia.blueprint.studio.property.contentchooser.OpenContentChooserAction
 */
[ExtConfig(target="com.coremedia.blueprint.studio.property.contentchooser.OpenContentChooserAction")]
public dynamic class openContentChooserAction extends action {

  public function openContentChooserAction(config:Object = null) {
    super(config || {});
  }

  /**
   * The set for settings to search for content with.
   */
  public native function get settings():Array;
  public native function set settings(value:Array):void;

  public native function get dialogTitle():String;
  public native function set dialogTitle(value:String):void;

  /**
   * The name of the root name.
   */
  public native function get rootNodeName():String;
  public native function set rootNodeName(value:String):void;

  /**
   * The contents value expression.
   */
  public native function get valuesExpression():ValueExpression;
  public native function set valuesExpression(value:ValueExpression):void;

  /**
   * If true, only a single value can be selected.
   */
  public native function get singleSelection():Boolean;
  public native function set singleSelection(value:Boolean):void;

  /**
   * The active content, mandatory to resolve the multi-side settings.
   */
  public native function get bindTo():ValueExpression;
  public native function set bindTo(value:ValueExpression):void;

  public native function get folders():String;
  public native function set folders(value:String):void;

  public native function get contentType():String;
  public native function set contentType(value:String):void;
}
}