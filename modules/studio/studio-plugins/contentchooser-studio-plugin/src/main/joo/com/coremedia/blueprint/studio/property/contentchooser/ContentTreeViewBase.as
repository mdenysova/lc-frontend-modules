package com.coremedia.blueprint.studio.property.contentchooser {
import com.coremedia.blueprint.studio.config.contentchooser.contentTreeView;
import com.coremedia.cap.content.Content;
import com.coremedia.ui.data.ValueExpression;

import ext.tree.TreeNode;
import ext.tree.TreePanel;
import ext.util.Observable;

/**
 * Base implementation of the content tree.
 */
public class ContentTreeViewBase extends TreePanel {

  public static const SELECTION_CLASS:String = "settings-form-node-label";

  private var treeModel:ContentTreeModel;
  private var treeSelectionValueExpression:ValueExpression;
  private var selectedValuesExpression:ValueExpression;
  private var taxonomy:String;
  private var singleSelection:Boolean;

  /**
   * @cfg {Boolean} singleSelection If true, only a single selection can be made from the tree.
   * @cfg {ValueExpression} treeSelectionValueExpression The current tree selection.
   * @cfg {ValueExpression} selectedValuesExpression The selected values.
   * @param config
   */
  public function ContentTreeViewBase(config:contentTreeView) {
    super(config);
    this.singleSelection = config.singleSelection;
    this.treeSelectionValueExpression = config.treeSelectionValueExpression;
    this.selectedValuesExpression = config.selectedValuesExpression;
    this.taxonomy = config.taxonomy;

    mon(this, "checkchange", handleCheckChangeClick);

    getSelectionModel().addListener('selectionchange', selectionChanged);
    treeModel = new ContentTreeModel(this, config.rootNodeName, config.content, config.folders, config.contentType, selectedValuesExpression, treeSelectionValueExpression);

    addListener('afterlayout', refreshTree);
  }

  private function refreshTree():void {
    treeModel.refreshTree();
  }

  /**
   * Returns the tree model of the tree.
   * @return
   */
  public function getContentTreeModel():ContentTreeModel {
    return treeModel;
  }

  /**
   * Executed for a selection change in the tree.
   * @return
   */
  private function selectionChanged(selectionModel:Observable, node:TreeNode):void {
    treeSelectionValueExpression.setValue(node);
  }

  /**
   * Event implementation for checkbox checked/unchecked events.
   * @param node
   * @param checked
   */
  private function handleCheckChangeClick(node:TreeNode, checked:Boolean):void {
    var selection:Content = node.attributes.content;
    var selectedValues:Array = selectedValuesExpression.getValue();
    if (checked) {
      //apply style..
      node.getUI().addClass(SELECTION_CLASS);
      if (singleSelection) { //remove  existing selection.
        treeModel.uncheckAll(node);
        selectedValues = [];
      }
      selectedValues.push(selection);
    }
    else {
      //...or remove style
      node.getUI().removeClass(SELECTION_CLASS);
      //remove value from selection value expression.
      for (var i:int = 0; i < selectedValues.length; i++) {
        if ((selectedValues[i] as Content).getUriPath() === selection.getUriPath()) {
          selectedValues.splice(i, 1);
          break;
        }
      }
    }

    //apply new value array.
    selectedValuesExpression.setValue(selectedValues);
  }


}
}