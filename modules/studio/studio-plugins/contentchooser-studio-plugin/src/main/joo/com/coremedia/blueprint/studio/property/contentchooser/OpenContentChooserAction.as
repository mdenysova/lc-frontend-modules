package com.coremedia.blueprint.studio.property.contentchooser {
import com.coremedia.blueprint.studio.config.contentchooser.contentChooser;
import com.coremedia.blueprint.studio.config.contentchooser.openContentChooserAction;
import com.coremedia.cap.content.Content;

import ext.Action;
import ext.Ext;
import ext.config.action;

/**
 * Shows the dialog for choosing content for a ValueExpression.
 */
public class OpenContentChooserAction extends Action {

  public function OpenContentChooserAction(config:openContentChooserAction) {
    super(action(Ext.apply({
      handler: function():void {
        var chooser:ContentChooser = new ContentChooser(contentChooser({
          dialogTitle: config.dialogTitle,
          rootNodeName: config.rootNodeName,
          valuesExpression: config.valuesExpression,
          singleSelection: config.singleSelection,
          settings: config.settings,
          contentType: config.contentType,
          folders: config.folders,
          content: config.bindTo.getValue() as Content
        }));
        chooser.show();
      }
    }, config)));
  }
}
}
