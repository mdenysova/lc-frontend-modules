package com.coremedia.blueprint.studio.property.contentchooser {
import com.coremedia.blueprint.studio.ContentchooserStudioPlugin_properties;
import com.coremedia.blueprint.studio.config.contentchooser.contentChooser;
import com.coremedia.cap.content.Content;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.ValueExpressionFactory;
import com.coremedia.ui.data.beanFactory;

import ext.Ext;
import ext.MessageBox;
import ext.Window;
import ext.form.TextField;
import ext.util.StringUtil;

/**
 * The Content Chooser window class, encapsulating the value expression
 * mandatory for the whole dialog.
 */
public class ContentChooserBase extends Window {


  private var filterValueExpression:ValueExpression;
  private var treeStatusExpression:ValueExpression;
  private var treeSelectionExpression:ValueExpression;
  private var selectedValuesExpression:ValueExpression;
  private var valuesExpression:ValueExpression;

  private var treeView:ContentTreeView;
  private var singleSelection:Boolean;

  public function ContentChooserBase(config:contentChooser = undefined) {
    this.valuesExpression = config.valuesExpression;
    this.singleSelection = config.singleSelection;

    var selectedValuesExpression:ValueExpression = valuesExpression;
    var activeSelection:Array = getSelectedValuesExpression().getValue();
    if (selectedValuesExpression.getValue()) {
      var previousSelection:Array = selectedValuesExpression.getValue() as Array;
      if (!previousSelection) {
        previousSelection = [];
        var selectedContent:Content = selectedValuesExpression.getValue() as Content;
        if (selectedContent) {
          previousSelection.push(selectedContent);
        }
      }

      for (var i:int = 0; i < previousSelection.length; i++) {
        activeSelection[i] = previousSelection[i];
        if (singleSelection) { //ignore other selections
          break;
        }
      }
    }

    super(config);
  }

  /**
   * Error message shown when configuration is not correct.
   * @param folder
   */
  public static function showConfigurationError(folder:String):void {
    MessageBox.alert(ContentchooserStudioPlugin_properties.INSTANCE.Settings_error, StringUtil.format(ContentchooserStudioPlugin_properties.INSTANCE.Settings_folder_not_found, folder));
  }


  /**
   * Returns the value expression for the tree node status.
   * @return
   */
  protected function getTreeStatusValueExpression():ValueExpression {
    if (!treeStatusExpression) {
      treeStatusExpression = ValueExpressionFactory.create('treeStatus', beanFactory.createLocalBean({treeStatus:''}));
      treeStatusExpression.addChangeListener(treeStatusChanged);
    }
    return treeStatusExpression;
  }

  /**
   * Returns the selected values expression for the tree selection.
   * @return
   */
  protected function getSelectedValuesExpression():ValueExpression {
    if (!selectedValuesExpression) {
      selectedValuesExpression = ValueExpressionFactory.create('selectedValues', beanFactory.createLocalBean({selectedValues:''}));
      selectedValuesExpression.setValue([]);
    }
    return selectedValuesExpression;
  }

  /**
   * Returns the value expression for the tree selection.
   * @return
   */
  protected function getTreeSelectionValueExpression():ValueExpression {
    if (!treeSelectionExpression) {
      treeSelectionExpression = ValueExpressionFactory.create('treeSelection', beanFactory.createLocalBean({treeSelection:''}));
    }
    return treeSelectionExpression;
  }

  /**
   * Returns the value expression for the status (expand/collapse) of the tree.
   * @return
   */
  protected function getFilterValueExpression():ValueExpression {
    if (!filterValueExpression) {
      filterValueExpression = ValueExpressionFactory.create('filter', beanFactory.createLocalBean({filter:''}));
      filterValueExpression.addChangeListener(filterChanged);
    }
    return filterValueExpression;
  }

  /**
   * Returns the tree view of the dialog.
   */
  protected function getContentTreeView():ContentTreeView {
    if (!treeView) {
      treeView = Ext.getCmp('treeView') as ContentTreeView;
    }

    return treeView;
  }

  /**
   * Invoked when expand or collapsed has been pressed.
   */
  private function treeStatusChanged(valueExpression:ValueExpression):void {
    var status:String = valueExpression.getValue();
    if (status === ContentTreeModel.STATUS_RELOAD) {
      getContentTreeView().getContentTreeModel().reloadModel();
      valueExpression.setValue('');
    }
    else if (status === ContentTreeModel.STATUS_RESET) {
      (Ext.getCmp('treeFilter') as TextField).setValue('');
      getContentTreeView().getContentTreeModel().resetFilter();
      valueExpression.setValue('');
    }
  }


  /**
   * Invoked for filter changed events.
   */
  private function filterChanged(valueExpression:ValueExpression):void {
    var filter:String = valueExpression.getValue();
    getContentTreeView().getContentTreeModel().applyFilter(filter);
  }

  /**
   * Handler for the ok button of the dialog.
   */
  protected function okHandler():void {
    var selection:Array = selectedValuesExpression.getValue();
    if (selection) {
      valuesExpression.setValue(selection); //assign the values to the given value expression.
    }
    this.close();
  }

  /**
   * Just closes the window, selections are ignored.
   */
  protected function cancelHandler():void {
    this.close();
  }
}
}
