package com.coremedia.blueprint.studio.property.contentchooser {
import com.coremedia.blueprint.studio.config.contentchooser.resetFilterAction;
import com.coremedia.ui.data.ValueExpression;

import ext.Action;
import ext.Ext;
import ext.config.action;

/**
 * Resets the filter string of the filter area, displays all tree nodes afterwards.
 * The taxonomy tree  is re-build afterwards, but not reloaded.
 */
public class ResetFilterAction extends Action {
  private var treeStatusValueExpression:ValueExpression;

  /**
   * @cfg {ValueExpression} treeStatusValueExpression The value expression stores the active status of the tree.
   * @param config
   */
  public function ResetFilterAction(config:resetFilterAction = undefined) {
    this.treeStatusValueExpression = config.treeStatusValueExpression;
    super(action(Ext.apply({
      handler: function():void {
        treeStatusValueExpression.setValue('reset');
      }
    }, config)));
  }
}
}
