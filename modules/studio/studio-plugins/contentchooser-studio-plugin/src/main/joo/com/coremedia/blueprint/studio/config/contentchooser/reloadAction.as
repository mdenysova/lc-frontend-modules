package com.coremedia.blueprint.studio.config.contentchooser {

import com.coremedia.ui.data.ValueExpression;

import ext.config.action;

/**
 * Reloads the taxonomies from the server.
 *
 * @see com.coremedia.blueprint.studio.property.contentchooser.ReloadAction
 */
[ExtConfig(target="com.coremedia.blueprint.studio.property.contentchooser.ReloadAction")]
public dynamic class reloadAction extends action {

  public function reloadAction(config:Object = null) {
    super(config || {});
  }

  /**
   * The value expression contains the current status of the tree.
   */
  public native function get treeStatusValueExpression():ValueExpression;
  public native function set treeStatusValueExpression(value:ValueExpression):void;
}
}