package com.coremedia.blueprint.studio.config.contentchooser {

import com.coremedia.ui.data.ValueExpression;

import ext.config.action;

[ExtConfig(target="com.coremedia.blueprint.studio.property.contentchooser.ApplyFilterAction")]
public dynamic class applyFilterAction extends action {

  public function applyFilterAction(config:Object = null) {
    super(config || {});
  }

  /**
   * The filter value expression contains the active filter string.
   */
  public native function get filterValueExpression():ValueExpression;
  public native function set filterValueExpression(value:ValueExpression):void;
}
}