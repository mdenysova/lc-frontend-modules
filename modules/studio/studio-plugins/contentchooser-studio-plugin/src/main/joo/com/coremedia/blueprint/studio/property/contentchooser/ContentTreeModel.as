package com.coremedia.blueprint.studio.property.contentchooser {
import com.coremedia.blueprint.studio.util.ContentUtil;
import com.coremedia.cms.editor.sdk.util.PathFormatter;
import com.coremedia.cap.content.Content;
import com.coremedia.cms.editor.sdk.editorContext;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.util.EncodingUtil;
import com.coremedia.ui.util.EventUtil;

import ext.config.treesorter;
import ext.tree.AsyncTreeNode;
import ext.tree.TreeNode;
import ext.tree.TreePanel;
import ext.tree.TreeSorter;

/**
 * Creates the json tree model, filters and refreshes the view.
 */
public class ContentTreeModel {

  public static const STATUS_RELOAD:String = 'reload';
  public static const STATUS_RESET:String = 'reset';
  public static const CONTENT_SETTINGS_ICON:String = "content-type-xs content-type-CMSettings-icon";

  private var nodeModel:* = {};
  private var root:AsyncTreeNode;
  private var tree:TreePanel;
  private var filterString:String;
  private var treeSelectionValueExpression:ValueExpression;
  private var selectedValuesExpression:ValueExpression;
  private var folders:String;
  private var contentType:String;

  private var content:Content;

  public function ContentTreeModel(tree:TreePanel, rootNodeName:String, content:Content, folders:String, contentType:String, selectedValuesExpression:ValueExpression, treeSelectionValueExpression:ValueExpression) {
    this.treeSelectionValueExpression = treeSelectionValueExpression;
    this.selectedValuesExpression = selectedValuesExpression;
    this.contentType = contentType;
    this.folders = folders;
    this.content = content;
    this.tree = tree;

    nodeModel = {text: rootNodeName, id: 0, leaf: false};
    root = new AsyncTreeNode(nodeModel);
    tree.setRootNode(root);

    reloadModel();
  }

  /**
   * Returns the unique cache name for the loaded settings.
   * @return
   */
  private function getCacheName():String {
    return 'settingsCache:' + folders;
  }

  /**
   * Creates json Tree Model from the taxonomy root folder.
   * @return
   */
  public function reloadModel():void {
    filterString = null;
    var contentSettings:Array = [];
    var rootFolder:Content = ContentUtil.getRootContent();
    rootFolder.invalidate(function():void {
      loadSettings(contentSettings, rootFolder);
    });
  }

  /**
   * Loads all settings using the setting configurations.
   * @param contentSettings
   * @param rootFolder
   */
  private function loadSettings(contentSettings:Array, rootFolder:Content):void {
    var loadCounter:int = 0;
    var settingsArray:Array = folders.split(',');
    settingsArray.forEach(function(folder:String):void {
      folder = PathFormatter.formatSitePath(folder, content);
      rootFolder.getChild(folder, function(settingsFolder:Content, absolutePath:String):void {
        if (!settingsFolder) {
//          ContentChooserBase.showConfigurationError(folder);
        }
        else {
          settingsFolder.invalidate(function():void {
            loadCounter += settingsFolder.getChildren().length;
            settingsFolder.getChildren().forEach(function(child:Content):void {
              child.invalidate(function():void {
                loadCounter--;
                var contentName:String = child.getType().getName();
                if (contentName === contentType) {
                  contentSettings.push(child);
                }

                if (loadCounter === 0) {
                  editorContext.getApplicationContext().set(getCacheName(), contentSettings);
                  refreshTree();
                }
              });
            });
          });
        }

      });
    });
  }


  /**
   * Expands all nodes.
   */
  public function expandAll():void {
    root.expand(true, false, null, null);
  }

  /**
   * Resets the filter.
   */
  public function resetFilter():void {
    applyFilter(null);
  }

  /**
   * Unchecks all, except the given node.
   * @param node
   */
  public function uncheckAll(node:TreeNode):void {
    toggleCheck(root, node, false);
  }

  /**
   * Sets the filter string that is applied when the tree model
   * is refreshed.
   * @param filterString The name to filter for.
   */
  public function applyFilter(filterString:String):void {
    this.filterString = filterString;
    refreshTree();
    treeSelectionValueExpression.setValue(null);
  }

  /**
   * Rebuilds the tree using the given model.
   */
  public function refreshTree():void {
    buildTree(nodeModel, getSettings(false));
    var textTreeSorter:TreeSorter = new TreeSorter(tree, treesorter({}));
    textTreeSorter['doSort'](root);   // use Ext public API?
    root.reload(function():void {
      EventUtil.invokeLater(expandAll);
    });
  }

  /**
   * Check/Uncheck all nodes of the tree, starting from the given node.
   * @param node Node to start with.
   * @param isCheck
   */
  private function toggleCheck(node:TreeNode, ignoreNode:TreeNode, isCheck:Boolean):void {
    if (node) {
      node.cascade(function(child:TreeNode):void {
        if (child.attributes.content && child.attributes.content.getUriPath() !== ignoreNode.attributes.content.getUriPath()) {
          child.getUI().toggleCheck(isCheck);
          child.getUI().removeClass(ContentTreeViewBase.SELECTION_CLASS);
          child.attributes.checked = isCheck;
        }
      });
    }
  }


  /**
   * Returns the root nodes for the active taxonomy.
   * @return
   */
  private function getSettings(force:Boolean):Array {
    var settings:Array = editorContext.getApplicationContext().get(getCacheName());
    if (!settings || force) {
      settings = [];
      editorContext.getApplicationContext().set(getCacheName(), settings);
    }

    return settings;
  }

  /**
   * Recursive call to build the json tree model based
   * on the taxonomy content folder.
   * @param node The json root node of the current iteration.
   * @param children The taxonomy children of the current node.
   */
  private function buildTree(node:Object, children:Array):void {
    node.children = [];
    children.forEach(function(child:Content):void {
      if (!filterString || filterString.length === 0 || child.getName().toLowerCase().indexOf(filterString.toLowerCase()) !== -1) {
        //check for filter settings
        var childNode:Object = createNode(child);
        node.children.push(childNode);
      }
    });
  }

  /**
   * Creates a new taxonomy node.
   * @param content The taxonomy content object
   * @return The TreeNode object.
   */
  private function createNode(content:Content):Object {
    var selected:Boolean = isSelected(content);
    return {
      id: content.getUriPath(),
      text: EncodingUtil.encodeForHTML(content.getName()),
      cls : getSelectionStyle(selected),
      iconCls: CONTENT_SETTINGS_ICON,
      checked: selected,
      leaf: true,
      content: content
    };
  }

  /**
   * Returns the style for selected nodes.
   * @param selected
   * @return
   */
  private function getSelectionStyle(selected:Boolean):String {
    if (selected) {
      return ContentTreeViewBase.SELECTION_CLASS;
    }
    return '';
  }

  /**
   * Returns true if the given content is already
   * selected for the property field.
   * @param child The child to check the selection for.
   * @return
   */
  private function isSelected(child:Content):Boolean {
    var selection:Array = selectedValuesExpression.getValue();
    if (selection) {
      for (var i:int = 0; i < selection.length; i++) {
        if (selection[i].getUriPath() === child.getUriPath()) {
          return true;
        }
      }
    }
    return false;
  }


}
}