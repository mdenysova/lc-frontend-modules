package com.coremedia.blueprint.studio.property.contentchooser {
import com.coremedia.blueprint.studio.config.contentchooser.applyFilterAction;
import com.coremedia.ui.data.ValueExpression;

import ext.Action;
import ext.Ext;
import ext.config.action;
import ext.form.TextField;

/**
 * Applies the filter string of the filter area, displays all tree nodes afterwards.
 */
public class ApplyFilterAction extends Action {

  private var filterValueExpression:ValueExpression;

  /**
   * @cfg {ValueExpression} filterValueExpression The filter value expression contains the active filter string.
   * @param config
   */
  public function ApplyFilterAction(config:applyFilterAction = undefined) {
    this.filterValueExpression = config.filterValueExpression;
    super(action(Ext.apply({
      handler: function():void {
        var filterField:TextField = Ext.getCmp('treeFilter') as TextField;
        var filterString:String = filterField.getValue();
        filterValueExpression.setValue(filterString);
      }
    }, config)));
  }
}
}
