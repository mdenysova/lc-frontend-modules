package com.coremedia.blueprint.studio.property.contentchooser {
import com.coremedia.blueprint.studio.config.contentchooser.reloadAction;
import com.coremedia.ui.data.ValueExpression;

import ext.Action;
import ext.Ext;
import ext.config.action;

/**
 * Reloads the taxonomies from the server.
 */
public class ReloadAction extends Action {

  private var treeStatusValueExpression:ValueExpression;

  /**
   * @cfg {ValueExpression} treeStatusValueExpression The value expression contains the current status of the tree.
   * @param config
   */
  public function ReloadAction(config:reloadAction = undefined) {
    this.treeStatusValueExpression = config.treeStatusValueExpression;
    super(action(Ext.apply({
      handler: function():void {
        treeStatusValueExpression.setValue('reload');
      }
    }, config)));
  }
}
}
