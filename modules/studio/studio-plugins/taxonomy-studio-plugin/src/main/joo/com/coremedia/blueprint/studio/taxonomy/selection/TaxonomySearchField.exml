<?xml version="1.0" encoding="UTF-8"?>
<exml:component xmlns:exml="http://www.jangaroo.net/exml/0.8"
                xmlns="exml:ext.config"
                xmlns:editor="exml:com.coremedia.cms.editor.sdk.config"
                baseClass="com.coremedia.blueprint.studio.taxonomy.selection.TaxonomySearchFieldBase">

  <exml:import class="ext.form.Field"/>
  <exml:import class="com.coremedia.blueprint.studio.taxonomy.TaxonomyStudioPlugin_properties"/>

  <exml:cfg name="searchResultExpression" type="com.coremedia.ui.data.ValueExpression">
    <exml:description>
      Contains a list of hits that match with the current search value.
    </exml:description>
  </exml:cfg>

  <exml:cfg name="siteSelectionExpression" type="com.coremedia.ui.data.ValueExpression">
    <exml:description>
      Contains the name of the site that should be included.
    </exml:description>
  </exml:cfg>

  <exml:cfg name="bindTo" type="com.coremedia.ui.data.ValueExpression">
    <exml:description>
      The value expression that contains the editors content.
    </exml:description>
  </exml:cfg>

  <exml:cfg name="forceReadOnlyValueExpression" type="com.coremedia.ui.data.ValueExpression">
    <exml:description>
       An optional ValueExpression which makes the component read-only if it is evaluated to true.
    </exml:description>
  </exml:cfg>

  <exml:cfg name="propertyName" type="String">
    <exml:description>
      The property name that is edited
    </exml:description>
  </exml:cfg>

  <exml:cfg name="showSelectionPath" type="Boolean">
    <exml:description>
      If true, the path of the selected node is shown as plain string in the textfield, empty string otherwise.
      Default is true.
    </exml:description>
  </exml:cfg>

  <exml:cfg name="taxonomyId" type="String">
    <exml:description>the id of the taxonomy that's tree is used to add items from.</exml:description>
  </exml:cfg>

  <exml:cfg name="resetOnBlur" type="Boolean">
    <exml:description>True, if the search field is used for the taxonomy administration, defaults to false.</exml:description>
  </exml:cfg>

  <combo loadingText="{TaxonomyStudioPlugin_properties.INSTANCE.TaxonomySearch_loading_text}"
         forceSelection="false"
         autoSelect="false"
         enableKeyEvents="true"
         mode="remote"
         typeAhead="false"
         hideTrigger="true"
         triggerAction="all"
         minChars="2"
         queryDelay="200"
         queryParam="text"
         pageSize="0"
         listWidth="600"
         cls="taxonomy-admin-search-field"
         selectOnFocus="true"
         itemSelector="div.taxonomy-search-item"
         emptyText="{getEmptyText(config.bindTo)}"
         flex="1"
         validator="{tagPrefixValidValidator}">
    <plugins>
      <editor:bindDisablePlugin bindTo="{config.bindTo}"
                                forceReadOnlyValueExpression="{config.forceReadOnlyValueExpression}"/>
    </plugins>
  </combo>

</exml:component>
