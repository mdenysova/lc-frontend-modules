package com.coremedia.blueprint.studio.util.folderchooser {

import com.coremedia.blueprint.studio.util.config.folderChooserDialog;
import com.coremedia.cap.content.Content;
import com.coremedia.cms.editor.sdk.collectionview.tree.LibraryTree;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.ValueExpressionFactory;

import ext.Window;
import ext.tree.TreeNode;

public class FolderChooserDialogBase extends Window {

  public static const REPOSITORY_TREE_ITEMID:String = "LibraryTree";

  public native function get successHandler():Function;

  public native function get cancelHandler():Function;
  private var selectedTreeNodeExpression:ValueExpression;

  private var selectedFolderExpression:ValueExpression;
  private var openPathExpression:ValueExpression;
  private var initialSelectionExpression:ValueExpression;

  public function FolderChooserDialogBase(config:folderChooserDialog = null) {
    super(config);

    this.initialSelectionExpression = config.openPathExpression;
    trace("PATH=" + initialSelectionExpression.getValue());

    mon(this, 'render', initDialog);
  }

  protected function getSelectedTreeNodeExpression():ValueExpression {
    if (!selectedTreeNodeExpression) {
      selectedTreeNodeExpression = ValueExpressionFactory.createFromValue(null);
    }
    return selectedTreeNodeExpression;
  }

  private function initDialog():void {
    var libraryTree:LibraryTree = find('itemId', REPOSITORY_TREE_ITEMID)[0] as LibraryTree;
    if (libraryTree) {

      // Apply initial selection
      var initialSelection:Content = initialSelectionExpression.getValue();
      if (initialSelection) {
        getOpenPathExpression().setValue(initialSelection);
      }
      libraryTree.getSelectionModel().addListener('selectionchange', onSelectionChange);
    }
  }

  private function onSelectionChange(tree:LibraryTree, node:TreeNode):void {
    trace("SEL CHANGE: " + node);
    getSelectedTreeNodeExpression().setValue(node);
  }

  /**
   * Applies the selection and closes the dialog.
   * If provided, the successHandler is called with the selected folder.
   */
  protected function applySelection():void {
    if (successHandler) {

      var selectedFolder:Content = getSelectedFolderExpression().getValue();

      // Set selected folder as open path
      openPathExpression.setValue(selectedFolder);

      successHandler(selectedFolder);
    }
    close();
  }

  protected function getSelectedFolderExpression():ValueExpression {
    if (!selectedFolderExpression) {
      selectedFolderExpression = ValueExpressionFactory.createFromValue();
    }
    return selectedFolderExpression;
  }

  protected function getOpenPathExpression():ValueExpression {
    if (!openPathExpression) {
      openPathExpression = ValueExpressionFactory.createFromValue();
    }
    return openPathExpression;
  }
}
}