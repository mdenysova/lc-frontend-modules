package com.coremedia.blueprint.studio.util {
import com.coremedia.ui.data.Calendar;

import ext.util.StringUtil;

import joo.ResourceBundleAwareClassLoader;

/**
 * Common date utilities such as date formatting and date compare.
 */
public class DateUtil {

  public static const DE_DATETIME_FORMAT:String = 'd.m.y H:i';
  public static const EN_DATETIME_FORMAT:String = 'm/d/y h:i A';

  /**
   * Formats the given date to the long date and time format
   */
  public static function formatDateTime(date:Date):String {
    if (!date) {
      return '-';
    }
    var locale:String = ResourceBundleAwareClassLoader.INSTANCE.getLocale();
    var pattern:String = EN_DATETIME_FORMAT;
    if (locale.toLowerCase() === 'de') {
      pattern = DE_DATETIME_FORMAT;
    }
    var dateString:String = date.format(pattern);
    return dateString;
  }

  /**
   * Formats the given date with the given pattern or
   * returns the minutes if inside the given threshold.
   * @param date The date object to format.
   * @param pattern The pattern to use for formatting.
   * @param threshold The threshold until the minutes should be displayed.
   * @return
   */
  public static function getPostingMinutes(date:Date, pattern:String, threshold:int):String {
    if (isToday(date)) {
      var today:Date = new Date();
      var minutesToday:Number = today.getHours() * 60 + today.getMinutes();
      var minutesDate:Number = date.getHours() * 60 + date.getMinutes();

      var diff:Number = (minutesToday - minutesDate);
      if (diff < threshold) {
        if (diff === 0) {
          diff = 1;
        }
        if (diff === 1) {
          return StringUtil.format(StudioPluginUtils_properties.INSTANCE.Date_format_minute, diff);

        }
        return StringUtil.format(StudioPluginUtils_properties.INSTANCE.Date_format_minutes, diff);
      }
    }

    return date.format(pattern);
  }


  /**
   * Returns true if the passed date is today.
   * @param date The date to check.
   * @return True, if the given date is today.
   */
  public static function isToday(date:Date):Boolean {
    var today:Date = new Date();
    return today.getDate() === date.getDate() && today.getMonth() === date.getMonth() && today.getFullYear() == date.getFullYear();
  }
}
}
