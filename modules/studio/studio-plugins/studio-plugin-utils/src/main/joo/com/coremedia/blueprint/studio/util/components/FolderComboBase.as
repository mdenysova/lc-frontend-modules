package com.coremedia.blueprint.studio.util.components {

import com.coremedia.blueprint.studio.util.ContentCreationUtil;
import com.coremedia.blueprint.studio.util.StudioPluginUtils_properties;
import com.coremedia.blueprint.studio.util.config.folderCombo;
import com.coremedia.cap.common.session;
import com.coremedia.cap.content.ContentType;
import com.coremedia.cms.editor.sdk.util.PathFormatter;
import com.coremedia.ui.components.LocalComboBox;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.ValueExpressionFactory;
import com.coremedia.ui.util.EventUtil;

import ext.Ext;
import ext.data.Record;
import ext.data.Store;

use namespace Ext;

/**
 * Superclass of the folder combo that allows to input or selected previously used folders.
 */
public class FolderComboBase extends LocalComboBox {
  private var availableFolderPathsExpression:ValueExpression;
  private var config:folderCombo;

  public function FolderComboBase(config:folderCombo) {
    if (!config.fieldLabel) {
      config.fieldLabel = StudioPluginUtils_properties.INSTANCE.FolderCombo_title;
    }
    this.config = config;

    super(config);
  }


  override protected function afterRender():void {
    super.afterRender();
    var available:Array = getAvailablePathsExpression().getValue() as Array;
    if (available && available.length > 0) {
      config.bindTo.setValue(available[0]['path']);
    }
    getAvailablePathsExpression().addChangeListener(selectFirst);
  }

  private function selectFirst(source:ValueExpression):void {
    var value:Array = source.getValue();
    if (value && value.length > 0) {
      var selected:String = value[0]['path'];
      setValue(selected);
      config.bindTo.setValue(selected);
    }
  }

  override protected function onDestroy():void {
    getAvailablePathsExpression().removeChangeListener(selectFirst);
    super.onDestroy();
  }

  /**
   * Returns the store that contains the last used folders concatenated with the configured default folders.
   *
   * @param config the folderCombo config
   * @return the store
   */
  protected function createFoldersStore(config:folderCombo):Array {
    // not strictly necessary, left in for compatibility reasons
    var paths:Array = getAvailablePathsExpression().getValue();
    if (paths && paths.length > 0) {
      return paths.map(function(item:Object):String {return item.path});
    }
    return [];
  }

  internal function getAvailablePathsExpression():ValueExpression {
    if (!availableFolderPathsExpression) {
      availableFolderPathsExpression = ValueExpressionFactory.createFromFunction(function():Array {
        var result:Array = [];
        if (getContentType(config)) {
          var lastUsedFolders:Array = ContentCreationUtil.getLastUsedFolders(getContentType(config));
          if (lastUsedFolders && lastUsedFolders.length > 0) {
            result = result.concat(makeRecords(lastUsedFolders));
          }
        }
        result = result.concat(getDefaultFolders(config));
        return result;
      })
    }
    return availableFolderPathsExpression;
  }

  private static function getContentType(config:folderCombo):ContentType {
    var contentType:String = config.contentType;
    if (!contentType) {
      if (config.contentTypeExpression) {
        contentType = config.contentTypeExpression.getValue();
      }
    }

    if (!contentType) {
      trace("neither 'contentType' nor 'contentTypeExpression' available to determine target folder");
      return null;
    }

    return session.getConnection().getContentRepository().getContentType(contentType);
  }

  private static function getDefaultFolders(config:folderCombo):Array {
    var formattedFolders:Array = [];
    if (config.folderPathsExpression) {
        return makeRecords(config.folderPathsExpression.getValue());
    } else {
      var configFolders:Array = config.folders as Array;
      if (configFolders) {
        formattedFolders = makeRecords(configFolders.map(function (path:String):String {
          return PathFormatter.formatSitePath(path);
        }));
      }
    }
    return formattedFolders || [];
  }

  private static function makeRecords(paths:Array):Array {
    var result:Array = [];
    Ext.each(paths, function(path:String, idx:int):Object {
      result.push({id: idx, path:path});
    });
    return result;
  }
}
}
