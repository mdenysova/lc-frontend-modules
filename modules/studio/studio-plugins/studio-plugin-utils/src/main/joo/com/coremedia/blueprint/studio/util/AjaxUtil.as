package com.coremedia.blueprint.studio.util {
import com.coremedia.ui.data.error.RemoteError;
import com.coremedia.ui.data.impl.RemoteService;
import com.coremedia.ui.data.impl.RemoteServiceMethodResponse;

import js.XMLHttpRequest;

public class AjaxUtil {

  /**
   * Loads the XML document and triggers the parsing process. Because the request is fired asynchronously,
   * the link list model will be filled asynchronously too, so the grid will be filled with lazy loading.
   * The model is a bean and fired property change events, so that the store is notified when the
   * model has been changed.
   * @param url The Ajax URL of the property
   */
  public static function requestXML(url:String, onSuccess:Function):void {
    //prepare Ajax request object...
    var requestObject:Object = {
      url : url,
      method: 'GET',
      success: onSuccess,
      failure: onError,
      callback: function():void {
        // no specific callback
      }
    };

    //...and execute the request.
    try {
      RemoteService.getConnection().request(requestObject);
    } catch (e:*) {
      throw new Error("Unable to send Bean " + requestObject.method + " request: " + e);
    }
  }

  /**
   * Default error message when the XMLHttpRequest failed.
   * @param request The XMLHttpRequest of the failed request.
   */
  public static function onErrorResponse(request:XMLHttpRequest):void {
    Logger.info('Request failed:' + request.responseText);
  }

  /**
   * Default error message when the RemoteServiceMethodResponse failed.
   * @param response The RemoteServiceMethodResponse of the failed request.
   */
  public static function onErrorMethodResponse(response:RemoteServiceMethodResponse):void {
    trace('[ERROR]', 'Request failed: ' + response.getError().errorName + '/' + response.getError().errorCode);
  }

  /**
   * Default error message with the given error
   * @param error the given remote error
   */
  public static function onError(error:RemoteError):void {
    trace('[ERROR]', 'Request failed: ' + error.errorName + '/' + error.errorCode);
  }

}
}