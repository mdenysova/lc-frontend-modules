package com.coremedia.blueprint.studio.util.dragdrop {
import com.coremedia.blueprint.studio.util.config.draggablePlugin;
import com.coremedia.ui.data.ValueExpression;

import ext.Component;
import ext.Plugin;

public class DraggablePlugin implements Plugin {
  private var contentExpression:ValueExpression;

  public function DraggablePlugin(config:draggablePlugin) {
    contentExpression = config.bindTo;
    super(config);
  }


  public function init(component:Component):void {
    component.on('afterrender', makeDraggable);
  }

  private function makeDraggable(cmp:Component):void {
    var contentDragZone:ContentDragZone = new ContentDragZone(cmp.getEl(), contentExpression);
    cmp.mon(cmp, "beforedestroy", function ():void {
      contentDragZone.unreg();
    });
  }
}
}
