package com.coremedia.blueprint.studio.util.components {

import com.coremedia.blueprint.studio.util.config.clearableTextField;

import ext.IEventObject;
import ext.form.TriggerField;

public class ClearableTextFieldBase extends TriggerField {

  private var enterHandler:Function;
  private var clearHandler:Function;


  public function ClearableTextFieldBase(config:clearableTextField = null) {
    enterHandler = config.onEnter;
    clearHandler = config.onClear;

    super(clearableTextField(config));

    if (enterHandler) {
      // Register handler function for ENTER pressed event
      addListener('specialkey', function (tf:ClearableTextField, e:IEventObject):void {
        if (e.getKey() === e.ENTER) {
          enterHandler();
        }
      });
    }

  }

  override public function onTriggerClick(e:IEventObject):void {
    reset();
    super.onTriggerClick(e);

    if (clearHandler) {
      clearHandler();
    }
  }
}

}
