package com.coremedia.blueprint.studio.util {
import com.coremedia.ui.util.UrlUtil;
import com.coremedia.ui.util.WindowUtil;

/**
 * A very simple logging facility controlled by hash parameter 'loglevel' with values debug|info|warn|error
 *
 * This class is very private API, so don't use it.
 */
public class Logger {

  private static const LEVELS:Array = ['debug','info','warn','error'];

  public static var DISABLED_LEVELS : Array;
  private static var debugLogger:Function;
  private static var infoLogger:Function;
  private static var warnLogger:Function;
  private static var errorLogger:Function;

  {
    WindowUtil.addHashChangeListener(initLogger);
    initLogger();
  }

  private static function initLogger():void {
    var index:int = LEVELS.indexOf(UrlUtil.getHashParam('loglevel'));
    DISABLED_LEVELS = -1 < index ? LEVELS.slice(0,index) : LEVELS;
    debugLogger = getLogger('debug',"[LOG]");
    infoLogger = getLogger('info',"[INFO]");
    warnLogger = getLogger('warn',"[WARN]");
    errorLogger = getLogger('error',"[ERROR]");
  }

  private static function getLogger(level:String,fallback:String):Function {
    if(DISABLED_LEVELS.indexOf(level) < 0){
      if(window.console && window.console[level]) {
        // chrome needs an extra wrapper
        return function(msg:String):void {
          window.console[level](msg);
        }
      }
      return function(msg:String){
        trace(fallback, msg);
      };
    }
    return null;
  }

  public static function debug(msg:String):void {
    debugLogger && debugLogger(msg);
  }

  public static function info(msg:String):void {
    infoLogger && infoLogger(msg);
  }

  public static function warn(msg:String):void {
    warnLogger && warnLogger(msg);
  }

  public static function error(msg:String):void {
    errorLogger && errorLogger(msg);
  }

}
}
