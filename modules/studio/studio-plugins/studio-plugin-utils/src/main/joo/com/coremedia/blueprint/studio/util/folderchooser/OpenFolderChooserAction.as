package com.coremedia.blueprint.studio.util.folderchooser {

import com.coremedia.blueprint.studio.util.config.folderChooserDialog;
import com.coremedia.blueprint.studio.util.config.openFolderChooserAction;
import com.coremedia.ui.data.ValueExpression;

import ext.Action;

public class OpenFolderChooserAction extends Action {

  private var successHandler:Function;
  private var cancelHandler:Function;
  private var openPathExpression:ValueExpression;

  public function OpenFolderChooserAction(config:openFolderChooserAction = null) {
    this.successHandler = config.successHandler;
    this.cancelHandler = config.cancelHandler;
    this.openPathExpression = config.openPathExpression;
    config.handler = openFolderChooser;
    super(config);
  }

  private function openFolderChooser():void {
    new FolderChooserDialog(folderChooserDialog({
      successHandler: successHandler,
      cancelHandler: cancelHandler,
      openPathExpression: openPathExpression
    })).show();
  }

}
}
