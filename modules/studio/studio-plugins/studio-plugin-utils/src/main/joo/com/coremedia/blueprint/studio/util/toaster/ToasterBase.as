package com.coremedia.blueprint.studio.util.toaster {

import com.coremedia.blueprint.studio.util.config.toaster;
import com.coremedia.ui.components.AnimatedNotification;

/**
 * Super class for the toaster, nothing special so far.
 */
public class ToasterBase extends AnimatedNotification {

  public function ToasterBase(config:toaster) {
    super(config);
  }
}
}
