package com.coremedia.blueprint.studio.util.plugins {

import com.coremedia.blueprint.studio.util.UserUtil;
import com.coremedia.blueprint.studio.util.config.limitVisibilityPlugin;

import ext.Component;
import ext.Container;
import ext.Plugin;

/**
 * The plugin removes/destroys components, depending on the group
 * membership of the current user. The plugin can used in two ways: to
 * destroy the component the plugin is bind to or to remove a component
 * from its parent.
 *
 * Sometimes only the second ways works, e.g. for tab panels whose tabs
 * are rendered when the tab is clicked. Therefore the tab is visible after
 * the page has been loaded, but disappears once the tab item has been selected.
 * In this case, the plugin must be bind to the parent component and
 * specify the item ids (comma separated) that should be visible if the user is
 * in one of the defined groups.
 */
public class LimitVisibilityPlugin implements Plugin {

  private var groups:Array;
  private var itemIds:Array;
  private var positions:Array;

  public function LimitVisibilityPlugin(config:limitVisibilityPlugin) {
    this.groups = config.groups.split(',');
    if (config.itemIds) {
      this.itemIds = config.itemIds.split(',');
    }
    if (config.positions) {
      this.positions = config.positions.split(',');
    }

    super(config);
  }


  /**
   * Destroys/Removes child component of the given component.
   * @param component
   */
  public function init(component:Component):void {

    //Check if the visibility applies for the current user.
    var notVisible:Boolean = false;
    groups.forEach(function(groupName:String):void {
      if (UserUtil.isInGroup(groupName)) {
        notVisible = true;
      }
    });

    //Remove component?
    if (notVisible) {
      if (itemIds || positions) {//Removing via item ids.
        var panel:Container = component as Container;
        panel.addListener('afterrender', function():void {
          if(positions) {
            positions.forEach(function(pos:int):void {
              var container:* = panel.items.itemAt(pos);
              if(container) {
                panel.remove(container);
                container.destroy();
              }
            });
          }

          if(itemIds) {
            itemIds.forEach(function(item:String):void {
              var container:* = panel.find('itemId', item)[0];
              if(container) {
                panel.remove(container);
                container.destroy();
              }
            });
          }

        });
        panel.doLayout(false, true);
      }
      else { //Just destroying works fine...
        component.addListener('afterrender', function():void {
          component.destroy();
        });
      }
    }
  }

}
}
