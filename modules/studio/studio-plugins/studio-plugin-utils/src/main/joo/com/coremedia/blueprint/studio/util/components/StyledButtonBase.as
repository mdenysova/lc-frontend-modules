package com.coremedia.blueprint.studio.util.components {

import com.coremedia.blueprint.studio.util.config.styledButton;

import ext.Button;
import ext.Template;

public class StyledButtonBase extends Button {

  internal static const ICON_BUTTON_TEMPLATE:Template = new Template([
    '<div id="{4}" class="styled-button {2}">',
    '<a href="#">{1}</a>',
    '</div>'
  ]).compile();


  public function StyledButtonBase(config:styledButton = null) {
    super(config);
  }

}
}
