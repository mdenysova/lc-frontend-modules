package com.coremedia.blueprint.studio.util.dragdrop {
import com.coremedia.cap.content.Content;
import com.coremedia.cms.editor.sdk.dragdrop.DragDropVisualFeedback;
import com.coremedia.ui.data.ValueExpression;

import ext.Ext;
import ext.IEventObject;
import ext.config.dragzone;
import ext.dd.DragZone;

import js.HTMLElement;

public class ContentDragZone extends DragZone {
  private var contentValueExpression:ValueExpression;

  public function ContentDragZone(el:*, contentVE:ValueExpression) {
    this.contentValueExpression = contentVE;
    super(el, dragzone({
      ddGroup:"ContentLinkDD",
      scroll:false
    }));
  }

  public function getContent():Content {
    return contentValueExpression.getValue();
  }

  override public function getDragData(e:IEventObject):Object {
    // create an appropriate drag proxy
    var targetEl:HTMLElement = e.getTarget();
    var d:* = targetEl.cloneNode(true);
    d.id = Ext.id();
    d.innerHTML = DragDropVisualFeedback.getHtmlFeedback([getContent()]);

    return {
      contents:[getContent()],
      repairXY:Ext.fly(targetEl).getXY(),
      ddel:d
    };
  }

  override public function getRepairXY(e:IEventObject):Array {
    return dragData.repairXY;
  }
}
}
