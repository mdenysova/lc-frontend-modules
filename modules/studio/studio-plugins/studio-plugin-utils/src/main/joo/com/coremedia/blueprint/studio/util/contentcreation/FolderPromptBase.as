package com.coremedia.blueprint.studio.util.contentcreation {

import com.coremedia.blueprint.studio.util.ContentCreationUtil;
import com.coremedia.blueprint.studio.util.components.FolderPanel;
import com.coremedia.blueprint.studio.util.config.folderPanel;
import com.coremedia.blueprint.studio.util.config.folderPrompt;
import com.coremedia.cap.content.Content;
import com.coremedia.cap.content.ContentPropertyNames;
import com.coremedia.ui.data.ValueExpressionFactory;

import ext.Container;
import ext.Window;
import ext.config.menuseparator;

/**
 * Base class of the folder prompt dialog, contains the current
 * folders marked for creation.
 */
public class FolderPromptBase extends Window {
  private static const HEADER_HEIGHT:Number = 24;
  private static const FOOTER_HEIGHT:Number = 43;
  private static const FIRST_SEPARATOR_HEIGHT:Number = 6;
  private static const NORTH_CONTAINER_MARGIN_TOP:Number = 9;
  private static const SOUTH_CONTAINER_MARGIN_TOP:Number = 4;
  private static const PATH_NAME_ROW_HEIGHT:Number = 27;

  public static const NORTH_CONTAINER_HEIGHT:Number = 21;
  public static const SOUTH_CONTAINER_HEIGHT:Number = 21;

  private var folders:Array;
  private var baseFolder:Content;
  private var callback:Function;

  public function FolderPromptBase(config:folderPrompt) {
    super(config);
    this.folders = config.folders;
    this.callback = config.callback;
    this.baseFolder = config.baseFolder;
    addListener('afterlayout', addPanels);
  }

  /**
   * Add the file panels after render
   */
  private function addPanels():void {
    removeListener('afterlayout', addPanels);
    addFolderItems();
  }

  /**
   * Creates the folder panels and adds
   * them to the dialog.
   */
  private function addFolderItems():void {
    ValueExpressionFactory.create(ContentPropertyNames.PATH, baseFolder).loadValue(function(path:String):void {
      var uploadPanel:Container = find('itemId', 'folder-list')[0];
      var fullPath:String = path + '/' + folders.reverse().join('/');
      for (var i:int = 0; i < folders.length; i++) {
        var panel:FolderPanel = new FolderPanel(new folderPanel({
          name:folders[i],
          path:fullPath
        }));
        uploadPanel.add(panel);
        uploadPanel.doLayout(false, true);
        uploadPanel.add(new menuseparator({cls:'upload-progress-dialog-separator'}));
      }
      uploadPanel.doLayout(false, true);
    });
  }

  /**
   * Calculates the height for this dialog, depending on
   * the amount of folders to create.
   * @param files
   * @return
   */
  protected function getCalculatedHeight(files:Array):Number {
    return (files.length * PATH_NAME_ROW_HEIGHT) + HEADER_HEIGHT
                                                 + FOOTER_HEIGHT
                                                 + NORTH_CONTAINER_HEIGHT
                                                 + NORTH_CONTAINER_MARGIN_TOP
                                                 + FIRST_SEPARATOR_HEIGHT
                                                 + SOUTH_CONTAINER_HEIGHT
                                                 + SOUTH_CONTAINER_MARGIN_TOP;
  }

  protected function okPressed():void {
    close();
    ContentCreationUtil.doCreateFolder(baseFolder, folders.reverse(), callback, [])
  }

  protected function cancelPressed():void {
    close();
    callback(new FolderCreationResultImpl(false, null, null));
  }

}
}
