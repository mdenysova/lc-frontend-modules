package com.coremedia.blueprint.studio.util {
import com.coremedia.cap.common.CapPropertyDescriptorType;
import com.coremedia.cap.common.CapType;
import com.coremedia.cap.common.session;
import com.coremedia.cap.content.Content;
import com.coremedia.cap.content.ContentPropertyNames;
import com.coremedia.cap.content.ContentType;
import com.coremedia.ui.data.Blob;
import com.coremedia.ui.data.RemoteBean;
import com.coremedia.ui.data.beanFactory;
import com.coremedia.ui.util.ObjectUtils;

import ext.Ext;
import ext.KeyMap;

/**
 * Common content related utilities.
 */
public class ContentUtil {
  private static const CAP_CONTENT_TYPE_PREFIX:String = 'coremedia:///cap/contenttype/';
  private static const CAP_CONTENT_PREFIX:String = 'coremedia:///cap/content/';

  private static var root:Content;

  /**
   * Returns true if the given content has the property field with the give name.
   * Returns undefined if the content has not yet been loaded.
   *
   * @param content the content
   * @param name the name of the property
   * @return true if the given content has the property
   */
  public static function hasProperty(content:Content, name:String):Boolean {
    var contentType:ContentType = content.getType();
    if (contentType === undefined) {
      return undefined;
    }
    var desc:Array = contentType.getDescriptors();
    for(var i:int = 0; i<desc.length; i++) {
      if(desc[i].name === name) {
        return true;
      }
    }
    return false;
  }

  /**
   * Returns an array of all content types names that are part of the given content type hierarchy.
   * @param contentType the content type
   * @return an array of all content types names that are part of the given content type hierarchy
   */
  public static function getContentTypeHierarchy(contentType:String):Array {
    var ctTypes:Array = [];
    ctTypes.push(contentType);
    var type:CapType = session.getConnection().getContentRepository().getContentType(contentType);
    while (type.getParent()) {
      var parent:CapType = type.getParent();
      ctTypes.push(parent.getName());
      type = parent;
    }
    return ctTypes;
  }

  /**
   * Reads the content for the given URL.
   * @param ref The string id of the content to load, leading content prefixes will be handled too.
   * @return The content object, load call is executed asynchronously.
   */
  public static function getContent(ref:String):Content {
    var contentId:String = ref;
    if (('' + ref).indexOf('/') != -1) {
      contentId = ref.substr(ref.lastIndexOf('/') + 1, ref.length);
    }
    var content:Content = beanFactory.getRemoteBean("content/" + contentId) as Content;
    content.load();
    return content;
  }

  /**
   * Returns the numeric id of the content instance.
   * @param content The content to retrieve the id from.
   * @return The numeric content id.
   */
  public static function getNumericId(content:*):int {
    return content.getNumericId();
  }

  /**
   * Loads the content repository root.
   * @return The root content object.
   */
  public static function getRootContent():Content {
    if (!root) {
      root = session.getConnection().getContentRepository().getRoot();
      root.load();
    }

    return root;
  }

  /**
   * Ensures that all items of the given content array are loaded.
   * The method is used to skip asynchronous calls afterwards.
   * @param items An array filled with content items.
   * @param callback Called when all items have been loaded.
   */
  public static function loadContents(items:Array, callback:Function):void {
    if(!items || items.length === 0) {
      callback.call(null);
    }
    var index:int = items.length;
    for(var i:int =0; i<items.length; i++) {
      var c:Content = items[i];
      c.load(function():void {
        index--;
        if(index === 0) {
          callback.call(null);
        }
      });
    }
  }


  /**
   * Checks if the given content object is inside the passed array.
   * @param array The array to search in.
   * @param taxonomy The content to search for.
   * @return True if the content is in the array.
   */
  public static function isInArray(array:Array, taxonomy:Content):Boolean {
    if (!array || array.length === 0) {
      return false;
    }

    for (var i:int = 0; i < array.length; i++) {
      if ((array[i] as Content).getUri() === taxonomy.getUri()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Returns the content id in REST format.
   * @param bean The content to retrieve the REST id from.
   * @return
   */
  public static function getRestId(bean:*):String {
    return 'content/' + bean.getNumericId();
  }

  /**
   * Returns true if the given string is a valid content id.
   * @param id The cap content id
   * @return True, if the id has a valid format.
   */
  public static function isCapContentTypeId(id:String):Boolean {
    if (!id || id.indexOf(CAP_CONTENT_TYPE_PREFIX) === -1) {
      return false;
    }
    var numericId:* = parseInt(id.substr(id.lastIndexOf('/') + 1, id.length));
    return typeof numericId == "number";
  }

  /**
   * Returns true if the given string is a valid content id.
   * @param id The cap content id
   * @return True, if the id has a valid format.
   */
  public static function isCapContentId(id:String):Boolean {
    if (!id || id.indexOf(CAP_CONTENT_PREFIX) === -1) {
      return false;
    }
    var numericId:* = parseInt(id.substr(id.lastIndexOf('/') + 1, id.length));
    return typeof numericId == "number";
  }

  /**
   * Returns the formatted content REST id, formatted using the CAP id.
   * @param ref
   * @return
   */
  public static function getRestIdFromCapId(ref:String):String {
    return 'content/' + ref.substr(ref.lastIndexOf('/') + 1, ref.length);
  }

  /**
   * Formats the CAP id for the content type name.
   * @param linkType The type to format the name for.
   * @return
   */
  public static function getContentTypeCapId(linkType:String):String {
    return CAP_CONTENT_TYPE_PREFIX + linkType;
  }

  /**
   * Applies the key binding for the console export.
   */
  public static function bindExport():void {
    var el:* = Ext.getDoc();
    var map:KeyMap = new KeyMap(el, {
      key:'e',
      shift:true,
      ctrl:true,
      alt:false,
      handler:ContentUtil.consoleExport,
      scope:undefined,
      stopEvent:undefined
    });
    map = new KeyMap(el, {
      key:'x',
      shift:true,
      ctrl:true,
      alt:false,
      handler:ContentUtil.consoleExport,
      scope:undefined,
      stopEvent:undefined
    });
  }

  public static function consoleExport():void {
    var content:Content = StudioUtil.getActiveContent();
    if (content) {
      preloadExport(content, function ():void {
        var desc:Array = content.getType().getDescriptors();
        var buffer:String = "\n'" + content.getName() + "'/" + content.getType().getName() + "/"
                + ContentUtil.getNumericId(content)
                + "\n======================================================\n";
        buffer += ContentPropertyNames.LIFECYCLE_STATUS + ":" + content.get(ContentPropertyNames.LIFECYCLE_STATUS) + "\n";
        buffer += ContentPropertyNames.PREVIEW_URL + ":" + content.get(ContentPropertyNames.PREVIEW_URL) + "\n";
        buffer += "------------------------------------------------------\n";
        for (var i:int = 0; i < desc.length; i++) {
          var name:String = desc[i].name;
          if (desc[i].type == CapPropertyDescriptorType.STRING ||
                  desc[i].type == CapPropertyDescriptorType.DATE ||
                  desc[i].type == CapPropertyDescriptorType.INTEGER ||
                  desc[i].type == CapPropertyDescriptorType.BOOLEAN) {
            buffer += name + ':' + content.getProperties().get(name) + '\n';
          }
          else if (desc[i].type == CapPropertyDescriptorType.BLOB) {
            buffer += name + ': -binary-\n';
          }
          else if (desc[i].type == CapPropertyDescriptorType.MARKUP) {
            var markupBlob:Blob = content.getProperties().get(name);
            if (markupBlob && markupBlob.getData()) {
              var markupDoc:* = XMLUtil.parseXML(markupBlob.getData());
              var markupXml:String = XMLUtil.serializeToString(markupDoc);
              buffer += name + ':' + markupXml + '\n';
            }
            else {
              buffer += name + ':' + '-' + '\n';
            }
          }
          else if (desc[i].type == CapPropertyDescriptorType.STRUCT) {
            var remoteBean:RemoteBean = content.getProperties().get(name);
            if (remoteBean) {
              if(remoteBean.isLoaded()) {
                buffer += "------------------------------------------------------\n";
                buffer += name + ':\n';
                buffer  += prettyPrintStructProperty(ObjectUtils.getPublicProperties(remoteBean.toObject()),1);
                buffer += "------------------------------------------------------\n";
              } else {
                buffer += name + ': NOT LOADED\n';
              }
            }
            else {
              buffer += name + ': -\n';
            }
          }
        }
        buffer += "======================================================\n";
        trace(buffer);
      });
    }
  }

  private static function prettyPrintStructProperty(obj:Object, indent:int):String {
    var result:String = '';
    for(var p:String in obj) {
      if('$Struct' !== p) {
        var val:* = obj[p];
        for(var i:int = 0; i < indent; i++) {
          result += ' ';
        }
        result += p + ':';
        if(typeof(val) === "object"){
          result += '\n' + prettyPrintStructProperty(val, indent + 1);
        } else if(typeof(val) !== "function") {
          result += val + '\n';
        }
      }
    }
    return result;
  }

  private static function preloadExport(content:Content, callback:Function):void {
    var desc:Array = content.getType().getDescriptors();
    var structs:Array = [];
    for (var i:int = 0; i < desc.length; i++) {
      var name:String = desc[i].name;
      if (desc[i].type == CapPropertyDescriptorType.STRUCT) {
        var remoteBean:RemoteBean = content.getProperties().get(name);
        structs.push(remoteBean);
      }
    } if (structs.length == 0) {
      callback.call(null);
    }

    var index:int = structs.length;
    structs.forEach(function (structBean:RemoteBean):void {
      if(structBean) {
        structBean.load(function ():void {
          index--;
          if (index == 0) {
            callback.call(null);
          }
        });
      } else {
        index--;
        if (index == 0) {
          callback.call(null);
        }
      }
    });
  }

}
}