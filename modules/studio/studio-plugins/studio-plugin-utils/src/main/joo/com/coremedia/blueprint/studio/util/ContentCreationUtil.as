package com.coremedia.blueprint.studio.util {

import com.coremedia.blueprint.studio.util.config.folderPrompt;
import com.coremedia.blueprint.studio.util.contentcreation.FolderCreationResultImpl;
import com.coremedia.blueprint.studio.util.contentcreation.FolderPrompt;
import com.coremedia.cap.common.session;
import com.coremedia.cap.content.Content;
import com.coremedia.cap.content.ContentCreateResult;
import com.coremedia.cap.content.ContentRepository;
import com.coremedia.cap.content.ContentType;
import com.coremedia.cms.editor.sdk.editorContext;
import com.coremedia.cms.editor.sdk.sites.Site;
import com.coremedia.ui.data.Bean;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.ValueExpressionFactory;
import com.coremedia.ui.data.beanFactory;
import com.coremedia.ui.data.error.remoteErrorHandlerRegistry;
import com.coremedia.ui.data.impl.RemoteErrorHandlerRegistryImpl;
import com.coremedia.ui.util.ArrayUtils;
import com.coremedia.ui.util.EventUtil;

public class ContentCreationUtil {
  private static const LAST_USED_NO_SITE_ID:String = 'NO_SITE';
  private static const LAST_USED_ALL_SITES_ID:String = 'ALL';
  private static const LAST_USED_MAX_ENTRIES:int = 5;

  private static var contentRepository:ContentRepository = session.getConnection().getContentRepository();
  private static var lastUsedBean:Bean = beanFactory.createLocalBean();

  /**
   * Create all required folders for a given path that are currently not present in the repository yet. For example,
   * if path is <code>/a/b/c/d/e</code>, and only the folder <code>/a/b</code> exists, this method will create the
   * folders <code>c</code>, <code>d</code>, and <code>e</code>.
   * @param path the path to create required subfolders for
   * @param callback a function that is called upon completion of the method, successful or not.
   *   Signature: <code>function(result:FolderCreationResult):void</code>
   * @param promptOnCreation when true, a modal dialog is displayed that asks the user to confirm that at least
   *   one subfolder needs to be created
   */
  public static function createRequiredSubfolders(path:String, callback:Function, promptOnCreation:Boolean = false):void {
    determineFolders(path, function (baseFolder:Content, toCreate:Array):void {
      if (promptOnCreation && toCreate.length > 0) {
        var prompt:FolderPrompt = new FolderPrompt(folderPrompt({
          baseFolder:baseFolder,
          callback:callback,
          folders:toCreate
        }));
        prompt.show();
      }
      else {
        doCreateFolder(baseFolder, toCreate, callback, []);
      }
    }, null);
  }

  /**
   * Convenience method to create content. This method creates content, opens it in a new tab, runs initializers if
   * one is found for the given document Type, and updates the registry of last used folders that is used by the
   * {@link NewContentDialog}
   * @param folder the base folder to create content in
   * @param name the name of the document to be created
   * @param type the type of the document to be created
   * @param callback a function that is called upon completion of the content creation process.
   *   Signature: <code>function(createdContent:Content):void</code>
   */
  public static function createContent(folder:Content, openInTab:Boolean, skipInitializers:Boolean, name:String, type:ContentType, callback:Function, callback_error:Function, properties:Object = null):void {
    name = StringHelper.trim(name, StringHelper.stringToCharacter(' '));
    type.createWithProperties(folder, name, properties || {}, function (result:ContentCreateResult):void {
      if (!result.error) {
        var createdContent:Content = result.createdContent;
        createdContent.load(function (c:Content):void {
          if (!skipInitializers) {
            var initializer:Function = editorContext.lookupContentInitializer(createdContent.getType());
            if (initializer) {
              initializer(createdContent);
            }
          }

          storeLastUsedFolder(createdContent);

          if (openInTab) {
            StudioUtil.openInTab(createdContent);
          }
          callback(createdContent);
        });
      }
      else {
        callback_error(result.error);
        if (!result.error.isHandled()) {
          (remoteErrorHandlerRegistry as RemoteErrorHandlerRegistryImpl).handleError(result.error, null);
        }

      }
    });
  }

  private static function storeLastUsedFolder(createdContent:Content):void {
    ValueExpressionFactory.createFromFunction(function():* {
      var site:Site = editorContext.getSitesService().getSiteFor(createdContent);
      var contentType:ContentType = createdContent.getType();
      var folder:Content = createdContent.getParent();

      if (site === undefined) {
        return undefined;
      }

      if (!createdContent.isLoaded()) {
        createdContent.load();
        return undefined;
      }

      var id:String = site ? site.getId() : LAST_USED_NO_SITE_ID;
      var contentTypeName:String = contentType.getName();
      var path:String = folder.getPath();

      if (path === undefined) {
        return undefined;
      }

      updateLastUsedBean(id, contentTypeName, path);
      updateLastUsedBean(LAST_USED_ALL_SITES_ID, contentTypeName, path);

      return null;
    }).loadValue(function():void{});
  }

  private static function updateLastUsedBean(siteId:String, contentType:String, path:String):void {
    var lastUsedExpression:ValueExpression = ValueExpressionFactory.create(
            siteId.concat('.', contentType), lastUsedBean);
    var lastUsed:Array = ArrayUtils.asArray(lastUsedExpression.getValue());

    lastUsed = lastUsed.filter(function (obj:Object):Boolean {
      return obj.path !== path;
    });

    if (lastUsed.length >= LAST_USED_MAX_ENTRIES) {
      lastUsed = lastUsed.slice(0, LAST_USED_MAX_ENTRIES);
    }

    lastUsed.unshift({
      'path': path,
      'timestamp': new Date().getTime()
    });

    lastUsedExpression.setValue(lastUsed);
  }

  /**
   * Return an array that contains the paths of the folders in which the user has created documents of the given type.
   * These are kept track of by the NewContentDialog, which inserts the last used folders per document type. Note
   * that content created via the library tree view does not update this store.
   * When the user has set a preferred site, the last used folders are filtered, so that only those folders are returned
   * that are part of the user's preferred site or not part of any site.
   * @param contentType the content type
   * @return an array containing the paths of the least recently used 5 folders for the document type, newest first
   */
  public static function getLastUsedFolders(contentType:ContentType):Array {
    var contentTypeName:String = contentType.getName();
    var preferredSiteId:String = editorContext.getSitesService().getPreferredSiteId();

    var lastUsed:Array;

    if (preferredSiteId) {
      var lastUsedInSite:Array = getLastUsedFoldersForSiteAndType(preferredSiteId, contentTypeName);
      var lastUsedNoSite:Array = getLastUsedFoldersForSiteAndType(LAST_USED_NO_SITE_ID, contentTypeName);

      lastUsed = lastUsedInSite.concat(lastUsedNoSite).sort(compareTimestamp);
    } else {
      lastUsed = getLastUsedFoldersForSiteAndType(LAST_USED_ALL_SITES_ID, contentTypeName);
    }

    if (lastUsed.length > LAST_USED_MAX_ENTRIES) {
      lastUsed = lastUsed. slice(0, LAST_USED_MAX_ENTRIES);
    }

    return lastUsed.map(function(obj:Object):String {
      return obj.path;
    });
  }

  private static function getLastUsedFoldersForSiteAndType(siteId:String, contentType:String):Array {
    return ArrayUtils.asArray(ValueExpressionFactory.create(
            siteId.concat('.', contentType), lastUsedBean).getValue());
  }

  private static function compareTimestamp(a:Object, b:Object):Number {
    if (a.timestamp > b.timestamp) return -1;
    if (a.timestamp < b.timestamp) return 1;
    if (a.timestamp === b.timestamp) return 0;
  }

  /**
   * Convenience method to create a "post-processor" function that can be used as an "onSuccess" callback function
   * for the {@link NewContentDialog} window to add created content to a linklist
   * @param sourceLinklist a value expression pointing to a linklist property to add the created content to
   * @return a function that takes content as its only parameter, and when called, adds that content to the linklist as
   *   configured through the parameter sourceLinklist
   */
  public static function linkInSourceLinklistHandler(sourceLinklist:ValueExpression, openInTab:Boolean = false):Function {
    return function (created:Content):void {
      var oldValue:Array = sourceLinklist.getValue() as Array;
      sourceLinklist.setValue(oldValue.concat(created));
      if (openInTab) {
        EventUtil.invokeLater(function ():void {
          StudioUtil.openInTab(created);
        });
      }
    }
  }

  public static function doCreateFolder(folder:Content, toCreate:Array, callback:Function, created:Array):void {
    var name:String = toCreate.pop();
    if (name) {
      contentRepository.getFolderContentType().create(folder, name, function (result:ContentCreateResult):void {
        if (result.error) {
          callback(new FolderCreationResultImpl(false, created, null, result.error));
        }
        else {
          result.createdContent.load(function ():void {
            created.push(result.createdContent);
            doCreateFolder(result.createdContent, toCreate, callback, created);
          });
        }
      })
    }
    else {
      callback(new FolderCreationResultImpl(true, created, folder));
    }
  }

  private static function determineFolders(path:String, callback:Function, toCreate:Array):void {
    if(!toCreate) {
      toCreate = [];
    }
    contentRepository.getChild(path, function (folder:Content):void {
      if (!folder) {
        var newPath:String = path.substr(0, path.lastIndexOf('/'));
        toCreate.push(path.substr(path.lastIndexOf('/') + 1));
        determineFolders(newPath, callback, toCreate);
      }
      else {
        if (folder.isDocument()) {
          folder.getParent().load(function ():void {
            callback(folder.getParent(), toCreate)
          });
        }
        else {
          folder.load(function ():void {
            callback(folder, toCreate);
          });
        }
      }
    });
  }
}
}
