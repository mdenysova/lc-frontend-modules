package com.coremedia.blueprint.studio.util.fields.boolean {

import com.coremedia.blueprint.studio.util.config.booleanStructPropertyField;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.ValueExpressionFactory;

import ext.form.Checkbox;

public class BooleanStructPropertyFieldBase extends Checkbox {

  private var localValueExpr:ValueExpression;
  private var remoteValueExpr:ValueExpression;

  public function BooleanStructPropertyFieldBase(config:booleanStructPropertyField = null) {
    super(config);
    remoteValueExpr = config.bindTo.extendBy("properties", config.propertyName);
    mon(this, 'render', loadRemoteValue);
    getLocalValueExpression().addChangeListener(onLocalValueChange);
  }

  /**
   * Loads the remote value from the configured struct property,
   * updates the local value expression and registers handler functions
   * to keep the remote and local value expressions in sync.
   */
  private function loadRemoteValue():void {
    if (remoteValueExpr) {
      remoteValueExpr.loadValue(function(value:Boolean):void {
        getLocalValueExpression().removeChangeListener(onLocalValueChange);
        getLocalValueExpression().setValue(value);
        // Register change listeners
        getLocalValueExpression().addChangeListener(onLocalValueChange);
        getRemoteValueExpression().addChangeListener(onRemoteValueChange);
      });
    }
  }

  /**
   * Updates the remote value expression when the local one changes.
   *
   * @param ve
   */
  private function onLocalValueChange(ve:ValueExpression):void {
    getRemoteValueExpression().setValue(ve.getValue());
  }

  /**
   * Updates the local value expression when the remote one changes.
   * @param ve
   */
  private function onRemoteValueChange(ve:ValueExpression):void {
    getLocalValueExpression().setValue(ve.getValue());
  }

  /**
   * Returns the value expression that holds the local checkbox value.
   *
   * @return
   */
  protected function getLocalValueExpression():ValueExpression {
    if (!localValueExpr) {
      localValueExpr = ValueExpressionFactory.createFromValue(false);
    }
    return localValueExpr;
  }

  /**
   * Returns the value expression that holds the remote struct value.
   *
   * @return
   */
  protected function getRemoteValueExpression():ValueExpression {
    return remoteValueExpr;
  }

}
}
