package com.coremedia.blueprint.studio.util.toaster {

import com.coremedia.blueprint.studio.util.config.toaster;
import com.coremedia.blueprint.studio.util.toaster.Toaster;

public class ToasterUtil {

  /**
   * Displays the toaster with the given title and message.
   * @param title The title of the toaster.
   * @param msg The message the toaster should display.
   */
  public static function showToaster(title:String, msg:String):void {
    var hotToaster:Toaster = new Toaster(toaster({toasterTitle:title, message:msg, target:'actions-toolbar'}));
    hotToaster.show();
  }
}
}
