package com.coremedia.blueprint.studio.util.folderchooser {

import com.coremedia.blueprint.studio.util.config.folderChooserField;
import com.coremedia.blueprint.studio.util.config.openFolderChooserAction;
import com.coremedia.cap.content.Content;
import com.coremedia.cap.content.ContentPropertyNames;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.ValueExpressionFactory;

import ext.Container;

public class FolderChooserFieldBase extends Container {

  private var selectedFolderExpression:ValueExpression;
  private var selectedFolderPathExpression:ValueExpression;
  private var openPathExpression:ValueExpression;

  public function FolderChooserFieldBase(config:folderChooserField = null) {
    this.selectedFolderExpression = config.selectedFolderExpression;
    this.selectedFolderExpression.addChangeListener(updateFolderPath);

    super(config);

    mon(this, "render", function ():void {
      updateFolderPath();
    });
  }

  protected function getSelectedFolderPathExpression():ValueExpression {
    if (!selectedFolderPathExpression) {
      selectedFolderPathExpression = ValueExpressionFactory.createFromValue("");
    }
    return selectedFolderPathExpression;
  }

  protected function getOpenPathExpression():ValueExpression {
    if (!openPathExpression) {
      openPathExpression = ValueExpressionFactory.createFromValue(null);
    }
    return openPathExpression;
  }

  private function updateFolderPath():void {
    selectedFolderExpression.extendBy(ContentPropertyNames.PATH).loadValue(function (path:String):void {
      getSelectedFolderPathExpression().setValue(path);
      getOpenPathExpression().setValue(selectedFolderExpression.getValue());
    });
  }

  protected function onFolderSelect(folder:Content):void {
    folder.load(function (f:Content):void {
      selectedFolderExpression.setValue(f);
    });
  }

  protected function showChooser():void {
    var selection:Content = selectedFolderExpression.getValue();
    new OpenFolderChooserAction(openFolderChooserAction({
      successHandler: onFolderSelect,
      openPathExpression: ValueExpressionFactory.createFromValue(selection)
    })).execute();
  }

}
}
