package com.coremedia.blueprint.studio.util.contentcreation {
import com.coremedia.cap.content.Content;
import com.coremedia.ui.data.error.RemoteError;

public class FolderCreationResultImpl implements FolderCreationResult {
  public function FolderCreationResultImpl(success:Boolean,  createdFolders:Array,  baseFolder:Content, remoteError:RemoteError = null) {
    this['success'] = success;
    this['createdFolders'] = createdFolders;
    this['baseFolder'] = baseFolder;
    this['remoteError'] = remoteError;
  }

  public native function get success():Boolean;

  public native function get createdFolders():Array;

  public native function get baseFolder():Content;

  public native function get remoteError():RemoteError;

}
}
