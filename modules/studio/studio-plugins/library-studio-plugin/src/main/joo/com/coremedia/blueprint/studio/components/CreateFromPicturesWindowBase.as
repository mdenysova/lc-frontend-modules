package com.coremedia.blueprint.studio.components {

import com.coremedia.blueprint.studio.LibrarySettingsStudioPlugin_properties;
import com.coremedia.blueprint.studio.config.library.createFromPicturesWindow;
import com.coremedia.cap.content.Content;
import com.coremedia.cms.editor.sdk.util.PathFormatter;
import com.coremedia.ui.data.Bean;
import com.coremedia.ui.data.Blob;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.ValueExpressionFactory;
import com.coremedia.ui.data.beanFactory;

import ext.Ext;

import ext.Window;

/**
 * Base class for windows to create documents (e.g. gallery from a list of pictures);
 */
public class CreateFromPicturesWindowBase extends Window {

  private var viewModel:Bean;

  /**
   *
   * @param config the config object
   */
  public function CreateFromPicturesWindowBase(config:createFromPicturesWindow) {
    super(config);
  }

  public static function calculateWindowHeight(w:Window):void {
    var workArea:*,calcHeight:Number,windowHeight:Number,offsetY:Number;
    offsetY = 160;
    workArea = Ext.getCmp('workarea');
    calcHeight = workArea.getHeight();
    windowHeight = w.getHeight();
    if (windowHeight >= calcHeight) {
      w.setHeight(calcHeight - offsetY);
    }
  }

  internal function getViewModel(config:createFromPicturesWindow):Bean {
    return getViewModel2(config.preferredName, config.targetFolder, config.selectedPictures);
  }

  internal function getViewModel2(preferredName:String, targetFolder:String, selectedPictures:Array):Bean {
    if (!viewModel) {
      viewModel = beanFactory.createLocalBean({
        name: preferredName || "",
        folder: getFormattedTargetFolder(targetFolder, selectedPictures),
        pictures: selectedPictures || []
      });
    }
    return viewModel;
  }

  internal function isDisabledValueExpression():ValueExpression {
    return ValueExpressionFactory.createFromFunction(function():Boolean {
      return !viewModel || !viewModel.get("name") || !viewModel.get("folder") || Array(viewModel.get("pictures")).length == 0;
    });
  }

  internal static function getFormattedTargetFolder(targetFolder:String, selectedPictures:Array):String {
    var result:String = PathFormatter.formatSitePath(targetFolder || "");
    if (!result) {
      if (selectedPictures && selectedPictures.length > 0) {
        var picture:Content = selectedPictures[0];
        result = PathFormatter.formatSitePath(targetFolder || "", picture);
        if (!result) {
          result = picture.getParent().getPath()
        }
      }
    }
    return result;
  }

  /**
   * Resolved the data url of the images shown in the list.
   * @param content
   * @return
   */
  internal static function resolveDataUrl(content:Content):String {
    var url:String = '';
    if (content && content.getState().readable) {
      var blob:Blob = (content.getProperties().get(LibrarySettingsStudioPlugin_properties.INSTANCE.image_gallery_action_preview_property) as Blob);
      if (blob !== null && blob !== undefined) {
        url = blob.getUri() + '/box;w=100;h=67';
      }
    }
    return url;
  }
}
}