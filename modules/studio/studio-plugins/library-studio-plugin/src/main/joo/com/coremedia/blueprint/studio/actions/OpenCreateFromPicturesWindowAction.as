package com.coremedia.blueprint.studio.actions {

import com.coremedia.blueprint.studio.LibrarySettingsStudioPlugin_properties;
import com.coremedia.blueprint.studio.config.library.openCreateFromPicturesWindowAction;
import com.coremedia.cap.content.Content;
import com.coremedia.cms.editor.sdk.actions.ContentAction;
import com.coremedia.cms.editor.sdk.config.contentAction;

import ext.ComponentMgr;
import ext.Ext;

/**
 * Action for opening a 'create-new-document' wizard based on the currently selected pictures.
 * The actions is only enabled if there is a non-empty selection of images.
 */
public class OpenCreateFromPicturesWindowAction extends ContentAction {

  private var multiSelect:Boolean;
  private var windowConfig:Object;

  public function OpenCreateFromPicturesWindowAction(config:openCreateFromPicturesWindowAction) {
    multiSelect = config.multiSelect;
    windowConfig = config.windowConfig;
    super(contentAction(Ext.apply({}, config, {handler: showWindow})));
  }

  /**
   * Displays the window.
   */
  private function showWindow():void {
    ComponentMgr.create(Ext.apply({}, windowConfig, {selectedPictures : getContents()})).show();
  }

  override protected function isDisabledFor(contents:Array):Boolean {
    if (!multiSelect && contents.length > 1) {
      return true;
    }
    return contents.some(function(content:Content):Boolean {
      return !content.getState().readable || !content.isDocument() ||
             !content.getType() ||
             !content.getType().isSubtypeOf(LibrarySettingsStudioPlugin_properties.INSTANCE.image_gallery_action_content_type);
    });
  }

}
}