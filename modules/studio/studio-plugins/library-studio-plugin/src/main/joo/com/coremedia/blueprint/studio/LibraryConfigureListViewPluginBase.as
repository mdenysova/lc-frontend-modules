package com.coremedia.blueprint.studio {
import com.coremedia.blueprint.studio.config.library.libraryConfigureListViewPlugin;
import com.coremedia.cms.editor.sdk.plugins.ConfigureListViewPlugin;

public class LibraryConfigureListViewPluginBase extends ConfigureListViewPlugin {

  public function LibraryConfigureListViewPluginBase(config:libraryConfigureListViewPlugin) {
    super(config)
  }

  /**
   * Extends the sort by sorting by name.
   *
   * @param field the sortfield which is selected and should be extended
   * @param direction the sortdirection which is selected
   * @return array filled with additional order by statements
   */
  internal static function extendOrderByName(field:String, direction:String):Array {
    var orderBys:Array = [];
    orderBys.push('name ' + direction);
    return orderBys;
  }

}
}