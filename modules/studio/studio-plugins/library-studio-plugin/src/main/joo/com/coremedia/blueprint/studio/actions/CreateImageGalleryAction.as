package com.coremedia.blueprint.studio.actions {

import com.coremedia.blueprint.studio.LibrarySettingsStudioPlugin_properties;
import com.coremedia.blueprint.studio.LibraryStudioPlugin_properties;
import com.coremedia.blueprint.studio.config.library.createImageGalleryAction;
import com.coremedia.blueprint.studio.util.AjaxUtil;
import com.coremedia.blueprint.studio.util.ContentCreationUtil;
import com.coremedia.blueprint.studio.util.contentcreation.FolderCreationResult;
import com.coremedia.cap.common.session;
import com.coremedia.cap.content.Content;
import com.coremedia.cap.content.ContentType;
import com.coremedia.cms.editor.sdk.util.MessageBoxUtil;

import ext.MessageBox;
import ext.util.StringUtil;

/**
 * Creates a new image gallery.
 */
public class CreateImageGalleryAction extends CreateFromPicturesAction {

  private static const IMAGE_GALLERY_CONTENT_TYPE:ContentType = session.getConnection().getContentRepository().getContentType(LibrarySettingsStudioPlugin_properties.INSTANCE.image_gallery_action_target_content_type);

  public function CreateImageGalleryAction(config:createImageGalleryAction) {
    super(config);
  }

  /**
   * Create a new gallery.
   */
  override protected function executePicturesAction(selectedName:String, selectedFolder:String, selectedPictures:Array,
                                                    callback:Function):void {
    ContentCreationUtil.createRequiredSubfolders(selectedFolder, function (result:FolderCreationResult):void {
      if (result.success) {
        result.baseFolder.getChild(selectedName, function (doc:Content, path:String):void {
          if (doc) {
            var msg:String = StringUtil.format(LibraryStudioPlugin_properties.INSTANCE.CreateImageGalleryAction_failure_exists, selectedName);
            MessageBox.alert(LibraryStudioPlugin_properties.INSTANCE.CreateImageGalleryAction_failure_text, msg);
          }
          else {
            const properties:Object = {};
            properties[LibrarySettingsStudioPlugin_properties.INSTANCE.image_gallery_action_target_property] = selectedPictures;
            properties[LibrarySettingsStudioPlugin_properties.INSTANCE.image_gallery_action_target_title_property] = selectedName;
            properties[LibrarySettingsStudioPlugin_properties.INSTANCE.image_gallery_action_target_title_teaser_property] = selectedName;

            ContentCreationUtil.createContent(result.baseFolder, true, false, selectedName, IMAGE_GALLERY_CONTENT_TYPE, callback, AjaxUtil.onError, properties);
          }
        });
      }
      else {
        if (result.remoteError) {
          MessageBoxUtil.showError(LibraryStudioPlugin_properties.INSTANCE.CreateImageGalleryAction_failure_text,
                          LibraryStudioPlugin_properties.INSTANCE.CreateImageGalleryAction_failure_msg + " " + result.remoteError.errorName);
        }
      }
    });
  }
}
}
