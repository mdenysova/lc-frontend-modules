package com.coremedia.blueprint.studio.actions {

import com.coremedia.blueprint.studio.config.library.createFromPicturesAction;
import com.coremedia.ui.data.Bean;

import ext.Action;
import ext.config.action;

/**
 * Base class for actions that create a new collection document from a set of pictures.
 */
public class CreateFromPicturesAction extends Action {

  private var viewModel:Bean;
  private var callback:Function;

  public function CreateFromPicturesAction(config:createFromPicturesAction) {
    var superConfig:action = new action(createFromPicturesAction);
    superConfig.handler = picturesActionHandler;
    super(superConfig);
    viewModel = config.viewModel;
    callback = config.callback;
  }

  private function picturesActionHandler():void {
    executePicturesAction(viewModel.get("name"), viewModel.get("folder"), viewModel.get("pictures"), callback)
  }

  protected function executePicturesAction(selectedName:String, selectedFolder:String, selectedPictures:Array,
                                           callback:Function):void {
    throw new Error("abstract method");
  }
}
}
