package com.coremedia.blueprint.studio.actions {

import com.coremedia.blueprint.studio.ImageMap_properties;
import com.coremedia.blueprint.studio.config.library.createImageMapAction;
import com.coremedia.blueprint.studio.util.AjaxUtil;
import com.coremedia.blueprint.studio.util.ContentCreationUtil;
import com.coremedia.blueprint.studio.util.contentcreation.FolderCreationResult;
import com.coremedia.cap.common.session;
import com.coremedia.cap.content.Content;
import com.coremedia.cap.content.ContentType;
import com.coremedia.cms.editor.sdk.util.MessageBoxUtil;

import ext.MessageBox;
import ext.util.StringUtil;

/**
 * Creates a new Image Map document.
 */
public class CreateImageMapAction extends CreateFromPicturesAction {

  private static const IMAGE_MAP_CONTENT_TYPE:ContentType = session.getConnection().getContentRepository().getContentType(ImageMap_properties.INSTANCE.ImageMap_action_target_content_type);

  public function CreateImageMapAction(config:createImageMapAction) {
    super(config);
  }

  /**
   * Create a new Image Map document with the given values.
   */
  override protected function executePicturesAction(selectedName:String, selectedFolder:String, selectedPictures:Array,
                                                    callback:Function):void {
    ContentCreationUtil.createRequiredSubfolders(selectedFolder, function (result:FolderCreationResult):void {
      if (result.success) {
        result.baseFolder.getChild(selectedName, function (doc:Content, path:String):void {
          if (doc) {
            var msg:String = StringUtil.format(ImageMap_properties.INSTANCE.ImageMap_action_failure_exists, selectedName);
            MessageBox.alert(ImageMap_properties.INSTANCE.ImageMap_action_failure_text, msg);
          } else {
            const properties:Object = {};
            properties[ImageMap_properties.INSTANCE.ImageMap_action_target_property] = selectedPictures;
            properties[ImageMap_properties.INSTANCE.ImageMap_action_target_title_property] = selectedName;
            properties[ImageMap_properties.INSTANCE.ImageMap_action_target_title_teaser_property] = selectedName;

            ContentCreationUtil.createContent(result.baseFolder, true, false, selectedName, IMAGE_MAP_CONTENT_TYPE, callback, AjaxUtil.onError, properties);
          }
        });
      } else {
        if (result.remoteError) {
          MessageBoxUtil.showError(ImageMap_properties.INSTANCE.ImageMap_action_failure_text,
                          ImageMap_properties.INSTANCE.ImageMap_action_failure_msg + " " + result.remoteError.errorName);
        }
      }
    });
  }

}
}