package com.coremedia.blueprint.studio.components {
import com.coremedia.blueprint.studio.config.library.relativeDateFilterFieldset;
import com.coremedia.cms.editor.sdk.collectionview.search.FilterFieldset;
import com.coremedia.ui.data.Bean;

import ext.util.StringUtil;

public class RelativeDateFilterFieldsetBase extends FilterFieldset {

  // use hour granularity to enable solr caching
  // (not NOW/DAY to support different client and server time zones)
  internal static const QUERY_FRAGMENT:String = '({0}:[NOW/HOUR-{1}DAYS TO NOW/HOUR+1HOUR] AND isdeleted:false)';
  internal static const ANY_DATE:int = -1;

  private var queryTemplate:String;

  private var fieldName:String;

  public function RelativeDateFilterFieldsetBase(config:relativeDateFilterFieldset) {
    fieldName = config.dateFieldName || 'modificationdate';

    queryTemplate = config.queryTemplate || QUERY_FRAGMENT;

    super(config);
  }


  override public function buildQuery():String {
    var state:Bean = getStateBean();
    var freshness:int = state.get(fieldName);
    if (freshness === ANY_DATE) {
      return null;
    }
    else {
      return StringUtil.format(queryTemplate, fieldName, freshness);
    }
  }

  override public function getDefaultState():Object {
    var state:Object = {};
    state[fieldName] = ANY_DATE;
    return state;
  }
}
}