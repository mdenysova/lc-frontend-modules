package com.coremedia.blueprint.studio.qcreate {
import com.coremedia.blueprint.studio.qcreate.editors.QuickCreateEditorFactory;
import com.coremedia.blueprint.studio.qcreate.processing.ProcessorFactory;

/**
 * Used to apply additional content types, properties and editors to the quick create dialog.
 */
public class QuickCreate {

  /**
   * Adds a property editor to the quick create dialog, shown if the dialog is shown for the given content type.
   * @param contentType The content type to apply the new editor for.
   * @param property The property to apply the editor value for.
   * @param editorFactory The factory method that created the editor widget of undefined if it is a standard property.
   */
  public static function addQuickCreateDialogProperty(contentType:String, property:String, editorFactory:Function = undefined):void {
    QuickCreateEditorFactory.addEditor(contentType, property, editorFactory);
  }


  /**
   * Adds a success handler for the given document type.
   * @param contentType The document type to add the handler for.
   * @param onSuccess The method to call after successful content creation.
   */
  public static function addSuccessHandler(contentType:String, onSuccess:Function):void {
    ProcessorFactory.addSuccessHandler(contentType, onSuccess);
  }


  /**
   * Hook for unprovided doctype images :(
   * @param name
   * @return
   */
  public static function convertIconClass(name:String):String {
    if(name === "CMQueryList") {
      return "CMDynamicList";
    }
    if(name === "CMSegment") {
      return "Segment";
    }
    if(name === "CMSegment") {
      return "Segment";
    }
    if(name === "CMSelectionRules") {
      return "SelectionRuleList"
    }
    return name;
  }
}
}