package com.coremedia.blueprint.studio.qcreate.editors {

import com.coremedia.blueprint.studio.Blueprint_properties;
import com.coremedia.blueprint.studio.config.components.textEditor;
import com.coremedia.blueprint.studio.dialog.editors.TextEditor;
import com.coremedia.blueprint.studio.qcreate.QuickCreateSettings_properties;
import com.coremedia.blueprint.studio.qcreate.QuickCreate_properties;
import com.coremedia.blueprint.studio.qcreate.processing.ProcessingData;
import com.coremedia.blueprint.studio.util.StudioConfigurationUtil;
import com.coremedia.blueprint.studio.util.components.FolderCombo;
import com.coremedia.blueprint.studio.util.config.folderCombo;
import com.coremedia.cap.common.CapPropertyDescriptorType;
import com.coremedia.cap.content.Content;
import com.coremedia.cms.editor.sdk.editorContext;
import com.coremedia.cms.editor.sdk.sites.Site;
import com.coremedia.cms.editor.sdk.util.PropertyEditorUtil;
import com.coremedia.ui.data.Bean;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.ValueExpressionFactory;
import com.coremedia.ui.data.beanFactory;
import com.coremedia.ui.util.EventUtil;

import ext.Component;
import ext.IEventObject;
import ext.config.observable;
import ext.util.StringUtil;

/**
 * The editor registry, defines which editors to create depending on the name of the property.
 */
public class QuickCreateEditorFactory {
  internal static const NAME:String = 'createContentNameField';
  private static var editorRegistry:Bean = beanFactory.createLocalBean();

  private static const TEXT_EDITOR_BUFFER_TIME_MILLIS:int = 10;

  /**
   * Creates a new property editor for the given property name.
   * The creation order works in the following order:
   * - find editor for custom properties
   * - find editor in editorRegistry
   * - try to create default editor depending on the type of the property.
   * @param propertyName The name of the property to create the editor for.
   * @param data The data the user input will be stored into.
   * @param callback The callback the created editor is passed to.
   * @param bindTo Used if the dialog is called from a link list
   */
  public static function createEditor(bindTo:ValueExpression, propertyName:String, data:ProcessingData, callback:Function, enterPressed:Function):void {
    var contentTypeName:String = data.getContentType().getName();

    //i18n of label and empty text
    var editorLabel:String = PropertyEditorUtil.getLocalizedString(contentTypeName, propertyName, PropertyEditorUtil.LABEL);
    var emptyTextLabel:String = PropertyEditorUtil.getLocalizedString(contentTypeName, propertyName, PropertyEditorUtil.EMPTY_TEXT);
    var properties:Object = {propertyName: propertyName, label: editorLabel, model: data, emptyText: emptyTextLabel, bindTo: bindTo};

    //some editors work site depending, so pass the VE as default
    properties.siteExpression = ValueExpressionFactory.create(ProcessingData.SITE_PROPERTY, data);

    if (propertyName == ProcessingData.NAME_PROPERTY) {
      var textEditorConfig:textEditor = textEditor(properties);
      textEditorConfig.label = QuickCreate_properties.INSTANCE.name_label;
      textEditorConfig.emptyText = QuickCreate_properties.INSTANCE.name_empty_text;
      textEditorConfig.itemId = NAME;
      textEditorConfig.validate = function ():Boolean {
        if (data.getName() && data.getName().length > 0) {
          return true;
        }
        return false;
      };
      textEditorConfig.buffer = TEXT_EDITOR_BUFFER_TIME_MILLIS;
      addEnterListener(textEditorConfig, enterPressed);
      var nameEditor:TextEditor = new TextEditor(textEditorConfig);
      callback.call(null, nameEditor, propertyName);
    }
    else if (propertyName == ProcessingData.FOLDER_PROPERTY) {
      var folderVE:ValueExpression = ValueExpressionFactory.create(propertyName, data);
      var homeFolderExpression:ValueExpression = getHomeFolderExpression(contentTypeName, bindTo);
      var folderComboConfig:folderCombo = folderCombo(properties);
      folderComboConfig.emptyText = Blueprint_properties.INSTANCE.Folder_Combo_empty_text;
      folderComboConfig.contentType = contentTypeName;
      folderComboConfig.style = 'width:348px;';
      folderComboConfig.folderPathsExpression = homeFolderExpression;
      folderComboConfig.bindTo = folderVE;
      folderComboConfig.validator = function (value:String):* {
        return !!(value && value.length > 0);
      };
      var fc:FolderCombo = new FolderCombo(folderComboConfig);
      callback.call(null, fc, propertyName);
    }
    else if (isTextProperty(data) && !isCustomEditor(contentTypeName, propertyName)) {
      var config:textEditor = textEditor(properties);
      config.buffer = TEXT_EDITOR_BUFFER_TIME_MILLIS;
      addEnterListener(config, enterPressed);
      callback.call(null, new TextEditor(config), propertyName);
    }
    else if (isCustomEditor(contentTypeName, propertyName)) {
      var factory:Function = editorRegistry.get(contentTypeName + '_' + propertyName) || editorRegistry.get(propertyName);
      var editor:Component = factory.call(null, data, properties, enterPressed) as Component;
      callback.call(null, editor, propertyName);
    }
    else {
      throw Error('No property editor found for dialog property "' + propertyName + "', " +
              "maybe the property does not exists or it's type is not supported yet.");
    }
  }

  private static function addEnterListener(config:observable, enterPressed:Function):void {
    config.listeners = {specialkey: function (component:Component, e:IEventObject):void {
      if (e.getKey() == e.ENTER) {
        e.stopEvent();
        component.el.blur();
        EventUtil.invokeLater(enterPressed);
      }
    }};
  }


  /**
   * Get the default home folder for the given content type.
   * To that end, first a site is determined: If bindTo evaluates to content, the root folder of the
   * site this content belongs to is used, otherwise, the preferred site is used.
   * If a setting bundle named "Content Creation" is available for the thusly determined site, and a setting
   * named paths.<content_type_name> is available and points to a folder, this folder is used as the default
   * root folder.
   *
   * If no such setting is found by the logic above, a bundle property named <content_type_name>_home_folder is
   * checked.
   *
   * If no such bundle property exists either, the site's root folder is taken.
   *
   * If no site could be determined (because neither bindTo evaluated to some content nor a preferred site was
   * chosen by the user), null is returned.
   *
   * @return an expression evaluating to and array of path strings, or null.
   */
  private static function getHomeFolderExpression(contentTypeName:String, bindTo:ValueExpression):ValueExpression {
    return ValueExpressionFactory.createFromFunction(function (name:String, bindTo:ValueExpression):Array {
      var site:Site;

      if (bindTo) {
        var content:Content = bindTo.getValue() as Content;
        if (content) {
          site = editorContext.getSitesService().getSiteFor(content);
        }
      }

      if (!site) {
        site = editorContext.getSitesService().getPreferredSite();
      }

      var folder:Content = StudioConfigurationUtil.getConfiguration("Content Creation", StringUtil.format("paths.{0}", contentTypeName));
      folder && folder.load();
      if (folder && folder.isLoaded()) {

        return [folder.getPath()];
      }

      var docTypeDefault:String = QuickCreateSettings_properties.INSTANCE[contentTypeName + '_home_folder'];
      if (docTypeDefault) {
        if (docTypeDefault.indexOf('/') === 0) {
          return [docTypeDefault];
        }
        if (site) {
          return [site.getSiteRootFolder().getPath() + '/' + docTypeDefault];
        }
      }

      if (site) {
        return [site.getSiteRootFolder().getPath()];
      }

      return null;

    }, contentTypeName, bindTo);
  }

  /**
   * Checks if a custom editor is defined for the given name and the given content type.
   * @param contentType
   * @param propertyName
   * @return
   */
  private static function isCustomEditor(contentType:String, propertyName:String):Boolean {
    var key:String = contentType + '_' + propertyName;
    if (editorRegistry.get(key) || editorRegistry.get(propertyName)) {
      return true;
    }
    return false;
  }

  /**
   * Checks if the given property name is a text property.
   * @param data The current user input.
   * @return
   */
  private static function isTextProperty(data:ProcessingData):Boolean {
    var descriptors:Array = data.getContentType().getDescriptors();
    for (var i:int = 0; i < descriptors.length; i++) {
      var desc:Object = descriptors[i];
      if (desc.type === CapPropertyDescriptorType.STRING) {
        return true;
      }
    }
    return false;
  }

  /**
   * Adds an editor factory call method to the editor factory.
   * @param contentType The name of the content type the editor property is for.
   * @param property The name of the property to create the editor for.
   * @param factoryMethod The method that returns a property editor.
   */
  public static function addEditor(contentType:String, property:String, factoryMethod:Function):void {
    if (!QuickCreateSettings_properties.INSTANCE['item_' + contentType]) {
      QuickCreateSettings_properties.INSTANCE['item_' + contentType] = property;
    }
    else {
      QuickCreateSettings_properties.INSTANCE['item_' + contentType] = QuickCreateSettings_properties.INSTANCE['item_' + contentType] + ',' + property;
    }

    property = contentType + '_' + property;
    editorRegistry.set(property, factoryMethod);
  }
}
}
