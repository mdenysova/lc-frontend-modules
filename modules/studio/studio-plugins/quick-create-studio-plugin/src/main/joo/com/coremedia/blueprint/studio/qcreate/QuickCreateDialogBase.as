package com.coremedia.blueprint.studio.qcreate {

import com.coremedia.blueprint.studio.config.quickcreate.quickCreateDialog;
import com.coremedia.blueprint.studio.qcreate.editors.QuickCreateEditorFactory;
import com.coremedia.blueprint.studio.qcreate.processing.ProcessingData;
import com.coremedia.blueprint.studio.qcreate.processing.ProcessorFactory;
import com.coremedia.blueprint.studio.util.ContentUtil;
import com.coremedia.blueprint.studio.util.StudioUtil;
import com.coremedia.cap.content.Content;
import com.coremedia.cms.editor.sdk.editorContext;
import com.coremedia.cms.editor.sdk.util.ContentLocalizationUtil;
import com.coremedia.cms.editor.sdk.util.LinkListUtil;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.ValueExpressionFactory;
import com.coremedia.ui.data.beanFactory;
import com.coremedia.ui.util.ArrayUtils;

import ext.Component;
import ext.Panel;
import ext.QuickTips;
import ext.Window;
import ext.config.quicktip;
import ext.form.TextField;
import ext.util.StringUtil;

/**
 * The base class of the quick create dialog creates all widgets for the
 * dialog, depending on the selected content type.
 */
public class QuickCreateDialogBase extends Window {

  private var disabledExpression:ValueExpression;
  private var model:ProcessingData;
  private var contentType:String;
  private var bindTo:ValueExpression;
  private var propertyName:String;
  private var linkType:String;
  private var defaultNameExpression:ValueExpression;
  private var inheritEditors:Boolean;
  private var defaultProperties:String;
  private var appendResultAtEnd:Boolean;

  private var editorRegistry:Array = [];

  public function QuickCreateDialogBase(config:quickCreateDialog) {
    super(config);
    appendResultAtEnd = config.appendResultAtEnd || false;
    contentType = config.contentType || config.contentTypeExpression.getValue();
    defaultProperties = config.defaultProperties;
    if (defaultProperties === undefined) {
      defaultProperties = (ProcessingData.NAME_PROPERTY + "," + ProcessingData.FOLDER_PROPERTY);
    }
    defaultNameExpression = config.defaultNameExpression;
    propertyName = config.propertyName;
    bindTo = config.bindTo;
    linkType = config.linkType;
    model = config.model;
    inheritEditors = config.inheritEditors;

    if (inheritEditors === undefined) {
      inheritEditors = true;
    }

    if (!model) {
      model = new ProcessingData();
    }
    model.addValueChangeListener(modelChangeListener);

    model.setContentType(contentType);
    model.set(ProcessingData.ON_SUCCESS, config.onSuccess);
    if (config.skipInitializers && config.skipInitializers == true) {
      model.set(ProcessingData.SKIP_INITIALIZERS, config.skipInitializers);
    }

    on("beforedestroy", cleanup);
  }

  /**
   * Init dialog
   */
  override protected function afterRender():void {
    super.afterRender();

    /**
     * Add content type specific property editor.
     */
    var propertiesString:String = defaultProperties;
    var additionalProperties:String = resolvePropertiesForContentType(inheritEditors);
    if (additionalProperties) {
      if(propertiesString.length > 0) {
        propertiesString = propertiesString + "," + additionalProperties;
      }
      else {
        propertiesString = additionalProperties;
      }
    }
    var properties:Array = propertiesString.split(',');

    var editContainer:Panel = find('itemId', 'editorContainer')[0];
    for (var i:int = 0; i < properties.length; i++) {
      QuickCreateEditorFactory.createEditor(bindTo, properties[i], model, function (editor:Component, propertyName:String = null):void {
        editorRegistry.push(editor);
        if (editor) {
          editContainer.add(editor);
          editContainer.doLayout(false, true);

          if(propertyName) {
            var nameField:TextField = find('name', propertyName)[0];
            if (nameField) {
              nameField.focus(false, 500);
              if (defaultNameExpression && defaultNameExpression.getValue()) {
                nameField.setValue(defaultNameExpression.getValue());
                model.setName(defaultNameExpression.getValue());
              }
              nameField.selectText(0, nameField.getValue().length);
            }
          }
        }
      }, enterPressed);
    }
    doLayout(false, true);
    modelChangeListener();
  }

  /**
   * Recursive resolving of content properties so that super types
   * are also allowed for property definitions.
   * @return
   */
  private function resolvePropertiesForContentType(recursively:Boolean = true):String {
    var ctTypes:Array = [contentType];
    if (recursively) {
      ctTypes = ContentUtil.getContentTypeHierarchy(contentType);
    }
    var properties:Array = [];
    for (var i:int = 0; i < ctTypes.length; i++) {
      var prop:String = QuickCreateSettings_properties.INSTANCE['item_' + ctTypes[i]];
      if (prop && properties.indexOf(prop) === -1) {
        properties.push(prop);
      }
    }

    if (properties.length > 0) {
      return properties.join(',');
    }
    return undefined;
  }

  public function enterPressed():void {
    if (!getDisabledExpression().getValue()) {
      handleSubmit();
    }
  }

  /**
   * Invokes the post processing and closes the dialog
   */
  protected function handleSubmit():void {
    destroy();
    ProcessorFactory.process(model, function (data:ProcessingData):void {
      var c:Content = data.getContent();

      //apply to a link list if the property name is defined.
      if (bindTo && propertyName) {
        var sourceLinklist:ValueExpression = LinkListUtil.createLinkValueExpression(bindTo, propertyName, linkType, false);
        var oldValue:Array = ArrayUtils.asArray(sourceLinklist.getValue());
        if (!ContentUtil.isInArray(oldValue, c)) {
          if(appendResultAtEnd) {
            sourceLinklist.setValue(oldValue.concat(c));
          }
          else {
            var newValue:Array = [];
            newValue.push(c);
            sourceLinklist.setValue(newValue.concat(oldValue));
          }
        }
      }

      //maybe there has been server side processing, so invalidate the content first
      c.invalidate(function ():void {
        var initializer:Function = editorContext.lookupContentInitializer(c.getType());
        if (initializer) {
          initializer(c);
        }
        StudioUtil.openInTab(c);
        openAdditionalContent(data);
      });
    });
  }

  private function openAdditionalContent(data:ProcessingData):void {
    var additionalContent:Array = data.getAdditionalContent();
    var reloadCount:int = additionalContent.length;
    for (var i:int = 0; i < additionalContent.length; i++) {
      var additional:Content = additionalContent[i];
      //the same here: there may have been server side post processing, so invalidate the content.
      additional.invalidate(function ():void {
        reloadCount--;
        if (reloadCount === 0) {
          StudioUtil.openInBackground(additionalContent);
        }
      });
    }
  }

  private function modelChangeListener():void {
    getDisabledExpression().setValue(false);
    for (var i:int = 0; i < editorRegistry.length; i++) {
      var editor:Component = editorRegistry[i];
      var validatorFunction:Function = editor.initialConfig['validate'];
      if (validatorFunction) {
        var result:Boolean = validatorFunction.call(null);
        if (!result) {
          getDisabledExpression().setValue(true);
          editor.addClass("issue-error");
          QuickTips.register(quicktip({
            target:editor.getId(),
            id:'quick-create-error-tt',
            text:QuickCreate_properties.INSTANCE.quick_create_missing_value,
            trackMouse:false,
            autoHide:true,
            dismissDelay:3000
          }));
        }
        else {
          editor.removeClass("issue-error");
          QuickTips.unregister(editor.el);
          QuickTips.getQuickTip().hide();
        }
      }
    }
  }

  private function cleanup():void {
    model.removeValueChangeListener(modelChangeListener);
  }

  /**
   * Formats the title for the dialog depending on the content type.
   * @param contentType The content type name.
   * @return
   */
  protected function getTitle(contentType:String, contentTypeExpression:ValueExpression):String {
    var contentName:String = ContentLocalizationUtil.localizeDocumentTypeName(contentType || contentTypeExpression.getValue());
    return StringUtil.format(QuickCreate_properties.INSTANCE.dialog_title, contentName);
  }

  /**
   * Calculates if the mandatory input is given.
   * @return
   */
  protected function getDisabledExpression():ValueExpression {
    if (!disabledExpression) {
      disabledExpression = ValueExpressionFactory.create('disabled', beanFactory.createLocalBean());
      disabledExpression.setValue(true);
    }
    return disabledExpression;
  }
}
}
