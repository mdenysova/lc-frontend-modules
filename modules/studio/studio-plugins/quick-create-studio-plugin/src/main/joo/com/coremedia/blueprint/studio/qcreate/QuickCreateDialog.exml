<?xml version="1.0" encoding="ISO-8859-1"?>
<exml:component xmlns:exml="http://www.jangaroo.net/exml/0.8"
                xmlns="exml:ext.config"
                xmlns:u="exml:untyped"
                xmlns:ui="exml:com.coremedia.ui.config"
                xmlns:editor="exml:com.coremedia.cms.editor.sdk.config"
                baseClass="com.coremedia.blueprint.studio.qcreate.QuickCreateDialogBase">

  <exml:import class="ext.MessageBox"/>
  <exml:import class="ext.util.StringUtil"/>
  <exml:import class="com.coremedia.ui.data.ValueExpressionFactory"/>
  <exml:import class="com.coremedia.cms.editor.sdk.util.ContentLocalizationUtil"/>
  <exml:import class="com.coremedia.cms.editor.sdk.config.propertyFieldGroup"/>
  <exml:import class="com.coremedia.blueprint.studio.qcreate.QuickCreate_properties"/>
  <exml:import class="com.coremedia.blueprint.studio.qcreate.processing.ProcessingData"/>
  <exml:import class="com.coremedia.blueprint.studio.util.StudioPluginUtils_properties"/>

  <exml:cfg name="contentType" type="String">
    <exml:description>The content type of the content to create.</exml:description>
  </exml:cfg>
  <exml:cfg name="contentTypeExpression" type="ValueExpression">
    <exml:description>The content type of the newly created content, optional if the content type name is set.</exml:description>
  </exml:cfg>
  <exml:cfg name="inheritEditors" type="Boolean">
    <exml:description>
      Optional: if set to false the property editors will not be inherited from the super document types.
      Default is true, i.e. the property editors are inherited.
    </exml:description>
  </exml:cfg>
  <exml:cfg name="propertyName" type="String">
    <exml:description>The content property name of the list to bind the newly created content to.</exml:description>
  </exml:cfg>
  <exml:cfg name="bindTo" type="com.coremedia.ui.data.ValueExpression">
    <exml:description>Contains the active content.</exml:description>
  </exml:cfg>
  <exml:cfg name="linkType" type="String">
    <exml:description>
       The allowed type of links is usually derived from the link property descriptor found through bindTo and propertyName,
       but to override this or provide an initial value for link properties in Structs that are created by this
       property field, you may specify a custom link type.
    </exml:description>
  </exml:cfg>
  <exml:cfg name="model" type="ProcessingData">
    <exml:description>
      Optional model for which the dialog editors wil be created.
      If not specified a model will be created internally.
    </exml:description>
  </exml:cfg>
  <exml:cfg name="skipInitializers" type="Boolean">
    <exml:description>If true the default initializers are skipped after content creation. Default is false.</exml:description>
  </exml:cfg>
  <exml:cfg name="defaultNameExpression" type="com.coremedia.ui.data.ValueExpression">
    <exml:description>The value expression contains default name that should be used for the dialog.
     If undefined, the default new document name will be used.
    </exml:description>
  </exml:cfg>
  <exml:cfg name="onSuccess" type="Function">
    <exml:description>An optional callback that the Dialog will invoke when content creation was successfully completed.
     Signature: function(createdContent:Content, data:ProcessingData, callback:Function):void
    </exml:description>
  </exml:cfg>
  <exml:cfg name="defaultProperties" type="String">
    <exml:description>Default properties to be used, allows to overwrite the name and folder properties (CSV format).</exml:description>
  </exml:cfg>
  <exml:cfg name="appendResultAtEnd" type="Boolean">
    <exml:description>If true, the newly created content will be appended to the end of the given linklist.
     By default, this option is set to 'false' so that new content items will be inserted at the beginning.</exml:description>
  </exml:cfg>

  <window title="{getTitle(config.contentType, config.contentTypeExpression)}"
          width="430"
          minWidth="430"
          modal="false"
          collapsible="false"
          cls="modal-window-base"
          constrainHeader="true"
          x="113"
          y="84"
          autoHeight="true"
          resizable="true"
          resizeHandles="e"
          draggable="true"
          layout="fit">
    <items>
      <editor:documentForm autoHeight="true"
                           autoWidth="true">
        <items>
          <panel u:labelAlign="top"
                 autoWidth="true"
                 cls="quickcreate-form-panel"
                 itemId="editorContainer"
                 ctCls="default-margin">
            <items>
            </items>
            <layout>
              <formlayout labelSeparator=""/>
            </layout>
          </panel>
        </items>
      </editor:documentForm>
    </items>
    <buttons>
      <ui:footerButton asSuggestion="true"
                       text="{StudioPluginUtils_properties.INSTANCE.CreateContent_createButton_label}"
                       itemId="createBtn"
                       handler="{handleSubmit}">
        <plugins>
          <ui:bindPropertyPlugin componentProperty="disabled"
                                 bindTo="{getDisabledExpression()}"/>
        </plugins>
      </ui:footerButton>
      <ui:footerButton text="{StudioPluginUtils_properties.INSTANCE.btn_cancel}"
                       itemId="cancelBtn"
                       handler="{close}"/>
    </buttons>
  </window>
</exml:component>
