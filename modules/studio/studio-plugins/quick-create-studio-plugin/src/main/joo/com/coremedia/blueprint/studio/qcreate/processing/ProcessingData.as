package com.coremedia.blueprint.studio.qcreate.processing {
import com.coremedia.blueprint.studio.util.ContentUtil;
import com.coremedia.cap.common.session;
import com.coremedia.cap.content.Content;
import com.coremedia.cap.content.ContentType;
import com.coremedia.ui.data.impl.BeanImpl;

/**
 * Data wrapper that contains all user input of the dialog.
 */
public class ProcessingData extends BeanImpl {
  //mandatory dialog properties
  public static const FOLDER_PROPERTY:String = "folder";
  public static const NAME_PROPERTY:String = "name";

  //dialog properties
  public static const SKIP_INITIALIZERS:String = "skipInitializers";
  public static const ON_SUCCESS:String = "onSuccess";

  //only used for page grid
  public static const SITE_PROPERTY:String = "site";

  private var contentType:String;
  private var content:Content;

  //property is used to remember all documents that have been touched.
  private var additionalContent:Array = [];

  public function ProcessingData() {
  }

  public function getOnSuccessCall():Function {
    return get(ON_SUCCESS);
  }

  public function doSkipInitializers():Boolean {
    return get(SKIP_INITIALIZERS);
  }

  public function addAdditionalContent(c:Content):void {
    if(!ContentUtil.isInArray(additionalContent, c)) {
      additionalContent.push(c);
    }
  }

  public function getAdditionalContent():Array {
    return additionalContent;
  }

  public function setContentType(contentType:String):void {
    this.contentType = contentType;
  }

  /**
   * Returns the folder the content has been created into.
   * @return
   */
  public function getFolder():Content {
    return get(FOLDER_PROPERTY);
  }

  public function getName():String {
    return get(NAME_PROPERTY);
  }

  public function setName(value:String):void {
    set(NAME_PROPERTY, value)
  }

  public function getContentType():ContentType {
    return session.getConnection().getContentRepository().getContentType(contentType);
  }

  public function setContent(c:Content):void {
    this.content = c;
  }

  public function getContent():Content {
    return content;
  }


  override public function toString():String {
    var value:String = 'Processing Data: ' + content + ', skipInitializers:' + doSkipInitializers()
            + ', additionalContent:' + additionalContent.length;
    return value;
  }
}
}