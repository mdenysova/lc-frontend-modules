package com.coremedia.blueprint.studio.qcreate {

import com.coremedia.blueprint.studio.config.components.newContentMenuButton;
import com.coremedia.blueprint.studio.config.quickcreate.quickCreateMenuItem;
import com.coremedia.cms.editor.sdk.editorContext;

import ext.Ajax;
import ext.Container;
import ext.menu.Item;

public class QuickCreateMenuItemBase extends Item {

  public function QuickCreateMenuItemBase(config:quickCreateMenuItem) {
    super(config);
  }

  /**
   * Apply the favorite icon class if the menu item
   * is added to the favourites toolbar.
   */
  override protected function afterRender():void {
    super.afterRender();
    var parent:Container = findParentByType(newContentMenuButton.xtype);
    if(parent) {
      addClass('fav-menu-item');
    }
  }

}
}
