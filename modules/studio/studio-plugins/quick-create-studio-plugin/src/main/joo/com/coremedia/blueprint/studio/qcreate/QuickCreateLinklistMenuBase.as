package com.coremedia.blueprint.studio.qcreate {

import com.coremedia.blueprint.studio.config.quickcreate.quickCreateLinklistMenu;
import com.coremedia.blueprint.studio.config.quickcreate.quickCreateMenuItem;
import com.coremedia.ui.components.IconButton;
import com.coremedia.ui.data.ValueExpression;

/**
 * The case class of the quick create menu.
 * The menu is build dynamically with the items passed with the 'contentTypes' property.
 */
public class QuickCreateLinklistMenuBase extends IconButton {

  private var contentTypes:Array;
  private var bindTo:ValueExpression;
  private var propertyName:String;
  private var linkType:String;
  private var onSuccess:Function;

  public function QuickCreateLinklistMenuBase(config:quickCreateLinklistMenu) {
    super(config);

    var contentTypesString:String = config.contentTypes || QuickCreateSettings_properties.INSTANCE.default_link_list_contentTypes;
    contentTypes = contentTypesString.split(',');
    bindTo = config.bindTo;
    propertyName = config.propertyName;
    linkType = config.linkType;
    onSuccess = config.onSuccess;
  }

  /**
   * Dynamically adds the quick create items to the menu.
   */
  override protected function afterRender():void {
    super.afterRender();
    for(var i:int = 0 ; i<contentTypes.length; i++) {
      var name:String = contentTypes[i];
      var item:QuickCreateMenuItem = new QuickCreateMenuItem(quickCreateMenuItem({contentType:name,
        bindTo:bindTo,
        propertyName:propertyName,
        linkType:linkType,
        onSuccess:onSuccess}));
      this.menu.add(item);
    }
    this.menu.doLayout(false, true);
  }
}
}
