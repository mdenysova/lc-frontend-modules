package com.coremedia.blueprint.studio.qcreate.processing {
import com.coremedia.blueprint.studio.qcreate.QuickCreateSettings_properties;
import com.coremedia.blueprint.studio.util.AjaxUtil;
import com.coremedia.blueprint.studio.util.ContentCreationUtil;
import com.coremedia.blueprint.studio.util.ContentUtil;
import com.coremedia.blueprint.studio.util.contentcreation.FolderCreationResult;
import com.coremedia.cap.content.Content;
import com.coremedia.ui.data.Bean;
import com.coremedia.ui.data.beanFactory;

/**
 * Factory for executing the post processing, depending on the given document type the
 * quick create dialog has been initialized with.
 */
public class ProcessorFactory {
  public static var onSuccessHandlers:Bean = beanFactory.createLocalBean();

  /**
   * Creates the content and invoked the post processor (if available).
   * @param data The user input.
   * @param callback The callback to call when processing is finished.
   */
  public static function process(data:ProcessingData, callback:Function):void {
    resolvePath(data, function(path:String):void {
    //first ensure that all folders exist
    ContentCreationUtil.createRequiredSubfolders(path, function (result:FolderCreationResult):void {
      if (result.success) {
          trace('[INFO]', 'Quick create finished creation of path ' + path);

        //apply the folder instance to the processing data
        data.set(ProcessingData.FOLDER_PROPERTY, result.baseFolder);

        //and create the content itself.
        ContentCreationUtil.createContent(data.getFolder(), false, data.doSkipInitializers(), data.getName(), data.getContentType(), function (content:Content):void {
          content.load(function ():void {
            //apply the created content to the processing data for post processing
            data.setContent(content);

            //sets the default values like string properties
            executeDefaultProcessing(data);

            //execute general custom handlers
            executeCustomPostProcessing(data, function ():void {
              //execute one time success handlers
              executeOnSuccessCall(data, function ():void {
                trace('[INFO]', 'Quick create post processing finished, invoking callback with data: ' + data);
                callback.call(null, data);
              });
            });
          });
        }, AjaxUtil.onError);
      }
    }, true);
    });


  }

  /**
   * Checks the folder property of the processing data.
   * Instead of a fix path a function can be passed that resolved the folder
   * depending on the given processing data.
   * @param data the current quick create data
   * @param callback the callback where the path value is passed.
   */
  private static function resolvePath(data:ProcessingData, callback:Function):void {
    trace('[INFO]', 'Quick create is resolving target path...');
    var path:String = data.get(ProcessingData.FOLDER_PROPERTY) as String;
    if(path) {
      callback.call(null, path);
    }
    else {
      var pathFunction:Function = data.get(ProcessingData.FOLDER_PROPERTY) as Function;
      pathFunction.call(null, data, callback);
    }
  }

  /**
   * Executes the success handler that has been declared for the content type.
   * These type of success handlers are executed for every content creation.
   * @param data
   * @param callback
   */
  private static function executeCustomPostProcessing(data:ProcessingData, callback:Function):void {
    var handlers:Array = onSuccessHandlers.get(data.getContentType().getName());
    if(handlers && handlers.length > 0) {
      var count:int = handlers.length;
      trace('[INFO]', 'Executing ' + count + ' success handler(s) for content type ' + data.getContentType().getName());
      executeProcessorsSequential(0, data, handlers, callback);
    }
    else {
      callback.call(null);
    }
  }

  private static function executeProcessorsSequential(count:int, data:ProcessingData, handlers:Array, callback:Function):void {
    if(count < handlers.length) {
      trace('INFO', "Finished sequential execution of post processor on position " + (count+1));
      executeSuccessHandler(handlers[count], data, function():void {
        count++;
        executeProcessorsSequential(count, data, handlers, callback);
      });
    }
    else {
      callback.call(null);
    }
  }

  /**
   * Executes the success handler declared for this dialog only. The handler has been passed
   * to the processing data, so it is only executed once.
   * @param data
   * @param callback
   */
  private static function executeOnSuccessCall(data:ProcessingData, callback:Function):void {
    var handler:Function = data.getOnSuccessCall();
    trace('[INFO]', 'Executing success handler');
    executeSuccessHandler(handler, data, callback);
  }

  /**
   * Executes the given post processing handler.
   * @param handler
   * @param data
   * @param callback
   */
  private static function executeSuccessHandler(handler:Function, data:ProcessingData, callback:Function):void {
    if (handler) {
      handler.call(null, data.getContent(), data, function ():void {
        callback.call(null);
      });
    }
    else {
      callback.call(null);
    }
  }

  /**
   * Executes the default processing the standard properties of the content.
   * @param data The data instance that contains all user input.
   */
  private static function executeDefaultProcessing(data:ProcessingData):void {
    var content:Content = data.getContent();
    var properties:String = QuickCreateSettings_properties.INSTANCE['item_' + content.getType().getName()];
    if (properties) { //ok, there are custom properties defined for the content type
      //create property-names array
      var propertiesArray:Array = properties.split(",");
      for (var i:int = 0; i < propertiesArray.length; i++) {
        var propertyName:String = propertiesArray[i];
        if (ContentUtil.hasProperty(data.getContent(), propertyName)) {
          var value:* = data.get(propertyName);
          if (value) {
            content.getProperties().set(propertyName, value);
          }
        }
        else {
          trace('[INFO]', 'Skipped processing of "' + propertyName + '": not a content property.');
        }
      }
    }
  }

  /**
   * Adds a success handler for the given content type, that is executed after the default processing.
   * @param contentType The content type to apply the success handler for.
   * @param onSuccess The method to call when the content has been created successfully.
   */
  public static function addSuccessHandler(contentType:String, onSuccess:Function):void {
    var handlers:Array = onSuccessHandlers.get(contentType) || [];
    handlers.push(onSuccess);
    onSuccessHandlers.set(contentType, handlers);
  }
}
}
