joo.loadModule("${project.groupId}", "${project.artifactId}");
joo.loadStyleSheet('joo/resources/css/preferences-ui.css');
coremediaEditorPlugins.push({
  name:"Preferences",
  mainClass:"com.coremedia.blueprint.studio.PreferencesStudioPlugin"
});

