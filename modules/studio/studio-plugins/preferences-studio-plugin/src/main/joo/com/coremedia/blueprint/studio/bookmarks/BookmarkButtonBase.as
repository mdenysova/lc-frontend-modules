package com.coremedia.blueprint.studio.bookmarks {

import com.coremedia.blueprint.studio.PreferencesStudioPlugin_properties;
import com.coremedia.blueprint.studio.config.preferences.bookmarkButton;
import com.coremedia.blueprint.studio.util.ToggleButtonUtil;
import com.coremedia.ui.components.IconButtonMedium;

public class BookmarkButtonBase extends IconButtonMedium {

  public function BookmarkButtonBase(config:bookmarkButton = null) {
    super(config);
  }

  internal function bookmarkToggleHandler(bookmarked:Boolean):void {
    if (btnEl) {
      if (bookmarked) {
        ToggleButtonUtil.setPressed(this);
        setTooltip(PreferencesStudioPlugin_properties.INSTANCE.Bookmark_button_remove_tooltip);
      } else {
        ToggleButtonUtil.setUnpressed(this);
        setTooltip(PreferencesStudioPlugin_properties.INSTANCE.Bookmark_button_add_tooltip);
      }
    }
  }
}
}
