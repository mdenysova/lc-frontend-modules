package com.coremedia.blueprint.studio.bookmarks {
import com.coremedia.blueprint.studio.PreferencesStudioPlugin_properties;
import com.coremedia.blueprint.studio.config.preferences.openBookmarkAction;
import com.coremedia.cap.content.Content;
import com.coremedia.cms.editor.sdk.collectionview.CollectionViewConstants;
import com.coremedia.cms.editor.sdk.editorContext;

import ext.Action;

/**
 * Action for opening a bookmark.
 */
public class OpenBookmarkAction extends Action {
  private var content:Content;

  /**
   * @param config
   */
  public function OpenBookmarkAction(config:openBookmarkAction) {
    config['handler'] = openBookmark;
    this.content = config.content;
    if (!content) {
      config['text'] = PreferencesStudioPlugin_properties.INSTANCE.Bookmark_empty_list;
    }
    else {
      config['text'] = config.name;
    }

    super(config);
    if (!content) {
      this.setDisabled(true);
    }
  }

  /**
   * The action handle for this action.
   */
  private function openBookmark():void {
    if (content) {
      if (content.isDocument()) {
        editorContext.getContentTabManager().openDocument(content);
      } else {
        editorContext.getCollectionViewManager().showInRepository(content, CollectionViewConstants.LIST_VIEW);
      }

    }
  }
}
}