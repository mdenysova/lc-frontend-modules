package com.coremedia.blueprint.studio.bookmarks {
import com.coremedia.cap.content.Content;
import com.coremedia.cms.editor.sdk.util.PreferencesUtil;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.ValueExpressionFactory;
import com.coremedia.ui.data.beanFactory;

import ext.Ext;

use namespace Ext;


/**
 * Management singleton to handle bookmark related actions.
 */
public class BookmarkManager {

  private static const PREFERENCE_BOOKMARKS:String = "bookmarks";
  private static var instance:BookmarkManager;
  private var bookmarkedContentExpression:ValueExpression;
  private var availableBookmarksExpression:ValueExpression;

  public static function getInstance():BookmarkManager {
    if (!instance) {
      instance = new BookmarkManager();
      instance.loadBookmarks();
    }
    return instance;
  }

  public function getBookmarkedContentExpression():ValueExpression {
    if (!bookmarkedContentExpression) {
      bookmarkedContentExpression = ValueExpressionFactory.create('bookmarks', beanFactory.createLocalBean());
    }
    return bookmarkedContentExpression;
  }

  public function getAvailableBookmarksExpression():ValueExpression {
    if (!availableBookmarksExpression) {
      availableBookmarksExpression = ValueExpressionFactory.createFromFunction(function ():Array {
        var result:Array = [];
        var bmContent:Array = getBookmarkedContentExpression().getValue();
        if (bmContent) {
          Ext.each(bmContent, function (content:Content):void {
            try {
              var name:String = content.getName();
              if (name && !(content.isDeleted() || content.isDestroyed())) {
                result.push(new Bookmark(name, content));
              }
            }
            catch(e:Error) {
              trace('ERROR', 'Unreadable bookmark: ' + content);
            }
          });
        }
        return result;
      });
      availableBookmarksExpression.addChangeListener(saveBookmarks);
    }
    return availableBookmarksExpression;
  }


  /**
   * Applies the content of the active tab as bookmark
   * to the bookmark list, if not already in it.
   */
  public function applyBookmark(name:String, content:Content):void {
    if (content) {
      var oldValue:Array = getBookmarkedContentExpression().getValue();
      if (oldValue) {
        if (!oldValue.some(function (aContent:Content):Boolean {
          return content === aContent
        })) {
          getBookmarkedContentExpression().setValue(oldValue.concat(content));
        }
      } else {
        getBookmarkedContentExpression().setValue([content]);
      }
    }
  }

  /**
   * Inserts some content into the list of bookmarks at the specified position, or moves it
   * to this position of the bookmarks if it is already bookmarked
   * @param index the position to insert the content at / move the content to
   * @param content the content
   * @return true if the action succeeded, false otherwise (on wrong index or undefined content, for example)
   */
  public function insertAtOrMoveTo(index:int, content:Content):Boolean {
    if (index < 0 || index > (getBookmarkedContentExpression().getValue() as Array).length + 1) {
      return false; //wrong index, do nothing
    }
    if (!content || !content is Content) {
      return false; // content undefined or not actually pointing to content, do nothing;
    }

    var oldValue:Array = getBookmarkedContentExpression().getValue();
    var newValue:Array = [];
    for (var i:int = 0; i < oldValue.length; i++) {
      if (index === i) {
        newValue.push(content);
      }
      if (!(content.getId() === (oldValue[i] as Content).getId())) {
        newValue.push(oldValue[i]);
      }
    }
    if (index === oldValue.length) {
      newValue.push(content); // special case: content was moved to end of list
    }
    getBookmarkedContentExpression().setValue(newValue);
    return true;
  }

  /**
   * Removes the given content from the list of bookmarks.
   * @param content
   */
  public function removeBookmark(content:Content):void {
    var oldValue:Array = getBookmarkedContentExpression().getValue();
    var newValue:Array = [];
    for (var i:int = 0; i < oldValue.length; i++) {
      if (!(content.getUriPath() === (oldValue[i] as Content).getUriPath())) {
        newValue.push(oldValue[i]);
      }
    }
    getBookmarkedContentExpression().setValue(newValue);
  }

  private function saveBookmarks():void {
    PreferencesUtil.updatePreferencesJSONProperty(getBookmarkedContentExpression().getValue(), BookmarkManager.PREFERENCE_BOOKMARKS);
  }

  /**
   * Loads the bookmarks from the users preferences document,
   * refreshes the bookmark menu afterwards.
   */
  private function loadBookmarks():void {
    var bookmarks:Object = PreferencesUtil.getPreferencesJSONProperty(decodeBookmarks, BookmarkManager.PREFERENCE_BOOKMARKS) || [];
    getBookmarkedContentExpression().setValue(bookmarks);
  }

  /**
   * Old versions of the bookmark manager used a string format for encoding bookmarks.
   * The set of bookmark used to be a space-separated string, where each part of the string
   * consisted of a numeric id of the content to bookmark, a separator dot,
   * and an ignored bookmark name in which spaces were escaped as a tilde and an underscore.
   *
   * @param value the stored value
   * @return an array of content if the given value was a string
   */
  private function decodeBookmarks(value:Object):Array {
    var valueString:String = value as String;
    if (!valueString) {
      // Only decode strings.
      return undefined;
    }
    var result:Array = [];
    valueString.split(' ').forEach(function (item:String):void {
      var contentId:String = item.substr(0, item.indexOf("."));
      if (contentId && contentId.length > 0) {
        var bookmarkContent:Content = beanFactory.getRemoteBean('content/' + contentId) as Content;
        if (bookmarkContent) {
          result.push(bookmarkContent);
        }
      }
    });
    return result;
  }

}
}