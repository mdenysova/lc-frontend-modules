package com.coremedia.blueprint.studio.bookmarks {

import com.coremedia.blueprint.studio.config.preferences.bookmarkFavouritesToolbarButton;
import com.coremedia.blueprint.studio.config.preferences.openBookmarkAction;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.ValueExpressionFactory;
import com.coremedia.ui.data.beanFactory;

import ext.Button;
import ext.Element;
import ext.IEventObject;
import ext.config.droptarget;
import ext.config.menuitem;
import ext.dd.DragSource;
import ext.dd.DropTarget;
import ext.menu.Item;
import ext.menu.Menu;

public class BookmarkFavouritesToolbarButtonBase extends Button {
  private var dropTarget:DropTarget;
  private var currentEl:Element;
  private var dropRowValueExpression:ValueExpression;

  public function BookmarkFavouritesToolbarButtonBase(config:bookmarkFavouritesToolbarButton = null) {
    super(config);
    addListener('render', initDropTarget);
  }

  protected function addDummyMenuItemWhenEmpty(menu:Menu, bookmarks:ValueExpression):void {
    if (!bookmarks.getValue() || (bookmarks.getValue() as Array).length === 0) {
      var menuItem:menuitem = menuitem({xtype:"menuitem", cls:'fav-menu-item',
        baseAction:new OpenBookmarkAction(openBookmarkAction({}))});
      menu.addMenuItem(menuItem);
    }
  }

  private function dropRowChanged(source:ValueExpression):void {
    if (currentEl) {
      currentEl.removeClass('row-insert-above');
    }
    var bookmark:Item = menu.items.get(source.getValue()) as Item;
    currentEl = bookmark ? bookmark.getEl().parent('li') : undefined;
    if (currentEl) {
      currentEl.addClass('row-insert-above');
    }
  }

  public function getDroppingRowValueExpression():ValueExpression {
    if (!dropRowValueExpression) {
      dropRowValueExpression = ValueExpressionFactory.create('row', beanFactory.createLocalBean());
    }
    return dropRowValueExpression;
  }

  private function initDropTarget():void {
    getDroppingRowValueExpression().addChangeListener(dropRowChanged);
    var dropTargetConfig:droptarget =
              new droptarget({
                ddGroup:'ContentLinkDD',
                gridDropTarget:this,
                notifyDrop:notifyDrop,
                notifyOver:notifyOver,
                notifyEnter:notifyEnter,
                notifyOut:notifyOut
    });
    menu.on('afterrender', function ():void {
      dropTarget = new DropTarget(menu.getEl(),
              dropTargetConfig);
    });
  }

  private function notifyDrop(d:DragSource, e:IEventObject, data:Object):Boolean {
    var row:int = getDroppingRowValueExpression().getValue();
    return BookmarkManager.getInstance().insertAtOrMoveTo(row, data.contents[0]);
  }

  private function notifyOver(d:DragSource, e:IEventObject, data:Object):String {
    var rowHeight:Number = menu.getHeight()/ menu.items.getCount();
    /* get the current position of the mouse pointer wrt to the menu entries:
       distance of the mouse pointer from the top of the menu, divided by the height of one menu entry */
    var eventPos:Number = Math.round((e.getPageY() - menu.getEl().getY()) / rowHeight);
    /* use an expression for this: notifyOver is called very often, but the change event will then only
       fire if the new value is actually different from the old one */
    getDroppingRowValueExpression().setValue(eventPos);
    return dropTarget.dropAllowed;
  }

  private function notifyEnter(d:DragSource, e:IEventObject, data:Object):void {
    notifyOver(d, e, data);
  }

  private function notifyOut(d:DragSource, e:IEventObject, data:Object):void {
    if (currentEl) {
      currentEl.removeClass('row-insert-above');
    }
  }

  override protected function beforeDestroy():void  {
    dropTarget && dropTarget.unreg();
    super.beforeDestroy();
  }
}
}
