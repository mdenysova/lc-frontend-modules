package com.coremedia.blueprint.studio.bookmarks {
import com.coremedia.blueprint.studio.config.preferences.bookmarkAction;
import com.coremedia.cap.content.Content;
import com.coremedia.cms.editor.sdk.actions.ContentAction;

/**
 * Action for creating a bookmark using the active document.
 */
public class BookmarkAction extends ContentAction {

  private var bookmarkManager:BookmarkManager = BookmarkManager.getInstance();
  private var handleBookmark:Function;

  /**
   * @param config
   */
  public function BookmarkAction(config:bookmarkAction) {
    config['handler'] = doBookmarks;
    super(config);
    handleBookmark = config.handleBookmark;
  }

  private function isBookmarked(content:Content):Boolean {
    return (bookmarkManager.getBookmarkedContentExpression().getValue() as Array).indexOf(content) !== -1;
  }


  /**
   * Updates the label of the action, depending on the selected
   * content is already bookmarked or not.
   */
  override protected function isDisabledFor(contents:Array):Boolean {
    var disabled:Boolean = super.isDisabledFor(contents) || isMultiContents(contents);
    if (disabled) {
      return disabled;
    }
    return !(contents[0] is Content);
  }

  override protected function calculateHidden():Boolean {
    return false;
  }

  private function isMultiContents(contents:Array):Boolean {
    //the contents may be an array of contents (in the toolbar) or a single content (in the context menu)
    var content:Content = (contents && contents.length === 1) ? contents[0] : null;

    if (!content) {
      return true;
    }

    if (handleBookmark) {
      handleBookmark(isBookmarked(content));
    }

    return false;
  }

  /**
   * The action handler for this action, checks single and multi-selection.
   */
  private function doBookmarks():void {
    var multiSelection:Array = getContents();
    if (multiSelection && multiSelection.length > 0) {
      multiSelection.forEach(doBookmark);
    }
  }

  /**
   * Executes the bookmark action for the given content.
   * @param content
   */
  private function doBookmark(content:Content):void {
    if (!isBookmarked(content)) {
      bookmarkManager.applyBookmark(content.getName(), content);
    }
    else {
      bookmarkManager.removeBookmark(content);
    }
  }
}
}