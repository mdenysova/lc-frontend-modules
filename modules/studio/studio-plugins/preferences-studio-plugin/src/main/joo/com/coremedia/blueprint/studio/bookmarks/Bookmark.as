package com.coremedia.blueprint.studio.bookmarks {
import com.coremedia.cap.content.Content;

/**
 * Model for a bookmark.
 */
public class Bookmark {
  private var name:String;
  private var content:Content;

  public function Bookmark(name:String, content:Content) {
    this.name = name;
    this.content = content;
  }

  public function getName():String {
    return name;
  }

  public function getContent():Content {
    return content;
  }

  /**
   * Type of toString implementation
   * is important for sorting, so redefine
   * this method when named bookmarks
   * should be used.
   * @return
   */
  public function toString():String {
    return content.getName().toLowerCase();
  }
}
}