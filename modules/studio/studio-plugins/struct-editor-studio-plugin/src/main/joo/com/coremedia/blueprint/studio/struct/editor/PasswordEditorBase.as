package com.coremedia.blueprint.studio.struct.editor {
import com.coremedia.blueprint.studio.struct.StructEditor_properties;
import com.coremedia.blueprint.studio.struct.config.passwordEditor;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.ValueExpressionFactory;
import com.coremedia.ui.data.error.RemoteError;
import com.coremedia.ui.data.impl.RemoteServiceMethod;
import com.coremedia.ui.data.impl.RemoteServiceMethodResponse;

import ext.Ajax;
import ext.Button;
import ext.Container;
import ext.form.TextField;

import js.Event;

public class PasswordEditorBase extends Container {
  protected static const ITEM_ID_NEW_PASSWORD_TEXT_FIELD:String = "newPassword";
  protected static const ITEM_ID_REPEAT_PASSWORD_TEXT_FIELD:String = "repeatPassword";
  protected static const ITEM_ID_APPLY_PASSWORD_BUTTON:String = "applyPassword";

  private var newPasswordTextField:TextField;
  private var newPasswordValueExpression:ValueExpression;
  private var repeatPasswordTextField:TextField;
  private var repeatPasswordValueExpression:ValueExpression;
  private var bindTo:ValueExpression;

  public function PasswordEditorBase(config:passwordEditor = undefined) {
    bindTo = config.bindTo;
    super(config);
  }

  override protected function afterRender():void {
    super.afterRender();
    getNewPasswordValueExpression().addChangeListener(newPasswordChanged);
    getRepeatPasswordValueExpression().addChangeListener(repeatPasswordChanged);
  }

  override protected function beforeDestroy():void {
    super.beforeDestroy();
    getNewPasswordValueExpression().removeChangeListener(newPasswordChanged);
    getRepeatPasswordValueExpression().removeChangeListener(repeatPasswordChanged);
  }

  protected function applyPassword(button:Button = null, event:Event = null, closeUserDetails:Boolean = true, success:Function = null):void {
    var self:PasswordEditorBase = this;
    new RemoteServiceMethod("structeditor/encryptPassword", "POST").request({
      newPassword:getNewPasswordTextField().getValue(),
      repeatNewPassword:getRepeatPasswordTextField().getValue()
    }, function (response:RemoteServiceMethodResponse):void {
      var encrypted:* = response.getResponseJSON();
      if (encrypted) {
        self.bindTo.setValue(encrypted.password);
      }
    }, function (response:RemoteServiceMethodResponse):void {
      var error:RemoteError = response.getError();
      applyError(error.status);
      error.setHandled(true);
    });
  }

  private function newPasswordChanged():void {
    if (getNewPasswordValueExpression().getValue() !== getRepeatPasswordValueExpression().getValue()) {
      applyError(412);
    }
  }

  private function repeatPasswordChanged():void {
    if (getRepeatPasswordValueExpression().getValue() !== getNewPasswordValueExpression().getValue()) {
      applyError(412);
    }
  }

  private function applyError(status:uint):void {
    if (status === 412) {
      getNewPasswordTextField().markInvalid(StructEditor_properties.INSTANCE.Struct_errors_new);
      getRepeatPasswordTextField().markInvalid(StructEditor_properties.INSTANCE.Struct_errors_repeat);
    }
  }

  protected function getNewPasswordValueExpression():ValueExpression {
    if (!newPasswordValueExpression) {
      newPasswordValueExpression = ValueExpressionFactory.create("newPassword");
    }

    return newPasswordValueExpression;
  }

  protected function getRepeatPasswordValueExpression():ValueExpression {
    if (!repeatPasswordValueExpression) {
      repeatPasswordValueExpression = ValueExpressionFactory.create("repeatPassword");
    }

    return repeatPasswordValueExpression;
  }

  private function getNewPasswordTextField():TextField {
    if (!newPasswordTextField) {
      newPasswordTextField = find("itemId", ITEM_ID_NEW_PASSWORD_TEXT_FIELD)[0] as TextField;
    }

    return newPasswordTextField;
  }

  private function getRepeatPasswordTextField():TextField {
    if (!repeatPasswordTextField) {
      repeatPasswordTextField = find("itemId", ITEM_ID_REPEAT_PASSWORD_TEXT_FIELD)[0] as TextField;
    }

    return repeatPasswordTextField;
  }
}
}