###############################################################################
# content repository connection, configuring cms-connection.xml
###############################################################################
repository.url=http://${installation.host}:41080/coremedia/ior
repository.user=webserver
repository.domain=
repository.password=webserver

# Solr configuration: TODO move the configuration to a component or remove it after the studio specific livecontext asset service is introduced
solr.search.collection=preview
solr.search.url=http://${installation.host}:44080/solr/preview

# whether to connect to the workflow server; set to false if no workflow
# support is needed
repository.workflow=true

# Alternative storage for Studio Control Room based on IBM DB2
# if MongoDB should not be used:
controlroom.jdbc.driver=com.ibm.db2.jcc.DB2Driver
controlroom.jdbc.url=jdbc:db2://${installation.host}:50000/CM
controlroom.jdbc.user=myuser
controlroom.jdbc.password=mypassword

###############################################################################
# CoreMedia Studio configuration
###############################################################################

# A comma-separated list of locales from which the user can choose
# the locale in which the Studio is displayed. If unset, the
# list locales defaults English or German.
studio.locales=en,de,ja

# The URI prefix of the CAE preview webapp. This prefix is used to compose
# preview URIs for documents, assuming that a standard CAE preview controller
# is mapped  at the path 'preview'.
# Placeholder {0} is replaced with current studio tenant (if available).
# studio.previewUrlPrefix=http://{0}.localhost:40081/blueprint/servlet
studio.previewUrlPrefix=http://localhost:40081/blueprint/servlet

# Configurable preview controller pattern
# Placeholder O: coremedia ID, 1: numerics ID
studio.previewControllerPattern=preview?id={0}

# Configures a list of valid preview origins. The preview integration does
# only work for preview documents from listed origins. Wildcards (*) may
# be used for valid origin entries. If left blank (or if the property is
# not listed at all), the origin of studio.previewUrlPrefix is the only accepted origin.
# NOTE: Once a whitelist is configured, the Studio has no chance to set a target origin
#       in outgoing messages anymore. Be aware that this is a minor security drawback.
# EXAMPLE:
# pbe.studioUrlWhitelist=https://host1:port1,\
#  https://host2:port2,\
#  http://localhost:*,\
#  *.company.com
studio.previewUrlWhitelist=http://localhost:40081,*.coremedia.vm,*.coremedia.vm:40080,*.coremedia.com,*.coremedia.com:8000,*.coremedia.vm:8000,*.blueprint-box.vagrant

# Supported time zones for the studio installation.
# Provide a comma separated list of supported Java time zone IDs
studio.timeZones=Europe/Berlin,Europe/London,America/New_York,America/Los_Angeles

cae.is.preview=true

# Default time zone for CoreMedia studio. Make sure that the default time zone is
# included in the studio.timeZones list.
studio.defaultTimeZone=Europe/Berlin

# Declaration, which actions are checked before execution. These actions are only allowed
# for documents without error issues. As some actions are in a dependency relationship
# (e.g. APPROVE depends on CHECKIN), only the least inclusive actions need to be declared
# (e.g. CHECKIN entails the check of CHECKIN and APPROVE actions).
# Currently, the only supported OPTIONS are CHECKIN, APPROVE or nothing
studio.validateBefore=APPROVE

# Location of the document that defines the available locales.
studio.availableLocalesContentPath=/Settings/Options/Settings/LocaleSettings
# Property path to the Struct-StringListProperty containing the locales.
studio.availableLocalesPropertyPath=settings.availableLocales

# The maximum size of a markup object in bytes for which differences
# with other markup can be compute. By default, this value is set to 300000,
# which amounts to approximately 15000 words.
#studio.differencing.maxMarkupSize=300000

# The priority of publications started in Studio
studio.publicationPriority=60

###############################################################################
# CoreMedia Studio - Content Security Policy Configuration
###############################################################################

# Level of Content Security Policy protection. For further details about CSP and
# the default policy settings please refer to the Studio Developer Manual.
# Allowed values are:
# ENFORCE - Enable CSP protection.
# ENFORCE_ALLOW_DISABLE - Enable CSP protection unless the 'disableCsp' query parameter is 'true'.
# REPORT - Enable CSP report only mode without enforcing CSP protection.
# DISABLE - Disable CSP protection and reporting.
studio.security.csp.mode=DISABLE

# List of values for the 'script-src' policy directive.
# If this property is not provided the default values are 'self','unsafe-eval'.
#studio.security.csp.scriptSrc='self','unsafe-eval'

# List of values for the 'style-src' policy directive.
# If this property is not provided the default values are 'self','unsafe-inline'.
#studio.security.csp.styleSrc='self','unsafe-inline'

# List of values for the 'frame-src' policy directive.
# The hierarchy of default values for this directive is as follows
#   1. studio.previewUrlWhitelist values if specified
#   2. schema and authority of studio.previewUrlPrefix if specified
#   3. 'self'
#studio.security.csp.frameSrc=

# List of values for the 'connect-src' policy directive.
# If no custom list is provided the default value is 'self'.
#studio.security.csp.connectSrc=

# List of values for the 'font-src' policy directive.
# If no custom list is provided the default value is 'self'.
#studio.security.csp.fontSrc=

# List of values for the 'img-src' policy directive.
# If no custom list is provided the default value is 'self'.
#
# We allow all image source loaded via http or https here
# as otherwise images in the external library cannot be loaded.
studio.security.csp.imgSrc='self',data:,http:,https:

# List of values for the 'media-src' policy directive.
# If no custom list is provided the default value is 'self'.
#studio.security.csp.mediaSrc=

# List of values for the 'object-src' policy directive.
# If no custom list is provided the default value is 'self'.
studio.security.csp.objectSrc='self',www.kaltura.com

# List of values for the 'report-uri' policy directive.
# If no custom list is provided the directive is not included.
#studio.security.csp.reportUri=

###############################################################################
# CAE Toolbox Plugin configuration
###############################################################################

# JMX connector URL of the preview CAE (to be monitored by CAE Tools)
toolbox.jmx.url=service:jmx:rmi:///jndi/rmi://${installation.host}:1098/cae-preview
# JMX credentials (optional)
toolbox.jmx.user=
toolbox.jmx.password=

# comma-separated list of group names, with a "@<domain>" suffix, if necessary, e.g. "group1@domain, developers"
# non-existent groups will be ignored
toolbox.authorized_groups=administratoren

###############################################################################
# Elastic Social configuration
###############################################################################
# This is ok for development but in production environments you have to choose a live
# cae instead of the preview to generate urls that direct the user to those. No user should
# ever send request to the preview cae. In general the port should be empty for production
# environments, as users usually request a web server in front of the cae on port 80.
es.cae.http.port=40081
es.cae.http.host=${installation.host}
es.cae.protocol=http

#
# The site mapping so that Elastic Social can build absolute urls.
#
blueprint.site.mapping.helios=//${es.cae.http.host}:${es.cae.http.port}

###############################################################################
# CoreMedia Studio External Preview Configuration
###############################################################################
# The URL the REST resource of the external preview should push the data to.
# The controller addressed here must belong to a preview CAE.
externalpreview.restUrl=http:///wi7scdv17.coremedia.com/blueprint/servlet/service/externalpreview

# The URL where the login page of the external preview can be found.
externalpreview.previewUrl=https:///wi7scdv17.coremedia.com/blueprint/externalpreview

# This prefix + the relative preview URL (content.getPreviewUrl()) is send
# to the external preview URL to be displayed in the preview iframe
externalpreview.urlPrefix=https://wi7scdv17.coremedia.com

# The _internal_ name of the commerce host. This hostname is used for the backend connection
# from CAE to commerce server
livecontext.ibm.wcs.host=wi7scdv17.coremedia.com

# The publicly known name of the production commerce host in a preview (staging) environment.
# In general this is the name of a virtual
# (apache) host. An end user uses this hostname to contact the commerce system.
livecontext.apache.preview.production.wcs.host=${livecontext.ibm.wcs.host}
livecontext.apache.wcs.host=${livecontext.apache.preview.production.wcs.host}

