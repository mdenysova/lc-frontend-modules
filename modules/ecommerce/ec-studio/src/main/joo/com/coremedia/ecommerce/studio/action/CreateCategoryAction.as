package com.coremedia.ecommerce.studio.action {
import com.coremedia.cms.editor.sdk.actions.ActionConfigUtil;
import com.coremedia.ecommerce.studio.CMCategoryExtension;
import com.coremedia.ecommerce.studio.ECommerceStudioPlugin_properties;
import com.coremedia.ecommerce.studio.config.createCategoryAction;
import com.coremedia.ecommerce.studio.config.createCommerceCatalogObjectAction;

public class CreateCategoryAction extends CreateCommerceCatalogObjectAction {

  /**
   * @param config the configuration object
   */
  public function CreateCategoryAction(config:createCategoryAction) {
    config.contentType = CMCategoryExtension.CONTENT_TYPE_CATEGORY;
    super(createCommerceCatalogObjectAction(ActionConfigUtil.extendConfiguration(ECommerceStudioPlugin_properties.INSTANCE, config, 'createCategory')));
  }

}
}
