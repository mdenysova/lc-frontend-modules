package com.coremedia.ecommerce.studio {
import com.coremedia.blueprint.base.pagegrid.PageGridConstants;
import com.coremedia.blueprint.base.pagegrid.PageGridUtil;
import com.coremedia.blueprint.studio.qcreate.QuickCreate;
import com.coremedia.blueprint.studio.qcreate.processing.ProcessingData;
import com.coremedia.cap.common.CapType;
import com.coremedia.cap.content.Content;
import com.coremedia.cap.content.ContentProperties;
import com.coremedia.cap.struct.Struct;
import com.coremedia.cms.editor.sdk.util.MessageBoxUtil;
import com.coremedia.ecommerce.studio.components.link.QuickCreateCatalogLink;
import com.coremedia.ecommerce.studio.config.quickCreateCatalogLink;
import com.coremedia.ecommerce.studio.helper.CatalogHelper;
import com.coremedia.ecommerce.studio.model.Category;
import com.coremedia.ui.data.OperationResult;
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.data.ValueExpressionFactory;

import ext.Component;
import ext.Container;
import ext.config.container;
import ext.util.StringUtil;

public class CMCategoryExtension {

  public static const PARENT_PROPERTY:String = "parentChannel";
  public static const ROOT_PROPERTY:String = "rootChannel";


  public static const CONTENT_TYPE_CATEGORY:String = "CMCategory";
  public static const PROPERTY_DISPLAY_NAME:String = "displayName";
  public static const PROPERTY_SEGMENT:String = "segment";
  public static const PROPERTY_PLACEMENT:String = "placement";

  public static function register():void {

    QuickCreate.addQuickCreateDialogProperty(CONTENT_TYPE_CATEGORY, PARENT_PROPERTY, createComponent);

    QuickCreate.addSuccessHandler(CONTENT_TYPE_CATEGORY, process);
  }

  /**
   * Creates the custom list for the quick create dialog to link a parent category
   * that determines the target location of the created content.
   */
  public static function createComponent(data:ProcessingData, properties:Object):Component {
    properties.ddGroup = CatalogHelper.DD_GROUP_CATALOG;
    properties.openLinkSources = CatalogHelper.getInstance().openCatalog;
    properties.catalogObjectType = CatalogModel.TYPE_CATEGORY;
    properties.emptyText = ECommerceStudioPlugin_properties.INSTANCE.CMCategory_parentChannel_emptyText;
    var myCatalogLink:QuickCreateCatalogLink = new QuickCreateCatalogLink(quickCreateCatalogLink(properties));
    return new Container(new container({
      cls: 'link-list-wrapper-hbox-layout',
      fieldLabel: properties.label,
      autoWidth: true,
      items: [myCatalogLink]
    }));
  }

  /**
   * Executes as post processing for the quick create dialog.
   */
  private static function process(newContent:Content, data:ProcessingData, quickCreateCallback:Function):void {
    var parentId:String = data.get(PARENT_PROPERTY);

    //use the root category as default
    var parentCategory:Content = data.get(ROOT_PROPERTY) as Content;

    //apply another category as parent if there is an object in the catalog link list
    if(parentId) {
      var category:Category = CatalogHelper.getInstance().getCatalogObject(parentId) as Category;
      parentCategory = CatalogHelper.getContentForCatalogObject(category);
    }

    editContent(parentCategory, function (content:Content, callback:Function = undefined):void {
      if(content) {
        // link the new content to the parent
        var properties:ContentProperties = content.getProperties();
        var children:Array = properties.get("children") || [];
        properties.set("children", children.concat(newContent));
        content.flush(callback);

        //apply the name property to the display name
        newContent.getProperties().set(PROPERTY_DISPLAY_NAME, data.getName());
        newContent.getProperties().set(PROPERTY_SEGMENT, formatUrl(data.getName()));

        //apply the layout from parent category to sub category
        var parentLayoutValueExpression:ValueExpression = ValueExpressionFactory.createFromFunction(PageGridUtil.getLayoutSettings, content, "placement");

        parentLayoutValueExpression.loadValue(function(parentLayout:Content):void {
          parentLayout.load(function():void {
            var placement:Struct = newContent.getProperties().get("placement");
            placement.getType().addStructProperty(PageGridConstants.NEW_PLACEMENTS_BASE_PROPERTY);

            var placement2:Struct = placement.get(PageGridConstants.NEW_PLACEMENTS_BASE_PROPERTY);
            placement2.getType().addLinkProperty("layout", parentLayout.getType(), parentLayout);

            newContent.flush(null);
            quickCreateCallback.call(null);
          })
        });

      }
      else {
        //content creation failed caused by checkout out parent
        newContent.doDelete();
      }
    });
  }

  private static function formatUrl(name:String):String {
    var url:String = name;
    url = url.replace(/ /g, '_');
    return url;
  }

  private static function editContent(parent:Content, editFunction:Function):void {
    if (parent.isCheckedIn()) {
      parent.checkOut(function (result:OperationResult):void {
        parent.load(function ():void { // the checkout just invalidates the bean, reloading does occur asynchronously
          editFunction(parent, function ():void {
            parent.checkIn();
          });
        })
      });

    } else if (parent.isCheckedOutByCurrentSession()) {
      editFunction(parent);
    } else {
      editFunction(null);
      parent.load(function(loadedParent:Content):void {
        if(loadedParent.getEditor()) {
          loadedParent.getEditor().load(function():void {
            var msg:String = StringUtil.format(ECommerceStudioPlugin_properties.INSTANCE.Action_createCategory_error_message, loadedParent.getName(), loadedParent.getEditor().getName());
            MessageBoxUtil.showError(ECommerceStudioPlugin_properties.INSTANCE.Action_createCategory_error_title, msg);
          });
        }
        else {
          var msg:String = StringUtil.format(ECommerceStudioPlugin_properties.INSTANCE.Action_createCategory_error_general_message, loadedParent.getName());
          MessageBoxUtil.showError(ECommerceStudioPlugin_properties.INSTANCE.Action_createCategory_error_title, msg);
        }
      });

    }
  }
}
}