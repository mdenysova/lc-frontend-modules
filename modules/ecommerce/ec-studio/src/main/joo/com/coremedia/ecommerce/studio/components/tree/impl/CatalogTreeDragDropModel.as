package com.coremedia.ecommerce.studio.components.tree.impl {
import com.coremedia.ui.data.ValueExpression;
import com.coremedia.ui.models.DragDropModel;
import com.coremedia.ui.models.TreeModel;

public class CatalogTreeDragDropModel implements DragDropModel {

  private var treeModel:TreeModel;

  public function CatalogTreeDragDropModel(catalogTree:TreeModel) {
    this.treeModel = catalogTree;
  }


  public function moveNodes(droppedNodeIds:Array, targetNodeId:String):void {
  }

  public function copyNodes(droppedNodeIds:Array, targetNodeId:String, copiedModelsExpression:ValueExpression=null):void {
  }

  public function mayBeMoved(nodeIds:Array, targetNodeId:String):Boolean {
    return false;
  }

  public function mayBeCopied(nodeIds:Array, targetNodeId:String):Boolean {
    return false;
  }

  public function getModelItemId():String {
    return undefined;
  }
}
}