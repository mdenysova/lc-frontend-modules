package com.coremedia.ecommerce.studio.action {
import com.coremedia.cms.editor.sdk.actions.ActionConfigUtil;
import com.coremedia.ecommerce.studio.CMProductExtension;
import com.coremedia.ecommerce.studio.ECommerceStudioPlugin_properties;
import com.coremedia.ecommerce.studio.config.createCategoryAction;
import com.coremedia.ecommerce.studio.config.createCommerceCatalogObjectAction;
import com.coremedia.ecommerce.studio.model.Catalog;

public class CreateProductAction extends CreateCommerceCatalogObjectAction {

  /**
   * @param config the configuration object
   */
  public function CreateProductAction(config:createCategoryAction) {
    config.contentType = CMProductExtension.CONTENT_TYPE_PRODUCT;
    super(createCommerceCatalogObjectAction(ActionConfigUtil.extendConfiguration(ECommerceStudioPlugin_properties.INSTANCE, config, 'createProduct')));
  }


  override protected function isDisabledFor(catalogObjects:Array):Boolean {
    return super.isDisabledFor(catalogObjects) || (catalogObjects[0] is Catalog);
  }
}
}
