package com.coremedia.ecommerce.studio {
import com.coremedia.blueprint.studio.qcreate.QuickCreate;
import com.coremedia.blueprint.studio.qcreate.processing.ProcessingData;
import com.coremedia.cap.content.Content;
import com.coremedia.cap.content.ContentProperties;
import com.coremedia.ecommerce.studio.helper.CatalogHelper;
import com.coremedia.ecommerce.studio.model.Category;

public class CMProductExtension {
  public static const CONTENT_TYPE_PRODUCT:String = "CMProduct";
  public static const PROPERTY_PRODUCT_NAME:String = "productName";
  public static const PROPERTY_SEGMENT:String = "segment";

  public static function register():void {

    QuickCreate.addQuickCreateDialogProperty(CONTENT_TYPE_PRODUCT, CMCategoryExtension.PARENT_PROPERTY, CMCategoryExtension.createComponent);

    QuickCreate.addSuccessHandler(CONTENT_TYPE_PRODUCT, process);
  }

  /**
   * Executes as post processing for the quick create dialog.
   */
  private static function process(newContent:Content, data:ProcessingData, quickCreateCallback:Function):void {
    var parentId:String = data.get(CMCategoryExtension.PARENT_PROPERTY);
    var category:Category = CatalogHelper.getInstance().getCatalogObject(parentId) as Category;
    var parentCategory:Content = CatalogHelper.getContentForCatalogObject(category);

    if (newContent) {
      // link the new content to the parent
      var properties:ContentProperties = newContent.getProperties();
      var contexts:Array = properties.get("contexts") || [];
      properties.set("contexts", contexts.concat(parentCategory));
      newContent.getProperties().set(PROPERTY_SEGMENT, formatUrl(data.getName()));

      newContent.getProperties().set(PROPERTY_PRODUCT_NAME, data.getName());
      newContent.flush();

      quickCreateCallback.call(null);
    }
  }

  private static function formatUrl(name:String):String {
    var url:String = name;
    url = url.replace(/ /g, '_');
    return url;
  }
}
}