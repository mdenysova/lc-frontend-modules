<?xml version="1.0" encoding="ISO-8859-1"?>
<exml:component xmlns:exml="http://www.jangaroo.net/exml/0.8"
                xmlns="exml:ext.config"
                xmlns:editor="exml:com.coremedia.cms.editor.sdk.config"
                xmlns:ui="exml:com.coremedia.ui.config"
                xmlns:ec="exml:com.coremedia.ecommerce.studio.config"
                baseClass="com.coremedia.ecommerce.studio.components.link.CatalogLinkBase">

  <exml:import class="com.coremedia.ecommerce.studio.CatalogModel"/>
  <exml:import class="com.coremedia.ecommerce.studio.helper.CatalogHelper"/>
  <exml:import class="com.coremedia.ecommerce.studio.model.CatalogObject"/>
  <exml:import class="com.coremedia.ecommerce.studio.ECommerceStudioPlugin_properties"/>
  <exml:import class="com.coremedia.cms.editor.sdk.columns.grid.GridColumns_properties"/>
  <exml:import class="com.coremedia.ui.data.Bean"/>
  <exml:import class="com.coremedia.blueprint.studio.property.ImageLinkListRenderer"/>
  <exml:import class="com.coremedia.ui.data.ValueExpressionFactory"/>

  <exml:constant name="SELECTED_ITEMS_VARIABLE_NAME" value="selectedItems">
    <exml:description>
       The context property name for the selected items.
       The context value is an array of catalog objects
    </exml:description>
  </exml:constant>

  <exml:constant name="SELECTED_POSITIONS_VARIABLE_NAME" value="selectedPositions">
    <exml:description>
       The context property name for the selected positions.
       The context value is an array of numbers.
    </exml:description>
  </exml:constant>

  <exml:constant name="CONTENT_VARIABLE_NAME" value="content">
    <exml:description>
       The context property name for the content.
       The context value is a content object.
    </exml:description>
  </exml:constant>

  <exml:constant name="FORCE_READ_ONLY_VARIABLE_NAME" value="forceReadOnly">
    <exml:description>
       The context property name for the force read only property.
       The context value is a boolean.
    </exml:description>
  </exml:constant>

  <exml:cfg name="showThumbnails" type="Boolean">
    <exml:description>
       Set to false to hide the thumbnail of the entries. Default is true
    </exml:description>
  </exml:cfg>

  <exml:cfg name="propertyName" type="String">
    <exml:description>
       The name of the string property of the Bean to bind in this field.
       The string property holds the id of the catalog product
    </exml:description>
  </exml:cfg>

  <exml:cfg name="bindTo" type="com.coremedia.ui.data.ValueExpression">
    <exml:description>
       A property path expression leading to the Bean whose property is edited.
       This property editor assumes that this bean has a property 'properties'.
       If it is not specified the component will use the specified model bean.
    </exml:description>
  </exml:cfg>

  <exml:cfg name="forceReadOnlyValueExpression" type="com.coremedia.ui.data.ValueExpression">
    <exml:description>
       An optional ValueExpression which forces the component to read-only if it is evaluated to true.
    </exml:description>
  </exml:cfg>

  <exml:cfg name="readOnlyValueExpression" type="com.coremedia.ui.data.ValueExpression">
    <exml:description>
       An optional ValueExpression which makes the component read-only if it is evaluated to true.
    </exml:description>
  </exml:cfg>


  <exml:cfg name="additionalToolbarItems" type="Array"/>

  <exml:description>
     Catalog product property field. It can be assigned to a string property of a content which holds the product id.
  </exml:description>

  <exml:var name="readOnlyValueExpression" type="ValueExpression"
            value="{config.readOnlyValueExpression || ValueExpressionFactory.createFromValue(true)}"/>

  <grid hideHeaders="true"
        forceLayout="true"
        cls="link-list-property catalog-link-grid"
        ctCls="image-link-list-grid"
        autoHeight="true"
        autoWidth="true"
        autoExpandColumn="name">
    <plugins>
      <ui:bindListPlugin bindTo="{ValueExpressionFactory.createFromFunction(getCatalogListFunction(config))}">
        <ui:fields>
          <ui:dataField name="type"
                        mapping=""
                        convert="{convertTypeLabel}"/>
          <ui:dataField name="typeCls"
                        mapping=""
                        convert="{convertTypeCls}"/>
          <ui:dataField name="id"
                        mapping=""
                        convert="{convertIdLabel}"/>
          <ui:dataField name="name"
                        mapping=""
                        convert="{convertNameLabel}"/>
          <datafield name="imageUri"
                     mapping=""
                     convert="{function(v:String, catalogObject:CatalogObject):String {
                          return CatalogHelper.getInstance().getImageUrl(catalogObject)}}"/>
        </ui:fields>
      </ui:bindListPlugin>
      <editor:bindReadOnlyStyleClassPlugin bindTo="{config.bindTo}"
                                           forceReadOnlyValueExpression="{readOnlyValueExpression}"/>
      <ui:contextMenuPlugin disableDoubleClick="false">
        <ui:contextMenu>
          <ec:catalogLinkContextMenu propertyName="{config.propertyName}"
                                     bindTo="{config.bindTo}"
                                     forceReadOnlyValueExpression="{readOnlyValueExpression}"/>
        </ui:contextMenu>
      </ui:contextMenuPlugin>
      <ui:bindSelectionPlugin selectedValues="{getSelectedValuesExpression()}"
                              selectedPositions="{getSelectedPositionsExpression()}"/>
    </plugins>
    <tbar>
      <ec:catalogLinkToolbar additionalToolbarItems="{config.additionalToolbarItems}"
                             propertyName="{config.propertyName}"
                             readOnlyValueExpression="{readOnlyValueExpression}"
                             forceReadOnlyValueExpression="{config.forceReadOnlyValueExpression}"
                             bindTo="{config.bindTo}"/>
    </tbar>
    <columns>
      <gridcolumn id="thumbnail"
                  width="110"
                  sortable="false"
                  dataIndex="imageUri"
                  renderer="{thumbColRenderer}"
                  hidden="{config.showThumbnails === false}"/>
      <editor:typeIconColumn sortable="false"/>
      <gridcolumn header="{ECommerceStudioPlugin_properties.INSTANCE.id_header}"
                  id="id"
                  dataIndex="id"
                  sortable="false"/>
      <gridcolumn header="{GridColumns_properties.INSTANCE.name_header}"
                  id="name"
                  dataIndex="name"
                  sortable="false"/>
    </columns>
  </grid>
</exml:component>
