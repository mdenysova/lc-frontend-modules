package com.coremedia.ecommerce.studio.action {
import com.coremedia.blueprint.studio.config.quickcreate.quickCreateDialog;
import com.coremedia.blueprint.studio.qcreate.processing.ProcessingData;
import com.coremedia.cap.common.session;
import com.coremedia.cap.content.Content;
import com.coremedia.cap.content.ContentPropertyNames;
import com.coremedia.cap.content.ContentRepository;
import com.coremedia.cap.struct.Struct;
import com.coremedia.cms.editor.sdk.actions.ActionConfigUtil;
import com.coremedia.cms.editor.sdk.editorContext;
import com.coremedia.cms.editor.sdk.sites.Site;
import com.coremedia.ecommerce.studio.CMCategoryExtension;
import com.coremedia.ecommerce.studio.ECommerceStudioPlugin_properties;
import com.coremedia.ecommerce.studio.config.createCommerceCatalogObjectAction;
import com.coremedia.ecommerce.studio.helper.CatalogHelper;
import com.coremedia.ecommerce.studio.model.Catalog;
import com.coremedia.ecommerce.studio.model.CatalogObject;
import com.coremedia.ecommerce.studio.model.Category;
import com.coremedia.ecommerce.studio.model.Store;
import com.coremedia.ui.data.ValueExpressionFactory;

import ext.ComponentMgr;
import ext.Window;

use namespace session;

public class CreateCommerceCatalogObjectAction extends CatalogObjectAction {

  /**
   * @param config the configuration object
   */
  // hide if no CMS store or no category is selected

  protected var contentType:String;

  public function CreateCommerceCatalogObjectAction(config:createCommerceCatalogObjectAction) {
    super(createCommerceCatalogObjectAction(ActionConfigUtil.extendConfiguration(ECommerceStudioPlugin_properties.INSTANCE, config, config.actionName, {handler: handler})));
    contentType = config.contentType;
  }


  override protected function isHiddenFor(catalogObjects:Array):Boolean {
    return !CatalogHelper.getInstance().isActiveCoreMediaStore() || super.isHiddenFor(catalogObjects);
  }

  override protected function isDisabledFor(catalogObjects:Array):Boolean {
    return !isEnabledFor(catalogObjects);
  }

  private function handler():void {
    ValueExpressionFactory.createFromFunction(loadSelectedCategoryContent).loadValue(doCreate);
  }


  private function doCreate(parentCategory:Content):void {
    if (parentCategory) {
      showQuickCreateDialog();
    }
  }


  protected function loadSelectedCategoryContent():Content {
    var selected:CatalogObject = getCatalogObjects()[0];
    if (selected is Category) {
      return loadCategoryContent(selected as Category);
    }
    if (selected is Catalog) {
      return loadRootCategory();
    }
    return null;
  }


  /**
   * Opens the quick create dialog for the given content type.
   * Property editors for the dialog have already been registered in the Studio Plugin base class
   * of this plugin.
   */
  protected function showQuickCreateDialog():void {
    var catalogObject:CatalogObject = getCatalogObjects()[0];
    var dialogConfig:quickCreateDialog = new quickCreateDialog();

    dialogConfig.contentType = contentType;
    dialogConfig.defaultProperties = ProcessingData.NAME_PROPERTY;
    dialogConfig.model = new ProcessingData();
    dialogConfig.model.set(ProcessingData.FOLDER_PROPERTY, resolvePath);
    dialogConfig.model.set(ProcessingData.NAME_PROPERTY, '');
    dialogConfig.model.set(CMCategoryExtension.ROOT_PROPERTY, loadRootCategory());
    //skip if the catalog is selected
    if (catalogObject as Category) {
      dialogConfig.model.set(CMCategoryExtension.PARENT_PROPERTY, catalogObject.getId());
    }

    var dialog:Window = ComponentMgr.create(dialogConfig, 'window') as Window;
    dialog.show();
  }

  private function resolvePath(data:ProcessingData, callback:Function):void {
    var parentId:String = data.get(CMCategoryExtension.PARENT_PROPERTY);
    var parent:Content = loadSelectedCategoryContent();
    if (parentId) {
      var category:Category = CatalogHelper.getInstance().getCatalogObject(parentId) as Category;
      parent = CatalogHelper.getContentForCatalogObject(category);

    }
    ValueExpressionFactory.createFromFunction(function():String {
      if(!parent.isLoaded()) {
        parent.load();
        return undefined;
      }

      var parentFolder:Content = parent.getParent();
      if(!parentFolder.isLoaded()) {
        parentFolder.load();
        return undefined;
      }

      if(!parentFolder.getPath()) {
        return undefined;
      }
      return parentFolder.getPath();
    }).loadValue(function(path:String):void {
      callback.call(null, path);
    });
  }

  /**
   * Used in a FunctionValueExpression, returns the loaded content object for the given category.
   * @param category the category to retrieve the content for
   * @return the loaded content bean
   */
  protected static function loadCategoryContent(category:Category):Content {
    if (!category.isLoaded()) {
      category.load();
      return undefined;
    }
    var content:Content = CatalogHelper.getContentForCatalogObject(category);
    if (content && !content.isLoaded()) {
      content.load();
      return undefined;
    }
    return content;
  }


  private static function isEnabledFor(catalogObjects:Array):Boolean {
    // enabled if single category or the catalog is selected
    return catalogObjects.length === 1 && (catalogObjects[0] is Catalog || catalogObjects[0] is Category);
  }

  private static function loadRootCategory():Content {
    var store:Store = CatalogHelper.getInstance().getActiveStoreExpression().getValue() as Store;
    var site:Site = editorContext.getSitesService().getSite(store.getSiteId());
    var siteRoot:Content = site.getSiteRootDocument();
    var settings:Array = siteRoot.getProperties().get("linkedSettings");
    for (var i:int = 0; i < settings.length; i++) {
      var setting:Content = settings[i];
      if (!setting.isLoaded()) {
        setting.load();
        return undefined;
      }

      if (setting.getName() === "LiveContext") {
        var struct:Struct = setting.getProperties().get("settings");
        return struct.get("livecontext.rootCategory");
      }
    }
    return null;
  }
}
}