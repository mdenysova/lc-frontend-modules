package com.coremedia.ecommerce.studio.rest;

import com.coremedia.blueprint.base.ecommerce.catalog.AbstractCmsCommerceBean;
import com.coremedia.blueprint.base.livecontext.ecommerce.common.CommerceConnectionInitializer;
import com.coremedia.livecontext.ecommerce.common.CommerceBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;

/**
 * An abstract catalog object as a RESTful resource.
 */
public abstract class CommerceBeanResource<Entity extends CommerceBean> extends AbstractCatalogResource<Entity> {

  protected static final Logger LOG = LoggerFactory.getLogger(CommerceBeanResource.class);

  private CommerceConnectionInitializer commerceConnectionInitializer;

  protected void fillRepresentation(CommerceBeanRepresentation representation) {
    CommerceBean entity = getEntity();

    if (entity == null) {
      LOG.error("Error loading commerce bean");
      throw new CatalogRestException(Response.Status.NOT_FOUND,
              CatalogRestErrorCodes.COULD_NOT_FIND_CATALOG_BEAN,
              "Could not load commerce bean with id " + getId());
    }

    representation.setId(entity.getId());
    representation.setExternalId(entity.getExternalId());
    representation.setExternalTechId(entity.getExternalTechId());

    if (entity instanceof AbstractCmsCommerceBean) {
      representation.setContent(((AbstractCmsCommerceBean) entity).getContent());
    }
  }

}
