package com.coremedia.blueprint.caefeeder;

import com.coremedia.cache.Cache;
import com.coremedia.cache.CacheKey;
import com.coremedia.cap.persistentcache.InvalidPersistentCacheKeyException;
import com.coremedia.cap.persistentcache.PersistentCache;
import com.coremedia.cap.persistentcache.PersistentCacheKey;
import com.coremedia.cap.persistentcache.PersistentCacheKeyFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * this class wraps the persistent cache key lookup into a transient cache key to work around DENALI-1306 / VINSON-53
 */
public class WrappedPersistentCache implements PersistentCache, InitializingBean, DisposableBean {
  private PersistentCache persistentCache;
  private PersistentCacheKeyFactory persistentCacheKeyFactory;
  private Cache transientCache;

  public void setPersistentCache(PersistentCache persistentCache) {
    this.persistentCache = persistentCache;
  }

  public void setPersistentCacheKeyFactory(PersistentCacheKeyFactory factory) {
    this.persistentCacheKeyFactory = factory;
  }

  PersistentCache getPersistentCache() {
    return persistentCache;
  }

  PersistentCacheKeyFactory getPersistentCacheKeyFactory() {
    return persistentCacheKeyFactory;
  }

  public void setTransientCache(Cache transientCache) {
    this.transientCache = transientCache;
  }

  @Override
  public Object get(PersistentCacheKey persistentCacheKey) {
    return transientCache.get(new TransientCacheKey(persistentCacheKey.getSerialized()));
  }

  @Override
  public void remove(PersistentCacheKey persistentCacheKey) {
    persistentCache.remove(persistentCacheKey);
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    transientCache.setEnvironment(WrappedPersistentCache.class.getName(), this);
  }

  static WrappedPersistentCache fromCache(Cache cache) {
    return (WrappedPersistentCache) cache.getEnvironment(WrappedPersistentCache.class.getName());
  }

  @Override
  public void destroy() throws Exception {
    transientCache.setEnvironment(WrappedPersistentCache.class.getName(), null);
  }

  static class TransientCacheKey extends CacheKey {
    private final String serialized;

    TransientCacheKey(String serialized) {
      if(serialized == null) {
        throw new IllegalArgumentException("serialized key is null");
      }
      this.serialized = serialized;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }

      TransientCacheKey that = (TransientCacheKey) o;
      return serialized.equals(that.serialized);
    }

    @Override
    public int hashCode() {
      return serialized.hashCode();
    }

    @Override
    public Object evaluate(Cache cache) throws Exception {
      WrappedPersistentCache wrap = WrappedPersistentCache.fromCache(cache);
      PersistentCacheKeyFactory persistentCacheKeyFactory = wrap.getPersistentCacheKeyFactory();
      PersistentCache persistentCache = wrap.getPersistentCache();
      PersistentCacheKey key = persistentCacheKeyFactory.createKey(serialized);
      if(key == null) {
        throw new InvalidPersistentCacheKeyException("Could not de-serialize key: "+serialized);
      }
      return persistentCache.get(key);
    }

    @Override
    public String toString() {
      return serialized;
    }
  }
}
