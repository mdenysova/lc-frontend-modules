package com.coremedia.blueprint.caefeeder;

import com.coremedia.blueprint.base.tree.TreeRelation;
import com.coremedia.blueprint.common.contentbeans.CMLocTaxonomy;
import com.coremedia.blueprint.common.contentbeans.CMTaxonomy;
import com.coremedia.blueprint.testing.ContentTestCaseHelper;
import com.coremedia.cae.testing.TestInfrastructureBuilder;
import com.coremedia.cap.persistentcache.EvaluationException;
import com.coremedia.cap.persistentcache.PersistentCache;
import com.coremedia.cap.persistentcache.PersistentCacheKey;
import com.coremedia.cap.persistentcache.StoreException;
import org.junit.After;
import org.junit.Before;

public abstract class AbstractTaxonomyPropertyConverterTest {
  private static final TestInfrastructureBuilder.Infrastructure infrastructure = TestInfrastructureBuilder
          .create()
          .withContentBeanFactory()
          .withContentRepository("classpath:/com/coremedia/testing/contenttest.xml")
          .withDataViewFactory()
          .withIdProvider()
          .withCache()
          .withBeans("classpath:/framework/spring/blueprint-contentbeans.xml")
          .build();
  protected CMLocTaxonomy sanFrancisco;
  protected CMLocTaxonomy michigan;
  protected CMTaxonomy formula1;
  protected TaxonomyPropertyConverter taxonomyPropertyConverter;
  private WrappedPersistentCache wrappedPersistentCache;

  @Before
  public void setUp() throws Exception {
    sanFrancisco = ContentTestCaseHelper.getContentBean(infrastructure, 72);
    michigan = ContentTestCaseHelper.getContentBean(infrastructure, 70);
    formula1 = ContentTestCaseHelper.getContentBean(infrastructure, 80);

    // spring in java.
    TreePathKeyFactory taxonomyIdPathKeyFactory = createTreePathKeyFactory();
    taxonomyIdPathKeyFactory.setContentRepository(infrastructure.getContentRepository());
    taxonomyIdPathKeyFactory.setTreeRelation(infrastructure.getBean("taxonomyTreeRelation", TreeRelation.class));
    DummyPersistentCache dummyPersistentCache = new DummyPersistentCache();
    wrappedPersistentCache = new WrappedPersistentCache();
    wrappedPersistentCache.setPersistentCache(dummyPersistentCache);
    wrappedPersistentCache.setPersistentCacheKeyFactory(taxonomyIdPathKeyFactory);
    wrappedPersistentCache.setTransientCache(infrastructure.getCache());
    wrappedPersistentCache.afterPropertiesSet();
    taxonomyIdPathKeyFactory.setPersistentCache(wrappedPersistentCache);
    taxonomyPropertyConverter = new TaxonomyPropertyConverter();
    taxonomyPropertyConverter.setTaxonomyPathKeyFactory(taxonomyIdPathKeyFactory);
  }

  @After
  public void tearDown() throws Exception {
    wrappedPersistentCache.destroy();
  }

  protected abstract TreePathKeyFactory createTreePathKeyFactory();

  private static class DummyPersistentCache implements PersistentCache {
    @Override
    public Object get(PersistentCacheKey persistentCacheKey) throws StoreException, EvaluationException {
      try {
        return persistentCacheKey.evaluate();
      } catch (Exception e) {
        throw new EvaluationException(e);
      }
    }

    @Override
    public void remove(PersistentCacheKey persistentCacheKey) throws StoreException {
      throw new UnsupportedOperationException("Unimplemented: #remove");
    }
  }
}
