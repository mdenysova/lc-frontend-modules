package com.coremedia.blueprint.cae.contentbeans;

import com.coremedia.blueprint.common.contentbeans.CMLinkable;
import com.coremedia.blueprint.common.contentbeans.CMQueryList;

/**
 * Generated base class for immutable beans of document type CMQueryList.
 * Should not be changed.
 */
public abstract class CMQueryListBase extends CMDynamicListImpl<CMLinkable> implements CMQueryList {
  @Override
  public CMQueryList getMaster() {
    return (CMQueryList) super.getMaster();
  }
}
