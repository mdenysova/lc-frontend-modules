package com.coremedia.blueprint.cae.handlers;

import com.coremedia.blueprint.common.contentbeans.CMObject;
import com.coremedia.cap.common.CapBlobRef;
import com.coremedia.cap.common.InvalidPropertyValueException;
import com.coremedia.cap.common.NoSuchPropertyDescriptorException;
import com.coremedia.cap.content.Content;
import com.coremedia.objectserver.beans.ContentBean;
import com.coremedia.objectserver.web.HttpError;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

import javax.activation.MimeType;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test for {@link CapBlobHandler}
 */
public class CapBlobHandlerTest extends HandlerBaseTest {

  private static final String URI_JPG = "/resource/blob/1234/567/nae-me-jpg-propertyName.jpg";
  private static final String URI_ANY = "/resource/blob/1234/567/nae-me-jpg-propertyName.any";
  private static final String URI_RAW = "/resource/blob/1234/567/nae-me-jpg-propertyName.raw";
  private static final String URI_PNG = "/resource/blob/1234/567/nae-me-jpg-propertyName.png";
  private static final String CONTENT_ID = "1234";
  private static final String ETAG = "567";

  private static final String propertyName = "propertyName";

  private CapBlobRef capBlobRef;
  private CMObject cmObject;
  private Content content;

  @Override
  public void setUp() throws Exception {
    super.setUp();

    // 1. --- set up handler.
    String mimeType = "image/jpeg";
    registerMimeTypeWithExtensions(mimeType, "jpg");
    when(getUrlPathFormattingHelper().tidyUrlPath("nä me.jpg")).thenReturn("nae-me-jpg");

    CapBlobHandler testling = new CapBlobHandler();
    testling.setMimeTypeService(getMimeTypeService());
    testling.setUrlPathFormattingHelper(getUrlPathFormattingHelper());

    registerHandler(testling);

    // 2. --- mock content
    capBlobRef = mock(CapBlobRef.class);

    content = mock(Content.class);
    when(content.getName()).thenReturn("nä me.jpg");
    when(content.getId()).thenReturn("coremedia:///cap/content/1234");
    when(content.isContent()).thenReturn(true);
    when(content.isContentObject()).thenReturn(true);
    when(content.getBlobRef(propertyName)).thenReturn(capBlobRef);

    // 3. --- mock content(-bean) related stuff
    cmObject = mock(CMObject.class);
    when(cmObject.getContent()).thenReturn(content);

    when(capBlobRef.getCapObject()).thenReturn(content);
    when(capBlobRef.getContentType()).thenReturn(new MimeType(mimeType));
    when(capBlobRef.getPropertyName()).thenReturn(propertyName);
    when(capBlobRef.getETag()).thenReturn("567");
  }


  /**
   * Tests link generation
   */
  @Test
  public void testLink() throws Exception {
    assertEquals("uri", URI_JPG, formatLink(capBlobRef, null, false));
  }

  /**
   * Tests link for null ETag.
   */
  @Test
  public void testLinkWithNullETag() throws Exception {
    when(capBlobRef.getETag()).thenReturn(null);

    String expectedUrl = URI_JPG.replace(ETAG, "-");
    assertEquals("uri", expectedUrl, formatLink(capBlobRef, null, false));
  }

  /**
   * Test bean resolution and pattern matching
   */
  @Test
  public void testHandleBlobUrl() throws Exception {
    when(getIdContentBeanConverter().convert(CONTENT_ID)).thenReturn(cmObject);

    assertModel(handleRequest(URI_JPG), capBlobRef);
    Assert.assertTrue(handleRequest(URI_ANY).getModelMap().get("self") instanceof HttpError);
    assertModel(handleRequest(URI_RAW), capBlobRef);
  }

  /**
   * Test bean resolution and pattern matching for a URL including Japanese characters.
   */
  @Test
  public void testHandleBlobUrlWithJapaneseCharacters() throws Exception {
    when(getIdContentBeanConverter().convert(CONTENT_ID)).thenReturn(cmObject);

    // Java literals use UTF-16 code points (requiring 2 bytes per character), whereas in the URL,
    // the segment will be encoded in UTF-8, requiring three bytes per character.
    // The UTF-8, URL encoded segment equivalent to these four characters, is "%E8%A9%A6%E9%A8%93%E7%94%BB%E5%83%8F".
    String japaneseName = "\u8A66\u9A13\u753B\u50CF.jpg";
    String url = "/resource/blob/1234/567/" + japaneseName + "-jpg-propertyName.jpg";

    when(content.getName()).thenReturn(japaneseName);

    assertModel(handleRequest(url), capBlobRef);
  }

  /**
   * Test accepting "-" for a null ETag.
   */
  @Test
  public void testHandleBlobUrlWithNullETag() throws Exception {
    when(getIdContentBeanConverter().convert(CONTENT_ID)).thenReturn(cmObject);
    when(capBlobRef.getETag()).thenReturn(null);

    String requestUrl = URI_JPG.replace(ETAG, "-");
    assertModel(handleRequest(requestUrl), capBlobRef);
  }

  /**
   * Return a "not found" object, if bean is of wrong type.
   */
  @Test
  public void testNotFoundIfWrongBeanType() throws Exception {
    when(getIdContentBeanConverter().convert(CONTENT_ID)).thenReturn(mock(ContentBean.class));
    assertNotFound("not CMObject", handleRequest(URI_JPG));
  }

  /**
   * Return a "not found" object, if bean is null.
   */
  @Test
  public void testNotFoundIfBeanIsNull() throws Exception {
    when(getIdContentBeanConverter().convert(CONTENT_ID)).thenReturn(null);
    assertNotFound("null bean", handleRequest(URI_JPG));
  }

  /**
   * Return a "not found" object, if wrong extension.
   */
  @Test
  public void testNotFoundIfWrongExtension() throws Exception {
    when(getIdContentBeanConverter().convert(CONTENT_ID)).thenReturn(cmObject);
    assertNotFound("wrong extension", handleRequest(URI_PNG));
  }

  @Test
  public void testRedirectIfWrongETag() throws Exception {
    when(getIdContentBeanConverter().convert(CONTENT_ID)).thenReturn(cmObject);
    when(capBlobRef.getETag()).thenReturn("890");
    ModelAndView mav = handleRequest(URI_JPG);
    assertModel(mav, capBlobRef);
    assertEquals("redirect:DEFAULT", mav.getViewName());
  }

  /**
   * Return a "not found" object, if bean is of wrong type or extension does not match.
   */
  @Test
  public void testNotFound() throws Exception {
    when(getIdContentBeanConverter().convert(CONTENT_ID)).thenReturn(cmObject);

    // invalid property name
    Content content = mock(Content.class);
    when(content.getBlobRef(propertyName)).thenThrow(new NoSuchPropertyDescriptorException(propertyName));
    when(cmObject.getContent()).thenReturn(content);

    assertNotFound("illegal property name", handleRequest(URI_ANY));

    // accessing non-blob property
    content = mock(Content.class);
    when(content.getBlobRef(propertyName)).thenThrow(
      new InvalidPropertyValueException(null, null, null, null, null, null));
    when(cmObject.getContent()).thenReturn(content);
    assertNotFound("not a blob property", handleRequest(URI_ANY));
  }
}
