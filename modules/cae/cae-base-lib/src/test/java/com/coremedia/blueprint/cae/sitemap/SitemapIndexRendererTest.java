package com.coremedia.blueprint.cae.sitemap;

import com.coremedia.xml.DelegatingSaxHandler;
import com.coremedia.xml.XmlUtil5;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.xml.XMLConstants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import static org.junit.Assert.*;

public class SitemapIndexRendererTest {
  private static final String URL_PREFIX = "http://www.acme.com/";
  private static final String A_URL = URL_PREFIX + "path?foo=bar&bla=blub";
  private static final String XML_ESCAPED_URL = StringEscapeUtils.escapeXml(A_URL);

  private static final String FILENAME = "sitemap";
  private static final String REPOSITORY_PATH = "/Repository/Path";

  private String uniqueTmpdirPostfix;

  @Before
  public void initTargetDirName() {
    uniqueTmpdirPostfix = System.currentTimeMillis() + "-" + Math.round(Math.random()*100000000);
  }

  @After
  public void deleteTargetDir() {
    File targetDir = getTargetDir();
    if (targetDir.exists()) {
      try {
        FileUtils.forceDelete(targetDir);
      } catch (IOException e) {
        throw new IOError(e);
      }
    }
  }


  /**
   * No problem if the required properties change,
   * just a reminder to adapt configuration and documentation.
   */
  @Test
  public void testRequiredProperties() {
    SitemapIndexRenderer sir = new SitemapIndexRenderer();
    Collection<String> props = sir.requiredProperties();
    assertEquals("Unexpected number of required properties", 2, props.size());
    assertTrue("repositoryPath", props.contains("repositoryPath"));
    assertTrue("domain", props.contains("domain"));
  }

  @Test
  public void testSimpleSitemapIndex() {
    simpleSitemapIndex();
    checkSitemapIndexIn(getOutputDir());
  }

  @Test
  public void testBackup() {
    deleteTargetDir();
    File backupDir = new File(getOutputDir(), FILENAME + ".bak");
    simpleSitemapIndex();
    assertFalse("Unexpected backup", backupDir.exists());
    simpleSitemapIndex();
    checkSitemapIndexIn(backupDir);
  }

  @Test
  public void testSplitSitemap() throws FileNotFoundException{
    SitemapIndexRenderer sir = new SitemapIndexRenderer();
    sir.setTargetDirectory(getTargetDir().getAbsolutePath());
    sir.setProperties(createRequiredProperties());
    sir.startUrlList();
    for (int i = 0; i < 50001; ++i) {
      sir.appendUrl(A_URL + i);
    }
    sir.endUrlList();
    sir.getResponse();

    checkSitemapIndexIn(getOutputDir());

    String sitemapIndex = fileToString(new File(getOutputDir(), FILENAME + "_index.xml"));
    assertTrue("Unexpected index content", sitemapIndex.contains("<loc>" + URL_PREFIX + "sitemap1.xml.gz</loc>"));
    assertTrue("Unexpected index content", sitemapIndex.contains("<loc>" + URL_PREFIX + "sitemap2.xml.gz</loc>"));
    assertFalse("Unexpected index content", sitemapIndex.contains("<loc>" + URL_PREFIX + "sitemap3.xml.gz</loc>"));

    String sitemap = gzipToString(new File(getOutputDir(), FILENAME + "1.xml.gz"));
    assertTrue("Unexpected sitemap content", sitemap.contains("<url><loc>" + XML_ESCAPED_URL + "0</loc></url>"));
    assertTrue("Unexpected sitemap content", sitemap.contains("<url><loc>" + XML_ESCAPED_URL + "49999</loc></url>"));
    assertFalse("Unexpected sitemap content", sitemap.contains("<url><loc>" + XML_ESCAPED_URL + "50000</loc></url>"));

    sitemap = gzipToString(new File(getOutputDir(), FILENAME + "2.xml.gz"));
    assertTrue("Unexpected sitemap content", sitemap.contains("<url><loc>" + XML_ESCAPED_URL + "50000</loc></url>"));
    assertFalse("Unexpected sitemap content", sitemap.contains("<url><loc>" + XML_ESCAPED_URL + "49999</loc></url>"));
  }


  // --- internal ---------------------------------------------------

  private void simpleSitemapIndex() {
    SitemapIndexRenderer sir = new SitemapIndexRenderer();
    sir.setTargetDirectory(getTargetDir().getAbsolutePath());
    sir.setProperties(createRequiredProperties());
    sir.startUrlList();
    sir.appendUrl(A_URL);
    sir.endUrlList();
    sir.getResponse();
  }

  private void checkSitemapIndexIn(File targetDir) {
    assertTrue("No directory " + targetDir.getAbsolutePath(), targetDir.isDirectory());
    File sitemapIndexFile = new File(targetDir, FILENAME + "_index.xml");
    assertTrue("No index file in " + targetDir.getAbsolutePath(), sitemapIndexFile.exists());
    validate(sitemapIndexFile);
    String sitemapIndex = fileToString(sitemapIndexFile);
    assertTrue("Unexpected content", sitemapIndex.contains("<loc>" + URL_PREFIX + "sitemap1.xml.gz</loc>"));
    assertTrue("No sitemap file in " + targetDir.getAbsolutePath(), new File(targetDir, FILENAME + "1.xml.gz").exists());
  }

  private String fileToString(File file) {
    try {
      return IOUtils.toString(file.toURI().toURL(), "UTF-8");
    } catch (IOException e) {
      // Must not happen, escalate
      throw new IOError(e);
    }
  }

  private String gzipToString(File file) throws FileNotFoundException {
    FileInputStream fileInputStream = new FileInputStream(file);
    GZIPInputStream gzipInputStream = null;
    try {
      gzipInputStream = new GZIPInputStream(fileInputStream);
      return IOUtils.toString(gzipInputStream, "UTF-8");
    } catch (IOException e) {
      // Must not happen, escalate
      throw new IOError(e);
    } finally {
      IOUtils.closeQuietly(gzipInputStream);
      IOUtils.closeQuietly(fileInputStream);
    }
  }

  private Map<String, String> createRequiredProperties() {
    HashMap<String, String> map = new HashMap<>();
    map.put("repositoryPath", REPOSITORY_PATH);
    map.put("domain", "www.acme.com");
    return map;
  }

  private File getTargetDir() {
    File tmpDir = new File(System.getProperty("java.io.tmpdir"));
    return new File(tmpDir, "coremedia-" + uniqueTmpdirPostfix);
  }

  private File getOutputDir() {
    return new File(getTargetDir(), REPOSITORY_PATH);
  }

  private void validate(File sitemapIndex) {
    try {
      XmlUtil5 xmlutil = new XmlUtil5();
      xmlutil.registerSchema("http://www.sitemaps.org/schemas/sitemap/0.9",
              XMLConstants.W3C_XML_SCHEMA_NS_URI,
              new String[]{"http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd"});
      InputStream inputStream = new FileInputStream(sitemapIndex);
      try {
        // DelegatingSaxHandler for strict validation
        xmlutil.saxParse(inputStream, new DelegatingSaxHandler(null, null, null), null, "http://www.sitemaps.org/schemas/sitemap/0.9");
      } finally {
        IOUtils.closeQuietly(inputStream);
      }
    } catch (Exception e) {
      throw new RuntimeException("Sitemap validation failed", e);
    }
  }
}
