package com.coremedia.blueprint.cae.sitemap;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface SitemapUrlGenerator {
  void generateUrls(HttpServletRequest request, HttpServletResponse response, boolean absoluteUrls, UrlCollector urlCollector);
}
