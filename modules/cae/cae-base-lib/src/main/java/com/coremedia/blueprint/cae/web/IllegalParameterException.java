package com.coremedia.blueprint.cae.web;

public final class IllegalParameterException extends RuntimeException {
  private String param;
  private String value;

  public IllegalParameterException(String param, String value) {
    this.param = param;
    this.value = value;
  }

  @Override
  public String getMessage() {
    return "Illegal value for request parameter \"" + param + "\": \"" + value +"\"";
  }
}
