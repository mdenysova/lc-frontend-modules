package com.coremedia.blueprint.cae.sitemap;

public interface SitemapRequestParams {
  /**
   * The folder to be sitemapped recursively.
   */
  String PARAM_REPOSITORY_PATH = "repositoryPath";

  /**
   * Folders to be excluded (recursively) from sitemap generation.
   *
   * A comma separated list of paths.
   */
  String PARAM_EXCLUDE_FOLDERS = "excludeFolders";

  /**
   * Determines whether the generated sitemap is to be gzipped.
   */
  String PARAM_GZIP_COMPRESSION = "gzip";

  /**
   * The domain parameter is not a feature but needed only for technical reasons.
   *
   * The value MUST be the same as the domain of the sitemap entries.
   */
  String PARAM_DOMAIN = "domain";
}
