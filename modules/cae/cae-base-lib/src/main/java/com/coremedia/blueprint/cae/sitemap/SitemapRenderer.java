package com.coremedia.blueprint.cae.sitemap;

import java.util.Collection;
import java.util.Map;

/**
 * Enhance UrlCollector with some package internal features.
 */
public interface SitemapRenderer extends UrlCollector {
  /**
   * Initialize the renderer.
   */
  void startUrlList();

  /**
   * Close the list.
   */
  void endUrlList();

  /**
   * Return the response.
   * <p>
   * This may be the actual URL list or just a message if the URL list itself is
   * persisted somehow.
   *
   * @return the response to be written into the HttpResponse
   */
  String getResponse();

  /**
   * Returns the required properties for these factory's renderers.
   * If it returns null, setProperties will not be invoked.
   *
   * @return the required properties for these factory's renderers
   */
  Collection<String> requiredProperties();

  /**
   * Set the required properties for these factory's renderers.
   *
   * @param properties must contain entries for all values of requiredProperties
   */
  void setProperties(Map<String, String> properties);
}
