package com.coremedia.blueprint.cae.sitemap;

import com.google.common.collect.ImmutableMap;

import java.util.Collection;
import java.util.Map;

/**
 * Controls the lifecycle of the SitemapRenderer.
 * Overriding methods must invoke their super methods, except for requiredProperties().
 */
public abstract class AbstractSitemapRenderer implements SitemapRenderer {
  private StringBuilder urlList;
  private String result;
  private int count;
  private Map<String, String> properties;


  // --- SitemapRenderer --------------------------------------------

  @Override
  public Collection<String> requiredProperties() {
    return null;
  }

  @Override
  public void setProperties(Map<String, String> properties) {
    for (String property : requiredProperties()) {
      if (!properties.containsKey(property) || properties.get(property)==null) {
        throw new IllegalArgumentException("Missing property " + property);
      }
    }
    this.properties = new ImmutableMap.Builder<String, String>().putAll(properties).build();
  }

  @Override
  public void startUrlList() {
    if (properties==null && requiredProperties()!=null) {
      throw new IllegalStateException("Must call setProperties before.");
    }
    result = null;
    count = 0;
    urlList = new StringBuilder();
  }

  @Override
  public void appendUrl(String url) {
    if (urlList==null || result!=null) {
      throw new IllegalStateException("Must call startUrlList before.");
    }
    ++count;
  }

  @Override
  public void endUrlList() {
    if (urlList==null || result!=null) {
      throw new IllegalStateException("Must call startUrlList before.");
    }
    result = urlList.toString();
    urlList = null;
  }

  @Override
  public String getResponse() {
    if (result==null) {
      throw new IllegalStateException("Must call endUrlList before.");
    }

    return result;
  }


  // --- features ---------------------------------------------------

  /**
   * Returns the properties which have been set by setProperties.
   *
   * @return the properties
   */
  protected Map<String, String> getProperties() {
    return properties;
  }

  /**
   * Append something to the result.
   *
   * Helper method for the implementation of appendUrl(String url).
   * May be invoked only after startUrlList() and before endUrlList().
   *
   * @param str something
   */
  protected final void print(String str) {
    if (urlList==null || result!=null) {
      throw new IllegalStateException("May be invoked only after startUrlList() and before endUrlList().");
    }

    urlList.append(str);
  }

  /**
   * Append something to the result and add a trailing newline.
   *
   * Helper method for the implementation of appendUrl(String url).
   * May be invoked only after startUrlList() and before endUrlList().
   *
   * @param str something
   */
  protected final void println(String str) {
    print(str);
    urlList.append("\n");
  }

  /**
   * Returns the current number of list entries in the result.
   *
   * @return the current number of list entries in the result.
   */
  protected final int currentCount() {
    return count;
  }

  /**
   * Returns the interim result.
   *
   * Helper method for the implementation of appendUrl(String url).
   * May be invoked only after startUrlList() and before endUrlList().
   *
   * @return the interim result
   */
  protected final String currentResult() {
    return urlList.toString();
  }
}
