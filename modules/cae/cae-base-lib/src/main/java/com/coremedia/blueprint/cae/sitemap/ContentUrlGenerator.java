package com.coremedia.blueprint.cae.sitemap;

import com.coremedia.blueprint.cae.web.IllegalParameterException;
import com.coremedia.cap.common.CapConnection;
import com.coremedia.cap.content.Content;
import com.coremedia.common.util.Predicate;
import com.coremedia.objectserver.beans.ContentBean;
import com.coremedia.objectserver.beans.ContentBeanFactory;
import com.coremedia.objectserver.web.links.LinkFormatter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static com.coremedia.blueprint.base.links.UriConstants.Links.ABSOLUTE_URI_KEY;

public class ContentUrlGenerator implements SitemapUrlGenerator {
  private static final Logger LOG = LoggerFactory.getLogger(ContentUrlGenerator.class);
  public static final String SECURE_PARAM_NAME = "secure";

  private CapConnection capConnection;
  private LinkFormatter linkFormatter;
  private ContentBeanFactory contentBeanFactory;

  private List<String> exclusionPaths = new ArrayList<>();
  private List<Predicate<Content>> predicates = new ArrayList<>();

  private List<String> folderNamesToExclude;


  // --- configuration ----------------------------------------------

  @Required
  public void setCapConnection(CapConnection capConnection) {
    this.capConnection = capConnection;
  }

  @Required
  public void setLinkFormatter(LinkFormatter linkFormatter) {
    this.linkFormatter = linkFormatter;
  }

  @Required
  public void setContentBeanFactory(ContentBeanFactory contentBeanFactory) {
    this.contentBeanFactory = contentBeanFactory;
  }

  protected ContentBeanFactory getContentBeanFactory() {
    return contentBeanFactory;
  }

  public void setExclusionPaths(List<String> paths) {
    this.exclusionPaths = paths;
  }

  /**
   * Set a list of predicates.
   * <p/>
   * The predicates are conjuncted, i.e. if a predicate is not fulfilled for
   * a content, the URL is not generated.
   *
   * @param predicates the predicates
   */
  public void setPredicates(List<Predicate<Content>> predicates) {
    this.predicates = predicates;
  }


  // --- SitemapUrlGenerator ----------------------------------------

  /**
   * Generate content URLs
   */
  @Override
  public void generateUrls(HttpServletRequest request, HttpServletResponse response, boolean absoluteUrls, UrlCollector sitemapRenderer) {
    String folderNamesToExcludeParam = request.getParameter(SitemapRequestParams.PARAM_EXCLUDE_FOLDERS);
    folderNamesToExclude = StringUtils.isNotEmpty(folderNamesToExcludeParam) ? Arrays.asList(folderNamesToExcludeParam.split(",")) : new ArrayList<String>();
    Content sitemapRoot = extractRepositoryPath(request);
    buildUrls(sitemapRenderer, sitemapRoot, request, response, absoluteUrls);
  }


  // --- internal ---------------------------------------------------

  @Nonnull private Content extractRepositoryPath(HttpServletRequest request) {
    String repositoryPath = request.getParameter(SitemapRequestParams.PARAM_REPOSITORY_PATH);
    if (repositoryPath == null) {
      repositoryPath = "/";
      LOG.info("Parameter repositoryPath was null. Set to '/'");
    }
    Content sitemapRoot = capConnection.getContentRepository().getChild(repositoryPath);
    if (sitemapRoot==null) {
      throw new IllegalParameterException(SitemapRequestParams.PARAM_REPOSITORY_PATH, repositoryPath);
    }
    return sitemapRoot;
  }

  /**
   * Recursive call through the repository.
   *
   * @param builder  The string builder the urls are stored into.
   * @param folder   The current folder.
   * @param request  The active request.
   * @param response The active response.
   */
  private void buildUrls(UrlCollector builder, Content folder, HttpServletRequest request, HttpServletResponse response, boolean absoluteUrls) {
    if (!isPathExcluded(folder) && !isFolderNameExcluded(folder)) {
      Set<Content> children = folder.getChildren();
      for (Content child : children) {
        if (child.isFolder()) {
          buildUrls(builder, child, request, response, absoluteUrls);
        } else {
          buildUrl(builder, child, request, response, absoluteUrls);
        }
      }
    }
  }

  /**
   * Append the URL for the content.
   *
   * @param builder  The string builder the urls are stored into.
   * @param content  The linkable.
   * @param request  The active request.
   * @param response The active response.
   */
  private void buildUrl(UrlCollector builder, Content content, HttpServletRequest request, HttpServletResponse response, boolean absoluteUrls) {
    try {
      if (isValid(content)) {
        String link = createLink(content, request, response, absoluteUrls);
        if (link!=null) {
          // Make absolutely absolute
          if (link.startsWith("//")) {
            String secureString = request.getParameter(SECURE_PARAM_NAME);
            link = (Boolean.parseBoolean(secureString) ? "https:" : "http:") + link;
          }
          builder.appendUrl(link);
        }
      }
    } catch (Exception e) {
      LOG.warn("Cannot handle \"" + content + "\". Omit and continue.", e);
    }
  }

  private boolean isFolderNameExcluded(Content child) {
    String path = child.getPath();
    // check all excluded folder names
    for (String folderName : folderNamesToExclude) {
      if (path.endsWith("/" + folderName)) {
        if(LOG.isInfoEnabled()) {
          LOG.info("Found excluded folder name " + folderName + " for content " + child + " with path " + path);
        }
        return true;
      }
    }
    return false;
  }

  private boolean isValid(Content content) {
    for (Predicate predicate : predicates) {
      if (!predicate.include(content)) {
        return false;
      }
    }
    return true;
  }

  /**
   * Creates the URL for the given content.
   *
   * @return The URL for the given content.
   */
  protected String createLink(Content content, HttpServletRequest request, HttpServletResponse response, boolean absoluteUrls) {
    try {
      ContentBean bean = contentBeanFactory.createBeanFor(content);
      request.setAttribute(ABSOLUTE_URI_KEY, absoluteUrls);
      return linkFormatter.formatLink(bean, null, request, response, false);
    } catch (Exception e) {
      LOG.warn("Cannot not create link for " + content + ": " + e.getMessage());
    }
    return null;
  }

  /**
   * Checks if the given folder should not be analyzed for link resolving.
   *
   * @param folder The folder to check.
   * @return True if the path's documents should not be resolved.
   */
  private boolean isPathExcluded(Content folder) {
    for (String path : exclusionPaths) {
      if (path.equals(folder.getPath())) {
        return true;
      }
    }
    return false;
  }
}
