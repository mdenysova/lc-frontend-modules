package com.coremedia.blueprint.cae.sitemap;

import com.coremedia.cap.common.CapConnection;
import com.coremedia.cap.content.Content;
import com.coremedia.cap.content.ContentType;
import com.coremedia.common.util.Predicate;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Required;

public class SitemapDoctypePredicate implements Predicate, InitializingBean {
  @Override
  public boolean include(Object o) {
    if (!(o instanceof Content)) {
      return false;
    }
    Content content = (Content) o;
    return checkType(content);
  }

  private boolean checkType(Content content) {
    ContentType type = content.getType();

    boolean includeIt = false;
    for (String include : includes) {
      if (type.isSubtypeOf(include)) {
        includeIt = true;
        break;
      }
    }

    boolean excludeIt = false;
    if (includeIt) {
      for (String exclude : excludes) {
        if (type.isSubtypeOf(exclude)) {
          excludeIt = true;
          break;
        }
      }
    }

    return includeIt && !excludeIt;
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    try {
      for (String include : includes) {
        capConnection.getContentRepository().getContentType(include);
      }
      for (String exclude : excludes) {
        capConnection.getContentRepository().getContentType(exclude);
      }
    } catch (Exception e) {
      throw new IllegalStateException("This predicate is not suitable for your doctypes", e);
    }
  }

  @Required
  public void setIncludes(String[] includes) {
    this.includes = includes;
  }

  public void setExcludes(String[] excludes) {
    this.excludes = excludes;
  }

  @Required
  public void setCapConnection(CapConnection capConnection) {
    this.capConnection = capConnection;
  }

  private String[] includes = new String[]{};
  private String[] excludes = new String[]{};
  private CapConnection capConnection;
}
