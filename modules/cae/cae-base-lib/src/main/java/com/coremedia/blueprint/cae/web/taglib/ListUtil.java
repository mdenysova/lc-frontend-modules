package com.coremedia.blueprint.cae.web.taglib;

import java.util.List;

/**
 * Utility class for List-operations in JSTL.
 */
public final class ListUtil {

  // static utility class
  private ListUtil() {
  }

  public static boolean contains(List l, Object o){
    return l != null && l.contains(o);
  }

}
