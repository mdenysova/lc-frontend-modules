package com.coremedia.blueprint.cae.web.taglib;

import com.coremedia.blueprint.base.settings.SettingsService;

/**
 * Static SettingsService utilities to implement taglib functions.
 */
public final class SettingsFunction {
  private SettingsFunction() {
  }

  public static Object setting(SettingsService settingsService, Object self, String key) {
    return setting(settingsService, self, key, null);
  }

  public static Object setting(SettingsService settingsService, Object self, String key, Object defaultValue) {
    return settingsService.settingWithDefault(key, Object.class, defaultValue, self);
  }
}
