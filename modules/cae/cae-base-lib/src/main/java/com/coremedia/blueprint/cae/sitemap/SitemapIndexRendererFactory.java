package com.coremedia.blueprint.cae.sitemap;

import org.springframework.beans.factory.annotation.Required;

public final class SitemapIndexRendererFactory implements SitemapRendererFactory {

  private String targetDirectory;

  // --- Spring config ----------------------------------------------

  @Required
  public void setTargetDirectory(String targetDirectory) {
    this.targetDirectory = targetDirectory;
  }


  // --- SitemapRendererFactory -------------------------------------

  @Override
  public SitemapRenderer createInstance() {
    SitemapIndexRenderer sitemapIndexRenderer = new SitemapIndexRenderer();
    sitemapIndexRenderer.setTargetDirectory(targetDirectory);
    return sitemapIndexRenderer;
  }

  @Override
  public String getContentType() {
    // The SitemapIndexRenderer does not return the actual sitemap index
    // but a success message, so the type is not xml but plain.
    return "text/plain";
  }

  @Override
  public boolean absoluteUrls() {
    return true;
  }
}