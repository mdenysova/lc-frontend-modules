package com.coremedia.blueprint.cae.sitemap;

import com.coremedia.blueprint.cae.web.IllegalParameterException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPOutputStream;


/**
 * Creates lists of URLs, e.g. for sitemaps or performance tests.
 * <p/>
 * This controller accepts the following request parameters:
 * <p/>
 * <ul>
 * <li>repositoryPath: If defined content URLs will be generated for all matching content objects living in this path</li>
 * <li>excludeFolders: A comma separated list of folder names inside the content repository to exclude. The
 * folder name may not contain slashes at the beginning or at the end.</li>
 * </ul>
 */
public class SitemapGenerationController {
  private static final Logger LOG = LoggerFactory.getLogger(SitemapGenerationController.class);

  private SitemapRendererFactory sitemapRendererFactory;
  private List<SitemapUrlGenerator> urlGenerators;


  // --- configuration ----------------------------------------------

  @Required
  public void setSitemapRendererFactory(SitemapRendererFactory sitemapRendererFactory) {
    this.sitemapRendererFactory = sitemapRendererFactory;
  }

  @Required
  public void setUrlGenerators(List<SitemapUrlGenerator> urlGenerators) {
    this.urlGenerators = urlGenerators;
  }


  // --- features ---------------------------------------------------

  protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) {
    try {
      response.setContentType(sitemapRendererFactory.getContentType());
      response.setCharacterEncoding("UTF-8");
      String result = createUrls(request, response);
      boolean gzipCompression = getBooleanParameter(request, SitemapRequestParams.PARAM_GZIP_COMPRESSION, false);
      writeResultToResponse(result, response, gzipCompression);
      response.setStatus(HttpServletResponse.SC_OK);
    } catch (IOException e) {
      String msg = "Error when creating url list: " + e.getMessage();
      handleError(response, msg, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    } catch (IllegalParameterException e) {
      handleError(response, e.getMessage(), null, HttpServletResponse.SC_NOT_FOUND);
    } catch(Exception e) {
      handleError(response, e.getMessage(), e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }
    return null;
  }


  // --- internal ---------------------------------------------------

  private void handleError(HttpServletResponse response, String msg, Exception e, int httpErrorCode) {
    if (e != null) {
      LOG.error(msg, e);
    } else {
      LOG.info(msg);
    }
    try {
      response.sendError(httpErrorCode, msg);
    } catch (IOException e1) {
      LOG.error("Cannot send error to client.", e1);
    }
  }

  /**
   * Writes the generated URLs to the response output stream
   *
   * @param result renderer's result.
   * @param response The http servlet response to write the urls into.
   * @param gzipCompression compression flag
   * @throws IOException
   */
  private void writeResultToResponse(String result, HttpServletResponse response, boolean gzipCompression) throws IOException {
    OutputStream out = createOutputStream(response, gzipCompression);
    try {
      out.write(result.getBytes("UTF-8"));
    } finally {
      IOUtils.closeQuietly(out);
    }
  }

  /**
   * Helper for parsing boolean parameter values.
   *
   * @param request The request that contains the parameter
   * @param param The name of the parameter
   * @param defaultValue The default value if the parameter is not set
   * @return A boolean param from the request
   */
  private boolean getBooleanParameter(HttpServletRequest request, String param, boolean defaultValue) {
    String value = request.getParameter(param);
    if (value != null) {
      return Boolean.parseBoolean(value);
    }
    return defaultValue;
  }

  /**
   * Creates the output stream for writing the response depending of passed parameters.
   *
   * @param response The HttpServletResponse to write for.
   * @param gzipCompression compression flag
   * @return The OutputStream
   * @throws IOException
   */
  private OutputStream createOutputStream(HttpServletResponse response, boolean gzipCompression) throws IOException {
    if (gzipCompression) {
      response.setHeader("Content-Encoding", "gzip");
      return new GZIPOutputStream(response.getOutputStream());
    }
    return new BufferedOutputStream(response.getOutputStream());
  }

  /**
   * Walks recursivly through the repository and resolves the link url
   * for each content object that is not filtered through the exclusion
   * criteria.
   *
   * @return The renderer's result.
   */
  private String createUrls(HttpServletRequest request, HttpServletResponse response) {
    SitemapRenderer sitemapRenderer = sitemapRendererFactory.createInstance();
    configureSitemapRendererFromRequest(sitemapRenderer, request);
    sitemapRenderer.startUrlList();
    for (SitemapUrlGenerator urlGenerator : urlGenerators) {
      urlGenerator.generateUrls(request, response, sitemapRendererFactory.absoluteUrls(), sitemapRenderer);
    }
    sitemapRenderer.endUrlList();
    return sitemapRenderer.getResponse();
  }

  /**
   * We fetch the required properties from request parameters in order to
   * preserve flexibility for requests.  If your properties are fix for the
   * application or have at least reasonable defaults, you can set them via
   * Spring and keep the requests simple.
   */
  private void configureSitemapRendererFromRequest(SitemapRenderer sitemapRenderer, HttpServletRequest request) {
    Collection<String> propertyKeys = sitemapRenderer.requiredProperties();
    if (propertyKeys!=null) {
      Map<String, String> properties = fetchParameters(request, propertyKeys);
      sitemapRenderer.setProperties(properties);
    }
  }

  /**
   * Fetch the requested parameters from the request.
   *
   * @param request the request
   * @param paramKeys  the required parameters
   * @return a Map of the parameters
   * @throws com.coremedia.blueprint.cae.web.IllegalParameterException if a parameter is not set in the request
   */
  private Map<String, String> fetchParameters(HttpServletRequest request, Collection<String> paramKeys) {
    Map<String, String> properties = new HashMap<>();
    for (String property : paramKeys) {
      String value = request.getParameter(property);
      if (value==null) {
        throw new IllegalParameterException(property, "not set");
      }
      properties.put(property, value);
    }
    return properties;
  }

}
