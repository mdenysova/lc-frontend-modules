<#ftl strip_whitespace=true>

<#-- blueprint-taglib -->
<#include "blueprint/core.ftl" />
<#include "blueprint/util.ftl" />
<#include "blueprint/button.ftl" />
<#include "blueprint/notification.ftl" />
<#include "blueprint/form.ftl" />
