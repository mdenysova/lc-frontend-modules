<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.navigation.Navigation" -->
<#-- @ftlvariable name="cssClass" type="java.lang.String" -->
<#-- @ftlvariable name="childrenCssClass" type="java.lang.String" -->
<#-- @ftlvariable name="maxDepth" type="java.lang.Integer" -->

<#assign cssClass=cm.localParameters().cssClass!""/>
<#assign childrenCssClass=cm.localParameters().childrenCssClass!""/>

<#if self.visibleChildren?has_content>
  <#if !(maxDepth?has_content) || (maxDepth > 0)>
    <#if maxDepth?has_content>
      <#assign maxDepth=maxDepth - 1 />
    </#if>
    <ul class="${cssClass}">
      <#list self.visibleChildren![] as child>
        <@cm.include self=child view="asNavLinkListItem" params={
          "maxDepth": maxDepth!0,
          "navPathList": cmpage.navigation.navigationPathList,
          "cssClass": childrenCssClass
        } />
      </#list>
    </ul>
  </#if>
</#if>
