<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.contentbeans.CMPicture" -->
<#-- @ftlvariable name="limitAspectRatios" type="java.util.List" -->
<#-- @ftlvariable name="classBox" type="java.lang.String" -->

<#assign title=self.title!"" />
<#-- get the last entry (largest image) for the selected aspect ratio -->
<#assign fullImageLink=bp.responsiveImageLinks(self, limitAspectRatios?first).lastEntry().value!"" />

<div class="cm-lightbox ${classBox}">
  <a href="${fullImageLink}" title="${title}">
    <@cm.include self=self params={
    "limitAspectRatios": limitAspectRatios,
    "classBox": "cm-aspect-ratio-box",
    "classImage": "cm-aspect-ratio-box__content"
    }/>
  </a>
</div>
