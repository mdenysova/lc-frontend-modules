/* --- create own namespace in javascript for own stuff ------------------------------------------------------------- */
var coremedia = (function (module) {
  return module;
}(coremedia || {}));
coremedia.blueprint = (function (module) {
  module.$ = $.noConflict(true);
  return module;
}(coremedia.blueprint || {}));
