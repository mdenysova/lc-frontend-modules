<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.contentbeans.CMPicture" -->
<#-- @ftlvariable name="limitAspectRatios" type="java.util.List" -->
<#-- @ftlvariable name="classBox" type="java.lang.String" -->
<#-- @ftlvariable name="classImage" type="java.lang.String" -->
<#-- @ftlvariable name="metadata" type="java.util.List" -->
<#-- @ftlvariable name="crop" type="java.lang.String" -->
<#-- @ftlvariable name="additionalAttr" type="java.util.Map" -->

<div class="${classBox!""}"<@cm.metadata (metadata![]) + [self.content]/>>
  <#if self.data?has_content>

    <#-- define list of css classes to be used, "cm-image" will always be added -->
    <#assign classes=["cm-image", "cm-image--background"] />
    <#-- add additional image class(es) passed from including template -->
    <#if classImage?has_content>
      <#assign classes=classes + [classImage] />
    </#if>

    <#assign imageLink="" />
    <#assign responsiveImageData={} />

    <#-- decide if responsiveImage functionality is to be used or uncropped image will be shown -->
    <#if self.disableCropping>
      <#-- A) Cropping disabled, display image in full size -->
      <#assign imageLink=bp.uncroppedImageLink(self) />
    <#else>
      <#-- B) display responsive image -->
      <#assign classes=classes + ["cm-responsive-image"] />
      <#assign responsiveImageData=bp.responsiveImageLinksData(self, limitAspectRatios!{}) />
    </#if>
    <#-- add all classes to attributes -->
    <#assign attributes=bp.extendSequenceInMap(additionalAttr!{}, "classes", classes) />

    <#-- add all attributes to the map -->
    <#if imageLink?has_content>
      <#assign attributes=attributes + {"style": "background-image: url(${imageLink})"} />
    </#if>

    <div <@bp.renderAttr attributes />
      <@cm.dataAttribute name="data-cm-responsive-image" data=responsiveImageData />
      <@cm.metadata data=["properties.data" + crop???string(".", "") + crop!""] />>
    </div>
  </#if>
</div>
