<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.contentbeans.CMDownload" -->
<#-- @ftlvariable name="openInTab" type="java.lang.Boolean" -->
<#-- @ftlvariable name="cssClass" type="java.lang.String" -->

<#-- same as  CMTeasable.asLink but with target=_blank, a link to the blob data and type and size information -->
<#assign cssClass=cm.localParameters().cssClass!"" />
<#assign target="">
<#if cm.localParameters().openInTab!true>
  <#assign target=' target="_blank"'>
</#if>

<#if self.data?has_content>
  <#assign extension=cm.getLink(self.data)?keep_after_last(".") + ", "!""/>
  <a class="${cssClass!""}" href="${cm.getLink(self.data)}"${target} title="${self.teaserTitle!''}"<@cm.metadata data=[self.content, "properties.teaserTitle"] />>
    ${self.teaserTitle!""} <span>(${extension} ${bp.getDisplaySize(self.data.size)})</span>
  </a>
</#if>
