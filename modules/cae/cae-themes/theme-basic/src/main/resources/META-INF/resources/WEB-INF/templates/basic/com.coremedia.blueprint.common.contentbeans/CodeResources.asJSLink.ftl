<#-- This template is used at bottom of <body/> for live-cae with minified and merged javascript -->
<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.contentbeans.CodeResources" -->

<script src="${cm.getLink(self, {"extension": "js"})}" <@cm.metadata self.context.content />></script>
