<#-- @ftlvariable name="self" type="com.coremedia.blueprint.common.contentbeans.CMSitemap" -->

<#if self.root?has_content>
<div class="cm-sitemap">
    <h2 class="cm-sitemap__title">${self.title!""}</h2>
    <ul class="cm-sitemap__items">
      <@cm.include self=self.root view="asLinkListItem" params={
      "maxDepth": bp.setting(self, "sitemap_depth", 3),
      "isRoot": true
      }/>
    </ul>
</div>
</#if>
