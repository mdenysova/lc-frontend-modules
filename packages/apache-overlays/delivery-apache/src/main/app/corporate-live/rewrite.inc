RewriteEngine on
RewriteLog "@DELIVERY_APACHE_LOG_DIR@/corporate-live-rewrite.log"
RewriteLogLevel @DELIVERY_APACHE_REWRITE_LOGLEVEL@

# Remove preview querystring
RewriteCond %{QUERY_STRING} ^.*view=fragmentPreview.*$
RewriteRule (.*) $1? [R=301,L]

# Internal services, served only by Tomcat. Deny external invocation.
RewriteCond %{REQUEST_URI} ^/internal/(.*)
RewriteRule ^/internal/(.*) - [F,PT,L]

RewriteCond %{REQUEST_URI} ^/blueprint/servlet/internal/(.*)
RewriteRule ^/blueprint/servlet/internal/(.*) - [F,PT,L]

# Map the cron generated sitemap files
RewriteCond  %{REQUEST_URI} ^/sitemap_index.xml
RewriteRule  ^/sitemap_index.xml  /blueprint/servlet/service/sitemap/Sites/Corporate/United\ States/English/sitemap_index.xml  [PT,L]

RewriteCond  %{REQUEST_URI} ^/sitemap(.*).xml.gz
RewriteRule  ^/sitemap(.*).xml.gz  /blueprint/servlet/service/sitemap/Sites/Corporate/United\ States/English/sitemap$1.xml.gz  [PT,L]

# robots.txt
RewriteCond %{REQUEST_URI} ^/robots.txt
RewriteRule ^/robots.txt /blueprint/servlet/service/robots/corporate [PT,L]

# Send empty URL to corporate home page
RewriteCond %{REQUEST_URI} ^/$
RewriteRule ^/$ /corporate [PT,L]
