drop database if exists cm7management;
drop database if exists cm7caefeeder;
drop database if exists cm7master;
drop database if exists cm7mcaefeeder;
drop database if exists cm7replication;

drop user cm7management@localhost;
drop user cm7caefeeder@localhost;
drop user cm7master@localhost;
drop user cm7mcaefeeder@localhost;
drop user cm7replication@localhost;
