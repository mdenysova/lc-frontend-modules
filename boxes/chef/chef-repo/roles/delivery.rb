name 'delivery'
description 'The role for CoreMedia Delivery nodes'

#noinspection RubyStringKeysInHashInspection
default_attributes 'coremedia' => {
        'sitemaps' => {
                'helios'  => {
                        'path'  => '/Sites/PerfectChef/United\%20States/English',
                        'hour' => '*/12'
                }
        }
}
#noinspection RubyStringKeysInHashInspection
override_attributes 'coremedia' => {'configuration' => {'configure.DELIVERY_TLD' => 'ps-test-01.coremedia.vm:40080'}}

run_list 'role[base]',
         'recipe[coremedia::management_configuration_override]',
         'recipe[coremedia::replication_configuration_override]',
         'recipe[coremedia::delivery]',
         'recipe[coremedia::delivery_apache]',
         'recipe[coremedia::sitemaps]'
